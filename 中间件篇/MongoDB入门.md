### MongoDB入门

#### 概述

- 数据库就是按照数据结构组织、存储和管理数据的仓库
- 此处不再详细介绍数据库相关知识，此处知识已经在MySQL篇详细介绍

---

#### MongoDB配置与安装

- 推荐教程： https://www.runoob.com/mongodb/mongodb-tutorial.html 

- 文档型数据库

- 是为快速开发互联网Web应用而设计得数据库系统
- 设计目标是极简、灵活、作为Web应用栈得一部分
- 她得数据模型是面向文档得，所谓文档是一种类似于JSON的结构。简单理解就是MongoDB这个数据库中村的是各种各样的JSON(BSON)

- **三个概念**
- 数据库（database）
  - 是一个仓库，在仓库中可以存放集合
- 集合（collection）
  - 集合类似于数组，在集合中可以存放文档
- 文档（document）
  - 文档数据库的最小单位，我们存储和操作的内容都是文档

- 官网： https://www.mongodb.org.cn/ 
- 偶数版本为稳定版本，奇数版本为开发版本
- 对32位操作系统支持不佳，需要使用64位操作系统

- 安装 - 傻瓜式安装即可
- 然后配置环境变量到bin目录下即可
- 在C盘根目录，新建一个data文件夹。然后在data文件夹里面新建一个db文件夹（这是默认的数据库文件地址），然后启动命令行 cmd ，然后执行 mongod 如下所示

![1600090170392](images/1600090170392.png)

- 然后再打开一个cmd窗口，输入mongo，然后测试

![1600090637637](images/1600090637637.png)

- 如果不想放在C盘，可以执行以下命令 但是每次都要设置才可以

```shell
mongod --dbpath E:\MongodbHome\data\db --port 123
后面的是指定端口
```

- mongod 启动的是服务端

- mongo 启动的是客户端

- 设置服务器开机自启，设置为系统服务，后台运行，不需要每次自己手动开启

  - 在C盘根目录下的data文件夹，创建log文件夹和db，用来存放日志
  - 找到安装路径：F:\EnvironmentHomes\mongodb3.2 添加一个配置文件 mongod. cfg 

  ```cfg
  systemLog:
      destination: file
      path: c:\data\log\mongod.log
  storage:
      dbPath: c:\data\db
  ```

  - 以管理员的身份打开命令行窗口

  ```txt
  sc.exe create MongoDB binPath= "\"F:\EnvironmentHomes\mongodb3.2\bin\mongod.exe\" --service --config=\"F:\EnvironmentHomes\mongodb3.2\mongod.cfg\"" DisplayName= "MongoDB" start= "auto"
  ```

  ![1600092594137](images/1600092594137.png)

  - 然后启动服务 如下 则设置成功
  
  ![1600093955902](images/1600093955902.png)
  
  - 如果启动失败，证明上边的操作有误，在控制台输入 sc delete MongoDB 删除之前配置的服务，然后从第一步再来一次

#### MongoDB基操

- 系统架构

![1600131032695](images/1600131032695.png)

- 基本概念
  - 数据库（database）
  - 集合（collection）
  - 文档（document）
  - 在mongo中，数据库和集合都不需要手动创建
  - 当我们创建的时候 ，如果文档所在的集合或者数据库不存在的时候就会自动创建数据库和集合
- 查询有哪些数据库：show dbs / show databases

![1600131245185](images/1600131245185.png)

- 进入数据库：use 数据库名字 注意：如果没有这个数据库就创建（在第一次进入并且添加内容的时候），有这个数据库就使用
- 查询当前处于哪个数据库：db
- 查看数据库有多少集合：show collections
- **数据库的CRUD操作**
  - **添加**
  - 向数据库中插入文档  db.<集合名字>.insert(文档)
  - db.collection.insert()
    - insert()可以向集合中插入一个或多个文档
  - db.collection.insertOne()
     - 向集合中插入一个文档
  - db.collection.insertMany()
    - 向集合中插入多个文档
  - 例子
  - db.<collection>.insert(doc) 
    - 例子：向test数据库中的，stus集合中插入一个新的学生对象
    - {name:"孙悟空",age:18,gender:"男"}
    - db.stus.insert({name:"孙悟空",age:18,gender:"男"})
  - **查询**
    - db.<collection>.find()
       - 查询当前集合中的所有的文档
  - db.collection.find()
    - 可以根据指定条件从集合中查询所有符合条件的文档
    - find() 里面的参数没有，那就是查询所有的
    - 可以接受一个对象作为条件参数({属性名:值})，例如：db.stus.find({name="孙悟空"});
    - 返回的是一个数组
  - db.collection.findOne()
    - 查询第一个符合条件的文档
    - 返回的是一个对象
  - db.collection.find().count()
    - 查询符合条件的文档的数量
  - **修改**
  - 修改数据库中的文档
    - db.collection.update()
    - 可以修改、替换集合中的一个或多个文档
    - db.stus.update({name="猪八戒"},{age=90}) // 前面的是查询。后面的是需要修改的内容，但是默认情况下是使用新对象来替换旧的对象，而且默认情况只修改一个
    - 如果需要修改指定的属性，$set 可以用来修改指定的属性 $unset 是删除一个属性
      - db.stus.update({name="猪八戒"},{$set {age=90}}) 
    - db.collection.updateOne()
    	- 修改集合中的一个文档
    - db.collection.updateMany()
      - 修改集合中的多个文档
    - db.collection.replaceOne()
      - 替换集合中的一个文档
  - **删除**
  - 删除集合中的文档
  	- db.collection.remove()
  		- 删除集合中的一个或多个文档（默认删除多个）
  		- 第二个参数传递一个 true 就只会删除一个，如果传递一个空对象作为参数，就会全部删除
  	- db.collection.deleteOne()
  		- 删除集合中的一个文档
  	- db.collection.deleteMany()
  		- 删除集合中的多个文档
  	- 清空一个集合
  		db.collection.remove({})
  	- 删除一个集合
  		db.collection.drop()
  	- 删除一个数据库
  		db.dropDatabase()

#### 练习1

```js
//1.进入my_test数据库
use my_test

//2.向数据库的user集合中插入一个文档  
db.users.insert({
    username:"sunwukong"
});

//3.查询user集合中的文档
db.users.find();

//4.向数据库的user集合中插入一个文档   
db.users.insert({
    username:"zhubajie"
});
   
//5.查询数据库user集合中的文档
db.users.find();

//6.统计数据库user集合中的文档数量
db.users.find().count();

//7.查询数据库user集合中username为sunwukong的文档
db.users.find({username:"sunwukong"});

//8.向数据库user集合中的username为sunwukong的文档，添加一个address属性，属性值为huaguoshan
db.users.update({username:"sunwukong"},{$set:{address:"huaguoshan"}});


//9.使用{username:"tangseng"} 替换 username 为 zhubajie的文档
db.users.replaceOne({username:"zhubajie"},{username:"tangseng"});    
    
//10.删除username为sunwukong的文档的address属性
db.users.update({username:"sunwukong"},{$unset:{address:1}});


//11.向username为sunwukong的文档中，添加一个hobby:{cities:["beijing","shanghai","shenzhen"] , movies:["sanguo","hero"]}
//MongoDB的文档的属性值也可以是一个文档，当一个文档的属性值是一个文档时，我们称这个文档叫做 内嵌文档
db.users.update({username:"sunwukong"},{$set:{hobby:{cities:["beijing","shanghai","shenzhen"] , movies:["sanguo","hero"]}}});
db.users.find();

//12.向username为tangseng的文档中，添加一个hobby:{movies:["A Chinese Odyssey","King of comedy"]}
db.users.update({username:"tangseng"},{$set:{hobby:{movies:["A Chinese Odyssey","King of comedy"]}}})

//13.查询喜欢电影hero的文档
//MongoDB支持直接通过内嵌文档的属性进行查询，如果要查询内嵌文档则可以通过.的形式来匹配
//如果要通过内嵌文档来对文档进行查询，此时属性名必须使用引号 
db.users.find({'hobby.movies':"hero"});

//14.向tangseng中添加一个新的电影Interstellar
//$push 用于向数组中添加一个新的元素
//$addToSet 向数组中添加一个新元素 ， 如果数组中已经存在了该元素，则不会添加
db.users.update({username:"tangseng"},{$push:{"hobby.movies":"I nterstellar"}});
db.users.update({username:"tangseng"},{$addToSet:{"hobby.movies":"Interstellar"}});
db.users.find();

//15.删除喜欢beijing的用户
db.users.remove({"hobby.cities":"beijing"});

//16.删除user集合
db.users.remove({});
db.users.drop();

show dbs;

//17.向numbers中插入20000条数据 7.2s
for(var i=1 ; i<=20000 ; i++){
    db.numbers.insert({num:i});
}

db.numbers.find()

db.numbers.remove({});


//0.4s
var arr = [];

for(var i=1 ; i<=20000 ; i++){
    arr.push({num:i});
}

db.numbers.insert(arr);

```

#### 练习2

```js
//18.查询numbers中num为500的文档
db.numbers.find({num:500})

//19.查询numbers中num大于50 0的文档
db.numbers.find({num:{$gt:500}});
db.numbers.find({num:{$eq:500}});

//20.查询numbers中num小于30的文档
db.numbers.find({num:{$lt:30}});

//21.查询numbers中num大于40小于50的文档
db.numbers.find({num:{$gt:40 , $lt:50}});

//22.查询numbers中num大于19996的文档
db.numbers.find({num:{$gt:19996}});

//23.查看numbers集合中的前10条数据
db.numbers.find({num:{$lte:10}});

//limit()设置显示数据的上限
db.numbers.find().limit(10);
//在开发时，我们绝对不会执行不带条件的查询
db.numbers.find();

//24.查看numbers集合中的第11条到20条数据
/*
    分页 每页显示10条
        1-10     0
        11-20    10
        21-30    20
        。。。
        
        skip((页码-1) * 每页显示的条数).limit(每页显示的条数);
        
    skip()用于跳过指定数量的数据    
    
    MongoDB会自动调整skip和limit的位置
*/
db.numbers.find().skip(10).limit(10);

//25.查看numbers集合中的第21条到30条数据
db.numbers.find().skip(20).limit(10);

db.numbers.find().limit(10).skip(10);
```

#### 文档之间的关系

```js
/*
  文档之间的关系
    一对一（one to one）
        - 夫妻 (一个丈夫 对应 一个妻子)
        - 在MongoDB，可以通过内嵌文档的形式来体现出一对一的关系
    
    一对多（one to many）/多对一(many to one)
        - 父母 - 孩子
          用户 - 订单
          文章 - 评论
          - 也可以通过内嵌文档来映射一对多的关系
          
    
    多对多(many to many)
       - 分类 - 商品
         老师 - 学生 
    
*/
db.wifeAndHusband.insert([
    {
        name:"黄蓉",
        husband:{
            name:"郭靖"
        }
    },{
        name:"潘金莲",
        husband:{
            name:"武大郎"
        }
    }

]);

db.wifeAndHusband.find();


//一对多 用户(users) 和 订单(orders)
db.users.insert([{
    username:"swk"
    },{
    username:"zbj"
}]);

db.order.insert({
    
    list:["牛肉","漫画"],
    user_id: ObjectId("59c47e35241d8d36a1d50de0")
    
});

db.users.find()
db.order.find()

//查找用户swk的订单
var user_id = db.users.findOne({username:"zbj"})._id;
db.order.find({user_id:user_id});
  
//多对多
db.teachers.insert([
    {name:"洪七公"},
    {name:"黄药师"},
    {name:"龟仙人"}
]);

db.stus.insert([
    {
        name:"郭靖",
        tech_ids:[
            ObjectId("59c4806d241d8d36a1d50de4"),
            ObjectId("59c4806d241d8d36a1d50de5")
        ]
    },{
        name:"孙悟空",
        tech_ids:[
            ObjectId("59c4806d241d8d36a1d50de4"),
            ObjectId("59c4806d241d8d36a1d50de5"),
            ObjectId("59c4806d241d8d36a1d50de6")
        ]
    }
])

db.teachers.find()

db.stus.find()
```

#### 练习3

```json
// depe.json
{
    "_id" : ObjectId("5941f2bac1bc86928f4de49a"),
    "deptno" : 10.0,
    "dname" : "财务部",
    "loc" : "北京"
}
{
    "_id" : ObjectId("5941f2bac1bc86928f4de49b"),
    "deptno" : 20.0,
    "dname" : "办公室",
    "loc" : "上海"
}
{
    "_id" : ObjectId("5941f2bac1bc86928f4de49c"),
    "deptno" : 30.0,
    "dname" : "销售部",
    "loc" : "广州"
}
{
    "_id" : ObjectId("5941f2bac1bc86928f4de49d"),
    "deptno" : 40.0,
    "dname" : "运营部",
    "loc" : "深圳"
}

```

```json
// emp.json
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4ac"),
    "empno" : 7369.0,
    "ename" : "林冲",
    "job" : "职员",
    "mgr" : 7902.0,
    "hiredate" : ISODate("1980-12-16T16:00:00Z"),
    "sal" : 800.0,
    "depno" : 20.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4ad"),
    "empno" : 7499.0,
    "ename" : "孙二娘",
    "job" : "销售",
    "mgr" : 7698.0,
    "hiredate" : ISODate("1981-02-19T16:00:00Z"),
    "sal" : 1600.0,
    "comm" : 300.0,
    "depno" : 30.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4ae"),
    "empno" : 7521.0,
    "ename" : "扈三娘",
    "job" : "销售",
    "mgr" : 7698.0,
    "hiredate" : ISODate("1981-02-21T16:00:00Z"),
    "sal" : 1250.0,
    "comm" : 500.0,
    "depno" : 30.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4af"),
    "empno" : 7566.0,
    "ename" : "卢俊义",
    "job" : "经理",
    "mgr" : 7839.0,
    "hiredate" : ISODate("1981-04-01T16:00:00Z"),
    "sal" : 2975.0,
    "depno" : 20.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b0"),
    "empno" : 7654.0,
    "ename" : "潘金莲",
    "job" : "销售",
    "mgr" : 7698.0,
    "hiredate" : ISODate("1981-09-27T16:00:00Z"),
    "sal" : 1250.0,
    "comm" : 1400.0,
    "depno" : 30.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b1"),
    "empno" : 7698.0,
    "ename" : "西门庆",
    "job" : "经理",
    "mgr" : 7839.0,
    "hiredate" : ISODate("1981-04-30T16:00:00Z"),
    "sal" : 2850.0,
    "depno" : 30.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b2"),
    "empno" : 7782.0,
    "ename" : "柴进",
    "job" : "经理",
    "mgr" : 7839.0,
    "hiredate" : ISODate("1981-06-08T16:00:00Z"),
    "sal" : 2450.0,
    "depno" : 10.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b3"),
    "empno" : 7788.0,
    "ename" : "公孙胜",
    "job" : "分析师",
    "mgr" : 7566.0,
    "hiredate" : ISODate("1987-07-12T16:00:00Z"),
    "sal" : 3000.0,
    "depno" : 20.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b4"),
    "empno" : 7839.0,
    "ename" : "宋江",
    "job" : "董事长",
    "hiredate" : ISODate("1981-11-16T16:00:00Z"),
    "sal" : 5000.0,
    "depno" : 10.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b5"),
    "empno" : 7844.0,
    "ename" : "阎婆惜",
    "job" : "销售",
    "mgr" : 7698.0,
    "hiredate" : ISODate("1981-09-07T16:00:00Z"),
    "sal" : 1500.0,
    "comm" : 0.0,
    "depno" : 30.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b6"),
    "empno" : 7876.0,
    "ename" : "李逵",
    "job" : "职员",
    "mgr" : 7902.0,
    "hiredate" : ISODate("1987-07-12T16:00:00Z"),
    "sal" : 1100.0,
    "depno" : 20.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b7"),
    "empno" : 7900.0,
    "ename" : "武松",
    "job" : "职员",
    "mgr" : 7782.0,
    "hiredate" : ISODate("1981-12-02T16:00:00Z"),
    "sal" : 950.0,
    "depno" : 10.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b8"),
    "empno" : 7902.0,
    "ename" : "吴用",
    "job" : "分析师",
    "mgr" : 7566.0,
    "hiredate" : ISODate("1981-12-02T16:00:00Z"),
    "sal" : 3000.0,
    "depno" : 20.0
}
{
    "_id" : ObjectId("5941f5bfc1bc86928f4de4b9"),
    "empno" : 7934.0,
    "ename" : "鲁智深",
    "job" : "职员",
    "mgr" : 7782.0,
    "hiredate" : ISODate("1982-01-22T16:00:00Z"),
    "sal" : 1300.0,
    "depno" : 10.0
}

```



```js
//26.将dept和emp集合导入到数据库中
db.dept.find()
db.emp.find()

//27.查询工资小于2000的员工
db.emp.find({sal:{$lt:2000}});

//28.查询工资在1000-2000之间的员工
db.emp.find({sal:{$lt:2000 , $gt:1000}});

//29.查询工资小于1000或大于2500的员工
db.emp.find({$or:[{sal:{$lt:1000}} , {sal:{$gt:2500}}]});

//30.查询财务部的所有员工
//(depno)
var depno = db.dept.findOne({dname:"财务部"}).deptno;
db.emp.find({depno:depno});

//31.查询销售部的所有员工
var depno = db.dept.findOne({dname:"销售部"}).deptno;
db.emp.find({depno:depno});

//32.查询所有mgr为7698的所有员工
db.emp.find({mgr:7698})

//33.为所有薪资低于1000的员工增加工资400元
db.emp.updateMany({sal:{$lte:1000}} , {$inc:{sal:400}});
db.emp.find()

```

#### sort和投影

- 查询文档时，默认情况是按照_id的值进行排列（升序）
- sort()可以用来指定文档的排序的规则,sort()需要传递一个对象来指定排序规则 1表示升序 -1表示降序
  //limit skip sort 可以以任意的顺序进行调用
- db.emp.find({}).sort({sal:1,empno:-1});
- 在查询时，可以在第二个参数的位置来设置查询结果的 投影
- db.emp.find({},{ename:1 , _id:0 , sal:1});

#### Mongoose

- 之前我们开发都是使用shell来完成对数据库的操作，但是大部分情况下，我们都需要通过程序来完成对数据库的操作
- 而Mongoose就是一个可以通过node来操作MongoDB的模块
- Mongoose是一个对象文档模型（ODM）库，它对Node原生的MongoDB模块进行了进一步的优化封装，并且提供了更多的功能
- 在大多数情况下，它被用来把结构化的模块应用到一个MongoDB集合，并提供了验证和类型转换等好处

**好处**

- 可以为文档创建一个模式结构（Schema）
- 可以对模型中的对象/文档进行验证
- 数据可以通过类型转换转换为对象模型
- 可以使用中间件来与应用业务逻辑挂钩
- 比Node原生的MongDB驱动更容易

**新的对象**

- mongoose中为我们提供了几个新的对象
  - Schema （模式对象）
    - Schema 对象定义约束了数据库中的文档结构
  - Model
    - Model对象作为集合中的所有文档的表示，相当于MongoDB数据库中的集合Collection
  - Document
    - Document 表示集合中的具体文档，相当于集合中的一个具体的文档

##### 演示

```js
/*
	1.下载安装Mongoose
		npm i mongoose --save
	2.在项目中引入mongoose
		var mongoose = require("mongoose");
	3.连接MongoDB数据库
 		mongoose.connect('mongodb://数据库的ip地址:端口号/数据库名', { useMongoClient: true});
 		- 如果端口号是默认端口号（27017） 则可以省略不写

 	4.断开数据库连接(一般不需要调用)
 		- MongoDB数据库，一般情况下，只需要连接一次，连接一次以后，除非项目停止服务器关闭，否则连接一般不会断开
 			mongoose.disconnect()

	- 监听MongoDB数据库的连接状态
		- 在mongoose对象中，有一个属性叫做connection，该对象表示的就是数据库连接
			通过监视该对象的状态，可以来监听数据库的连接与断开

		数据库连接成功的事件
		mongoose.connection.once("open",function(){});

		数据库断开的事件
		mongoose.connection.once("close",function(){});

		Schema
		Model
		Document

 */

//引入
var mongoose = require("mongoose");
//连接数据库
mongoose.connect("mongodb://127.0.0.1/mongoose_test" , { useMongoClient: true});

mongoose.connection.once("open",function(){
    console.log("数据库连接成功~~~");
});

mongoose.connection.once("close",function(){
    console.log("数据库连接已经断开~~~");
});

//断开数据库连接
mongoose.disconnect();

```

![1600152601233](images/1600152601233.png)

```js
var mongoose = require("mongoose");
mongoose.connect("mongodb://127.0.0.1/mongoose_test",{useMongoClient:true});
mongoose.connection.once("open",function () {
    console.log("数据库连接成功~~~");
});

var Schema = mongoose.Schema;

var stuSchema = new Schema({
    name:String,
    age:Number,
    gender:{
        type:String,
        default:"female"
    },
    address:String

});

var StuModel = mongoose.model("student" , stuSchema);
/*
	- 有了Model，我们就可以来对数据库进行增删改查的操作了

 	Model.create(doc(s), [callback])
 	- 用来创建一个或多个文档并添加到数据库中
 	- 参数：
 		doc(s) 可以是一个文档对象，也可以是一个文档对象的数组
 		callback 当操作完成以后调用的回调函数

 	查询的：
	 Model.find(conditions, [projection], [options], [callback])
	 	- 查询所有符合条件的文档 总会返回一个数组
	 Model.findById(id, [projection], [options], [callback])
	 	- 根据文档的id属性查询文档
	 Model.findOne([conditions], [projection], [options], [callback])
	 	- 查询符合条件的第一个文档 总和返回一个具体的文档对象

 		conditions 查询的条件
 		projection 投影 需要获取到的字段
 			- 两种方式
 				{name:1,_id:0}
 				"name -_id"
 		options  查询选项（skip limit）
 				{skip:3 , limit:1}
 		callback 回调函数，查询结果会通过回调函数返回
 					回调函数必须传，如果不传回调函数，压根不会查询

 */

/*StuModel.find({name:"唐僧"},function (err , docs) {
	if(!err){
		console.log(docs);
	}
});*/

/*StuModel.find({},{name:1 , _id:0},function (err , docs) {
	if(!err){
		console.log(docs);
	}
});*/

/*StuModel.find({},"name age -_id", {skip:3 , limit:1} , function (err , docs) {
	if(!err){
		console.log(docs);
	}
});*/

/*StuModel.findOne({} , function (err , doc) {
	if(!err){
		console.log(doc);
	}
});*/

/*StuModel.findById("59c4c3cf4e5483191467d392" , function (err , doc) {
	if(!err){
		//console.log(doc);
		//通过find()查询的结果，返回的对象，就是Document，文档对象
		//Document对象是Model的实例
		console.log(doc instanceof StuModel);
	}
});*/



/*StuModel.create([
	{
		name:"沙和尚",
		age:38,
		gender:"male",
		address:"流沙河"
	}

],function (err) {
	if(!err){
		console.log(arguments);
	}
});*/


/*
	修改
 Model.update(conditions, doc, [options], [callback])
 Model.updateMany(conditions, doc, [options], [callback])
 Model.updateOne(conditions, doc, [options], [callback])
 	- 用来修改一个或多个文档
 	- 参数：
 		conditions 查询条件
 		doc 修改后的对象
 		options 配置参数
 		callback 回调函数
 Model.replaceOne(conditions, doc, [options], [callback])
* */

//修改唐僧的年龄为20
/*StuModel.updateOne({name:"唐僧"},{$set:{age:20}},function (err) {
	if(!err){
		console.log("修改成功");
	}
});*/

/*
	删除：
 Model.remove(conditions, [callback])
 Model.deleteOne(conditions, [callback])
 Model.deleteMany(conditions, [callback])
 */
/*StuModel.remove({name:"白骨精"},function (err) {
	if(!err){
		console.log("删除成功~~");
	}
});*/

/*
 Model.count(conditions, [callback])
 	- 统计文档的数量的
 */
StuModel.count({},function (err , count) {
    if(!err){
        console.log(count);
    }
});


```

```js
var mongoose = require("mongoose");
mongoose.connect("mongodb://127.0.0.1/mongoose_test",{useMongoClient:true});
mongoose.connection.once("open",function () {
	console.log("数据库连接成功~~~");
});

var Schema = mongoose.Schema;

var stuSchema = new Schema({

	name:String,
	age:Number,
	gender:{
		type:String,
		default:"female"
	},
	address:String

});

var StuModel = mongoose.model("student" , stuSchema);
/*
	Document 和 集合中的文档一一对应 ， Document是Model的实例
		通过Model查询到结果都是Document
 */

//创建一个Document
var stu = new StuModel({
	name:"奔波霸",
	age:48,
	gender:"male",
	address:"碧波潭"
});

/*
	document的方法
 		Model#save([options], [fn])
 */
/*
stu.save(function (err) {
	if(!err){
		console.log("保存成功~~~");
	}
});*/

StuModel.findOne({},function (err , doc) {
	if(!err){
		/*
		 	update(update,[options],[callback])
		 		- 修改对象
		 	remove([callback])
		 		- 删除对象

		 */
		//console.log(doc);
		/*doc.update({$set:{age:28}},function (err) {
			if(!err){
				console.log("修改成功~~~");
			}
		});*/

		/*doc.age = 18;
		doc.save();*/

		/*doc.remove(function (err) {
			if(!err){
				console.log("大师兄再见~~~");
			}
		});*/


		/*
			get(name)
				- 获取文档中的指定属性值
			set(name , value)
				- 设置文档的指定的属性值
			id
				- 获取文档的_id属性值
			 toJSON() ******
			 	- 转换为一个JSON对象

			 toObject()
			 	- 将Document对象转换为一个普通的JS对象
			 		转换为普通的js对象以后，注意所有的Document对象的方法或属性都不能使用了

		 */
		//console.log(doc.get("age"));
		//console.log(doc.age);

		//doc.set("name","猪小小");
		//doc.name = "hahaha";

		//console.log(doc._id);
		//var j = doc.toJSON();
		//console.log(j);

		//var o = doc.toObject();

		//console.log(o);

		doc = doc.toObject();

		delete doc.address;

		console.log(doc._id);

	}
});

```

