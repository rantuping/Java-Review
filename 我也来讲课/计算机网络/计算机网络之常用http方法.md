### 计算机网络之常用http方法

#### HTTP请求的常用方法有哪些

HTTP请求的常用方法有：GET方法、POST方法、HEAD方法、PUT方法、DELETE方法、CONNECT方法、OPTIONS方法、TRACE方法。

#### 1.GET:获取资源

GET方法用来请求URL指定的资源。指定的资源经服务器端解析后返回响应内容。例子：

|                             请求                             |           响应           |
| :----------------------------------------------------------: | :----------------------: |
| GET /index.html HTTP/1.1 Host: [www.hackr.cn](https://links.jianshu.com/go?to=http%3A%2F%2Fwww.hackr.cn) | 返回index.html的页面资源 |

#### 2.POST:传输实体主题

POST方法用来传输实体的主体。

|                             请求                             |               响应               |
| :----------------------------------------------------------: | :------------------------------: |
| POST /submit.cgi HTTP/1.1 [Host:www.hackr.cn](https://links.jianshu.com/go?to=Host%3Awww.hackr.cn) Content-Length:1560 | 返回submit.cgi接收数据的处理结果 |

#### 3.PUT:传输文件

PUT方法用来传输文件。就像FTP协议的文件上传一样，要求在请求报文主体中包含文件的内容，然后保存到请求URL指定的位置。不太常用。

|                             请求                             |                            响应                            |
| :----------------------------------------------------------: | :--------------------------------------------------------: |
| PUT /example.html HTTP/1.1 [Host:www.hackr.cn](https://links.jianshu.com/go?to=Host%3Awww.hackr.cn) Content-Type: text/html Content-Length: 1560 | 响应返回状态码204 No Content(比如：该html已存在于服务器上) |

#### 4.HEAD：获取报文首部

|                             请求                             |             响应             |
| :----------------------------------------------------------: | :--------------------------: |
| HEAD /index.html HTTP/1.1 [Host:www.hackr.cn](https://links.jianshu.com/go?to=Host%3Awww.hackr.cn) | 返回index.html有关的响应首部 |

#### 5.DELETE：删除文件

DELETE方法用来删除文件，是PUT的相反方法。DELETE方法按请求URL删除指定的资源。也不常用。

|                             请求                             |                             响应                             |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| DELETE /example.html HTTP/1.1 [Host:www.hackr.cn](https://links.jianshu.com/go?to=Host%3Awww.hackr.cn) | 响应返回状态码204 No Content(比如：该html已从该服务器上删除) |

#### 6.OPTIONS:询问支持的方法

OPTIONS方法用来查询针对请求URL指定的资源支持的方法。

|                             请求                             |                    响应                     |
| :----------------------------------------------------------: | :-----------------------------------------: |
| OPTIONS * HTTP/1.1 [Host:www.hackr.cn](https://links.jianshu.com/go?to=Host%3Awww.hackr.cn) | HTTP/1.1 200 OK Allow:GET,POST,HEAD,OPTIONS |

#### 7.TRACE:追踪路径

TRACE方法是让Web服务器端将之前的请求通信环回给客户端方法。客户端可以用TRACE方法查询发送出去的请求时怎样被加工修改的。不常用，还容易引发XST攻击

|                     请求                      |                             响应                             |
| :-------------------------------------------: | :----------------------------------------------------------: |
| TRACE / HTTP/1.1 Host:hackr.cn Max-Forwards:2 | HTTP:/1.1 200 OK Content-Type:message/http Content-Length:1024 TRACE / HTTP/1.1 Host:hackr.cn Max-Forwards:2 |

#### 8.CONNECT:要求用隧道协议链接代理

CONNECT方法要求在与代理服务器通信时建立隧道，实现用隧道协议进行TCP通信。主要使用SSL和TSL协议把通信内容加密后经网络隧道传输。

|                           请求                           |               响应                |
| :------------------------------------------------------: | :-------------------------------: |
| CONNECT proxy.hackr.cn:8080 HTTP/1.1 Host:proxy.hacky.cn | HTTP/1.1 200 OK(之后进入网络隧道) |

#### 总结下HTTP1.1和HTTP1.0支持的方法

|  请求   |          说明          | 支持的HTTP协议版本 |
| :-----: | :--------------------: | :----------------: |
|   GET   |        获取资源        |      1.0、1.1      |
|  POST   |      传输实体主体      |      1.0、1.1      |
|   PUT   |        传输文件        |      1.0、1.1      |
|  HEAD   |      获得报文首部      |      1.0、1.1      |
| DELETE  |        删除文件        |      1.0、1.1      |
| OPTIONS |     询问支持的方法     |        1.1         |
|  TRACE  |        追踪路径        |        1.1         |
| CONNECT | 要求用隧道协议连接代理 |        1.1         |
|  LINK   |  建立和资源之间的联系  |        1.0         |
| UNLINK  |      断开连接关系      |        1.0         |

#### LINK和UNLINK已被HTTP/1.1废弃。

本篇只展示一些常用的方法，关于其他一些扩展方法，不再做展示