### JavaReview

#### 更新必看

- 最新的专栏文章已经更新到语雀，这是一个系列的专题，从各个方面讲解Java，欢迎拜访：https://www.yuque.com/icanci

- 更新的部分有：Base-Java、Java集合框架源码、设计模式、Java函数式编程、JUC、Tomcat8.5.x、JVM 等等

#### 介绍
自我学习Java中的复盘和源码阅读解析。

#### 软件架构

是自我学习的总结，经验与教训。

如果您发现有错误的地方，可以添加  Issue 。


#### 安装教程

克隆到本地即可查看。

#### 使用说明

本项目遵循  [木兰宽松许可证, 第2版]( https://license.coscl.org.cn/MulanPSL2/ ) 开源协议 

开源不易，感谢Star。

如果您在阅读过程中发现错误的地方，欢迎指正。

#### 软件目录结构树

##### Java基础篇

- [Java-IO-基础](./Java基础篇/Java-IO-基础.md)
- Java-IO-进阶-[未完成]
- [Java-常用类-Arrays](./Java基础篇/Java-常用类-Arrays.md)
- [Java-常用类-Collections](./Java-常用类-Collections.md)
- [Java-常用类-Date](./Java基础篇/Java-常用类-Date.md)
- [Java-常用类-Math](./Java基础篇/Java-常用类-Math.md)
- [Java-常用类-Object](./Java基础篇/Java-常用类-Object.md)
- [Java-常用类-Random](./Java基础篇/Java-常用类-Random.md)
- [Java-常用类-Runtime](./Java基础篇/Java-常用类-Runtime.md)
- [Java-常用类-String](./Java基础篇/Java-常用类-String.md)
- [Java-常用类-StringBuilder和StringBuffer](./Java基础篇/Java-常用类-StringBuilder和StringBuffer.md)
- [Java-常用类-System](./Java基础篇/Java-常用类-System.md)
- [Java-常用类-UUID](./Java基础篇/Java-常用类-UUID.md)
- [Java-多线程-JUC-并发编程-基础](./Java基础篇/Java-多线程-JUC-并发编程-基础.md)
- Java-多线程-JUC-并发编程-进阶-[未完成]
- [Java-多线程-基础详解](./Java基础篇/Java-多线程-基础详解.md)
- [Java-多线程-线程池](./Java基础篇/Java-多线程-线程池.md)
- [Java-多线程-增强篇-FutureTask](./Java基础篇/Java-多线程-增强篇-FutureTask.md)
- [Java-多线程-增强篇-LongAdder](./Java基础篇/Java-多线程-增强篇-LongAdder.md)
- [Java-多线程-增强篇-四种引用类型](./Java基础篇/Java-多线程-增强篇-四种引用类型.md)
- [Java-多线程-增强篇-锁和提升第1篇](./Java基础篇/Java-多线程-增强篇-锁和提升第1篇.md)
- [Java-多线程-增强篇-锁和提升第2篇](./Java基础篇/Java-多线程-增强篇-锁和提升第2篇.md)
- [Java-多线程-增强篇-锁和提升第3篇](./Java基础篇/Java-多线程-增强篇-锁和提升第3篇.md)
- [Java-多线程-增强篇-阻塞队列](./Java基础篇/Java-多线程-增强篇-阻塞队列.md)
- [Java-集合框架-哈希表-HashMap](./Java基础篇/Java-集合框架-哈希表-HashMap.md)
- [Java-集合框架-红黑树之前置知识](./Java基础篇/Java-集合框架-红黑树之前置知识.md)
- [Java-集合框架-HashMap底层-红黑树深度解读](./Java基础篇/Java-集合框架-HashMap底层-红黑树深度解读.md)
- [Java-集合框架-哈希表-HashMap-再挖掘](./Java基础篇/Java-集合框架-哈希表-HashMap-再挖掘.md)
- [Java-集合框架-TreeMap](./Java基础篇/Java-集合框架-TreeMap.md)
- [Java-集合框架-TreeSet](./Java基础篇/Java-集合框架-TreeSet.md)
- [Java-集合框架-哈希表-LinkedHashMap](./Java基础篇/Java-集合框架-哈希表-LinkedHashMap.md)
- [Java-集合框架-哈希集合-HashSet](./Java基础篇/Java-集合框架-哈希集合-HashSet.md)
- [Java-集合框架-链表-LinkedList](./Java基础篇/Java-集合框架-链表-LinkedList.md)
- [Java-集合框架-数组-ArrayList](./Java基础篇/Java-集合框架-数组-ArrayList.md)
- [java-网络编程-基础](./Java基础篇/java-网络编程-基础.md)
- java-网络编程-进阶-[未完成]
- [Java-异常-基础](./Java基础篇/Java-异常-基础.md)
- [Java-异常-进阶-OOM异常](./Java基础篇/Java-异常-进阶-OOM异常.md)
- [java-注解和反射](./Java基础篇/java-注解和反射.md)
- [JDK-新特性-Java4](./Java基础篇/JDK-新特性-Java4.md)
- [JDK-新特性-Java5](./Java基础篇/JDK-新特性-Java5.md)
- [JDK-新特性-Java6](./Java基础篇/JDK-新特性-Java6.md)
- [JDK-新特性-Java7](./Java基础篇/JDK-新特性-Java7.md)
- [JDK-新特性-Java8](./Java基础篇/JDK-新特性-Java8.md)
- JDK-新特性-Java9 [未完成]
- JDK-新特性-Java10-[未完成]
- JDK-新特性-Java11-[未完成]
- JDK-新特性-Java12-[未完成]
- JDK-新特性-Java13-[未完成]
- JDK-新特性-Java14-[未完成]
- [阅读JDK源码环境搭建](./Java基础篇/阅读JDK源码环境搭建.md)

##### Java进阶篇

- [Java-底层建筑-JVM-第1篇-开篇概述](./Java进阶篇/Java-底层建筑-JVM-第1篇-开篇概述.md)
- [Java-底层建筑-JVM-第2篇-类加载子系统](./Java进阶篇/Java-底层建筑-JVM-第2篇-类加载子系统.md)
- [Java-底层建筑-JVM-第3篇-运行时数据区概述和线程](./Java进阶篇/Java-底层建筑-JVM-第3篇-运行时数据区概述和线程.md)
- [Java-底层建筑-JVM-第4篇-程序计数器](./Java进阶篇/Java-底层建筑-JVM-第4篇-程序计数器.md)
- [Java-底层建筑-JVM-第5篇-虚拟机栈](./Java进阶篇/Java-底层建筑-JVM-第5篇-虚拟机栈.md)
- [Java-底层建筑-JVM-第6篇-本地方法接口](./Java进阶篇/Java-底层建筑-JVM-第6篇-本地方法接口.md)
- [Java-底层建筑-JVM-第7篇-本地方法栈](./Java进阶篇/Java-底层建筑-JVM-第7篇-本地方法栈.md)
- [Java-底层建筑-JVM-第8篇-堆](./Java进阶篇/Java-底层建筑-JVM-第8篇-堆.md)
- [Java-底层建筑-JVM-第9篇-方法区](./Java进阶篇/Java-底层建筑-JVM-第9篇-方法区.md)
- [Java-底层建筑-JVM-第10篇-对象的实例化内存布局与访问定位](./Java进阶篇/Java-底层建筑-JVM-第10篇-对象的实例化内存布局与访问定位.md)
- [Java-底层建筑-JVM-第11篇-直接内存](./Java进阶篇/Java-底层建筑-JVM-第11篇-直接内存.md)
- [Java-底层建筑-JVM-第12篇-执行引擎](./Java进阶篇/Java-底层建筑-JVM-第12篇-执行引擎.md)
- [Java-底层建筑-JVM-第13篇-StringTable](./Java进阶篇/Java-底层建筑-JVM-第13篇-StringTable.md)
- [Java-底层建筑-JVM-第14篇-垃圾回收概述](./Java进阶篇/Java-底层建筑-JVM-第14篇-垃圾回收概述.md)
- [Java-底层建筑-JVM-第15篇-垃圾回收相关算法](./Java进阶篇/Java-底层建筑-JVM-第15篇-垃圾回收相关算法.md)
- [Java-底层建筑-JVM-第16篇-垃圾回收相关概念](./Java进阶篇/Java-底层建筑-JVM-第16篇-垃圾回收相关概念.md)
- [Java-底层建筑-JVM-第17篇-垃圾回收器](./Java进阶篇/Java-底层建筑-JVM-第17篇-垃圾回收器.md)
- [Java-底层建筑-JVM-第18篇-Class文件](./Java进阶篇/Java-底层建筑-JVM-第18篇-Class文件.md)
- [Java-底层建筑-JVM-第19篇-字节码指令集与解析举例](./Java进阶篇/Java-底层建筑-JVM-第19篇-字节码指令集与解析举例.md)
- [Java-底层建筑-JVM-第20篇-类的加载过程详解](./Java进阶篇/Java-底层建筑-JVM-第20篇-类的加载过程详解.md)
- [Java-底层建筑-JVM-第21篇-再谈类的加载器](./Java进阶篇/Java-底层建筑-JVM-第21篇-再谈类的加载器.md)

##### Java加餐篇

- [Java加餐篇之Java-SPI机制](./Java加餐篇/Java加餐篇之Java-SPI机制.md)

##### MySQL篇

- [MySQL-基础篇-第0篇-MySQL错误码对照](./MySQL篇/MySQL-基础篇-第0篇-MySQL错误码对照.md)
- [MySQL-基础篇-第1篇-概述与基本查询语句](./MySQL篇/MySQL-基础篇-第1篇-概述与基本查询语句.md)
- [MySQL-基础篇-第2篇-单行函数](./MySQL篇/MySQL-基础篇-第2篇-单行函数.md)
- [MySQL-基础篇-第3篇-高级查询](./MySQL篇/MySQL-基础篇-第3篇-高级查询.md)
- [MySQL-基础篇-第4篇-数据库设计](./MySQL篇/MySQL-基础篇-第4篇-数据库设计.md)
- [MySQL-基础篇-第5篇-视图和Limit语句](./MySQL篇/MySQL-基础篇-第5篇-视图和Limit语句.md)
- [MySQL-基础篇-第6篇-函数以及存储过程和触发器](./MySQL篇/MySQL-基础篇-第6篇-函数以及存储过程和触发器.md)
- [MySQL-基础篇-第7篇-46道题目补充测试练习](./MySQL篇/MySQL-基础篇-第7篇-46道题目补充测试练习.md)
- [MySQL-进阶篇-第1篇-MySQL索引基础篇](./MySQL篇/MySQL-进阶篇-第1篇-MySQL索引基础篇.md)
- [MySQL-进阶篇-第2篇-MySQL索引高级篇](./MySQL篇/MySQL-进阶篇-第2篇-MySQL索引高级篇.md)
- [MySQL-进阶篇-第3篇-MySQL架构和内部模块](./MySQL篇/MySQL-进阶篇-第3篇-MySQL架构和内部模块.md)
- [MySQL-进阶篇-第4篇-MySQL索引彻底刨析](./MySQL篇/MySQL-进阶篇-第4篇-MySQL索引彻底刨析.md)
- [MySQL-进阶篇-第5篇-MySQL事务与锁机制](./MySQL篇/MySQL-进阶篇-第5篇-MySQL事务与锁机制.md)
- [MySQL-进阶篇-第6篇-MySQL优化思路和工具](./MySQL篇/MySQL-进阶篇-第6篇-MySQL优化思路和工具.md)

##### SpringBoot整合篇

- [SpringBoot的项目属性配置](./SpringBoot整合篇/SpringBoot的项目属性配置.md)
- [SpringBoot基础学习篇](./SpringBoot整合篇/SpringBoot基础学习篇.md)
- [SpringBoot使用异步和邮件和定时任务](./SpringBoot整合篇/SpringBoot使用异步和邮件和定时任务.md)
- [SpringBoot事务配置管理](./SpringBoot整合篇/SpringBoot事务配置管理.md)
- [SpringBoot整合fastjson和jackson](./SpringBoot整合篇/SpringBoot整合fastjson和jackson.md)
- [SpringBoot整合knife4j](./SpringBoot整合篇/SpringBoot整合knife4j.md)
- [SpringBoot整合MyBatis](./SpringBoot整合篇/SpringBoot整合MyBatis.md)
- [SpringBoot整合Redis](./SpringBoot整合篇/SpringBoot整合Redis.md)
- [SpringBoot整合slf4j进行日志记录](./SpringBoot整合篇/SpringBoot整合slf4j进行日志记录.md)
- [SpringBoot整合Swagger](./SpringBoot整合篇/SpringBoot整合Swagger.md)
- [SpringBoot整合Thymeleaf模板引擎](./SpringBoot整合篇/SpringBoot整合Thymeleaf模板引擎.md)
- [SpringBoot中的MVC支持](./SpringBoot整合篇/SpringBoot中的MVC支持.md)
- [SpringBoot中的切面AOP处理](./SpringBoot整合篇/SpringBoot中的切面AOP处理.md)
- [SpringBoot中的全局异常处理](./SpringBoot整合篇/SpringBoot中的全局异常处理.md)
- [SpringBoot中集成Shiro初步集成](./SpringBoot整合篇/SpringBoot中集成Shiro初步集成.md)
- SpringBoot中集成Shiro实战-[未完成]
- [SpringBoot中集成SpringSecurity](./SpringBoot整合篇/SpringBoot中集成SpringSecurity.md)
- [SpringBoot中使用监听器](./SpringBoot整合篇/SpringBoot中使用监听器.md)
- [SpringBoot中使用拦截器](./SpringBoot整合篇/SpringBoot中使用拦截器.md)
- [如何查看开源项目](./SpringBoot整合篇/SpringBoot整合篇/如何查看开源项目.md)

##### SSM三大框架篇

- [SSM-框架核心思想解读与前置知识](./SSM三大框架篇/SSM-框架核心思想解读与前置知识.md)

###### 基础支持层

- [SSM-Mybatis3-源码解读-第1篇-开篇](./SSM三大框架篇/SSM-Mybatis3-源码解读-第1篇-开篇.md)

- [SSM-Mybatis3-源码解读-第2篇-框架目录结构](./SSM三大框架篇/SSM-Mybatis3-源码解读-第2篇-框架目录结构.md)

- [SSM-Mybatis3-源码解读-第3篇-解析器模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第3篇-解析器模块.md)

- [SSM-Mybatis3-源码解读-第4篇-反射模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第4篇-反射模块.md)

- [SSM-Mybatis3-源码解读-第5篇-异常模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第5篇-异常模块.md)

- [SSM-Mybatis3-源码解读-第6篇-数据源模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第6篇-数据源模块.md)

- [SSM-Mybatis3-源码解读-第7篇-事务模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第7篇-事务模块.md)

- [SSM-Mybatis3-源码解读-第8篇-缓存模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第8篇-缓存模块.md)

- [SSM-Mybatis3-源码解读-第9篇-类型模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第9篇-类型模块.md)

- [SSM-Mybatis3-源码解读-第10篇-IO模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第10篇-IO模块.md)

- [SSM-Mybatis3-源码解读-第11篇-日志模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第11篇-日志模块.md)

- [SSM-Mybatis3-源码解读-第12篇-注解模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第12篇-注解模块.md)

- [SSM-Mybatis3-源码解读-第13篇-Binding模块](./SSM三大框架篇/SSM-Mybatis3-源码解读-第13篇-Binding模块.md)

###### 核心处理层

- TODO

###### 接口层

- TODO

##### 测试篇

- [JMeter接口压力测试打造高性能服务](./测试篇/JMeter接口压力测试打造高性能服务.md)

##### 常用基础小工具篇

- [Apache-POI-&-EasyExcel](./常用基础小工具篇/Apache-POI-&-EasyExcel.md)
- [阿里云短信验证码服务](./常用基础小工具篇/阿里云短信验证码服务.md)

##### 纯手写篇

- 剑指Offer-复盘
- 设计模式-复盘 
- [手写数据库连接池-Kangaroo-Pool](./纯手写篇/手写数据库连接池-Kangaroo-Pool.md)

##### 读书笔记

- [《Web信息结构设计大型网站》读后感](./读书笔记/《Web信息结构设计大型网站》读后感.md)
- [《编写高质量Java代码的151个建议》读后感](./读书笔记/《编写高质量Java代码的151个建议》读后感.md)
- [《大型网站技术架构：核心原理与案例分析》读后感](./读书笔记/《大型网站技术架构：核心原理与案例分析》读后感.md)
- [《富人为什么富有：富人生活的十二个法则》读后感](./读书笔记/《富人为什么富有：富人生活的十二个法则》读后感.md)
- [《穷爸爸富爸爸》读后感](./读书笔记/《穷爸爸富爸爸》读后感.md)
- [《小强升职记》读后感](./读书笔记/《小强升职记》读后感.md)
- [领域驱动设计-笔记](./读书笔记/领域驱动设计-笔记.md)

##### 分布式与微服务篇

- [微服务架构](./分布式与微服务篇/微服务架构.md)

- [Dubbo+Zookeeper+SpringBoot入门篇](./分布式与微服务篇/Dubbo+Zookeeper+SpringBoot入门篇.md)
- SpringCloud-[未完成]

##### 汇编原理学习

- [狂神计算机原理](./汇编原理学习/狂神计算机原理.md)

##### 计算机理论基础篇

- **编译原理**
  - [编译原理](./计算机理论基础篇/编译原理/编译原理.md)
- **操作系统**
  - [操作系统](./计算机理论基础篇/操作系统/操作系统.md)
- **计算机网络**
  - [计算机网络](./计算机理论基础篇/计算机网络/计算机网络.md)
- **计算机组成原理**
  - [计算机组成原理](./计算机理论基础篇/计算机组成原理/计算机组成原理.md)

##### 开发工具篇

- [Git的学习和使用](./开发工具篇/Git的学习和使用.md)
- Gradle的学习和使用-[未完成]
- Maven安装以及配置源和本地仓库 - 图片在码云不显示可以在本地查看 #TODO 图片更换
- Maven高级应用 - 图片在码云不显示可以在本地查看 #TODO 图片更换

##### 课外杂项篇

- [大厂面试题部分-答案没写](./课外杂项篇/大厂面试题部分-答案没写.md)
- [理财进阶](./课外杂项篇/理财进阶.md)

##### 密码学篇

- [密码学](./密码学篇/密码学.md)

##### 面试篇

- **MySQL篇**
- [MySQL面试题-第1篇-基本概念](./面试篇/MySQL面试题-第1篇-基本概念.md)
- [MySQL面试题-第2篇-索引](./面试篇/MySQL面试题-第2篇-索引.md)
- [MySQL面试题-第3篇-事务](./面试篇/MySQL面试题-第3篇-事务.md)
- [MySQL面试题-第4篇-存储引擎](./面试篇/MySQL面试题-第4篇-存储引擎.md)
- [MySQL面试题-第5篇-优化](./面试篇/MySQL面试题-第5篇-优化.md)
- [MySQL面试题-第6篇-数据库锁](./面试篇/MySQL面试题-第6篇-数据库锁.md)
- [MySQL面试题-第7篇-其他](./面试篇/MySQL面试题-第7篇-其他.md)

##### 前端框架学习篇

- [Nodejs基础学习](./前端框架学习篇/Nodejs基础学习.md)
- [Vue-第1篇-基础学习](./前端框架学习篇/Vue-第1篇-基础学习.md)
- [Vue-第2篇-组件化编码](./前端框架学习篇/Vue-第2篇-组件化编码.md)
- [Vue-第3篇-ajax](./前端框架学习篇/Vue-第3篇-ajax.md)
- [Vue-第4篇-ui组件库](./前端框架学习篇/Vue-第4篇-ui组件库.md)
- [Vue-第5篇-路由router](./前端框架学习篇/Vue-第5篇-路由router.md)

##### 全科目电子书

- [小猪童鞋整理全科目有PDF电子书集合](./全科目电子书/小猪童鞋整理全科目有PDF电子书集合.md)

##### 软件安装和系统配置

- Linux服务器安装和部署
  - [虚拟机安装多节点教程](./软件安装和系统配置/Linux服务器安装和部署/虚拟机安装多节点教程.md)
- 我的软件目录
  - [软件目录](./软件安装和系统配置/我的软件目录/软件目录.md)

##### 设计模式篇

- [设计模式绪论与七大原则](./设计模式篇/设计模式绪论与七大原则.md)
- [创建型模式-单例设计模式](./设计模式篇/创建型模式-单例设计模式.md)
- [创建型模式-工厂模式](./设计模式篇/创建型模式-工厂模式.md)

##### 数据结构与算法篇

- [数据结构和算法](./数据结构与算法篇/数据结构和算法.md)

##### 我也来讲课

- **锁**
  - [Java中的锁和应用场景](./我也来讲课/锁/Java中的锁和应用场景.md)
- **计算机网络**
  - [计算机网络之100个网络基础知识普及](./我也来讲课/计算机网络/计算机网络之100个网络基础知识普及.md)
  - [计算机网络之GET和POST的区别-cookie与session-http的状态码](./我也来讲课/计算机网络/计算机网络之GET和POST的区别-cookie与session-http的状态码.md)
  - [计算机网络之https与http的区别与详解](./我也来讲课/计算机网络/计算机网络之https与http的区别与详解.md)
  - [计算机网络之TCP滑动窗口机制](./我也来讲课/计算机网络/计算机网络之TCP滑动窗口机制.md)
  - [计算机网络之tcp长连接心跳包机制](./我也来讲课/计算机网络/计算机网络之tcp长连接心跳包机制.md)
  - [计算机网络之常用http方法](./我也来讲课/计算机网络/计算机网络之常用http方法.md)
  - [计算机网络之滑动窗口下产生的粘包问题及其解决方案](./我也来讲课/计算机网络/计算机网络之滑动窗口下产生的粘包问题及其解决方案.md)
  - [计算机网络之浏览器输入url地址回车之后发生了什么](./我也来讲课/计算机网络/计算机网络之浏览器输入url地址回车之后发生了什么.md)
  - [计算机网络之拥塞控制(慢启动算法和拥塞避免算法)](./我也来讲课/计算机网络/计算机网络之拥塞控制(慢启动算法和拥塞避免算法).md)

##### 运维篇

- [Linux阿里云真实环境学习](./运维篇/Linux阿里云真实环境学习.md)
- 宝塔部署项目 - 图片在码云不显示可以在本地查看 #TODO 图片更换
- [Linux阿里云-Git-GitLab-jdk-maven-nexus-mysql-sonarqube-Jenkins学习](./运维篇/Linux阿里云-Git-GitLab-jdk-maven-nexus-mysql-sonarqube-Jenkins学习.md)
- [Linux阿里云-Nginx学习](./运维篇/Linux阿里云-nginx学习.md)

##### 职场篇

- [职场上同事不会给你说的10个潜规则！](./职场篇/职场上同事不会给你说的10个潜规则！.md)
- [为什么曾经优秀的人突然变得平庸？](./职场篇/为什么曾经优秀的人突然变得平庸？.md)
- [从锋芒毕露到道法内敛](./职场篇/从锋芒毕露到道法内敛.md)

##### 中间件篇

- [ElasticSerach详解](./中间件篇/ElasticSerach详解.md)
- [Redis详解](./中间件篇/Redis详解.md)
- [MongoDB入门](./中间件篇/MongoDB入门.md)
- [RocketMQ4.X详解](./中间件篇/RocketMQ4.X详解.md)

##### 自己的软件安装教程

- [Linux软件安装](./自己的软件安装教程/Linux软件安装.md)

##### 参与贡献

- Fork 本仓库

- 新建 Feat_xxx 分支

- 提交代码

- 新建 Pull Request

---

**感谢您的Star、Fork和宝贵的建议**