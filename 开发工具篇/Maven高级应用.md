##### Maven高级应用

###### 假如你还有没有安装 Maven 这里有安装的步骤嗷 [@安装Maven](https://www.jianshu.com/p/ea5674fbb074)

###### 我的环境
idea 2019.1
JDK 1.8
我的 JDK 版本
![我的JDK版本](https://upload-images.jianshu.io/upload_images/19921157-682f4ef8dc80179d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
我的 maven 版本  Maven 3.5.4
![maven版本](https://upload-images.jianshu.io/upload_images/19921157-1ac3bfbc74ba0185.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
在安装Maven之前需要安装JDK 和下载 Maven  安装Maven和JDK的步骤在文章顶部的链接里面有 需要的小伙伴可以看一下
Lombok插件 这里是使用和安装链接(包括idea和eclipse)  [@简单粗暴节省JavaBean代码插件 Lombok.jar](https://www.jianshu.com/p/2472e28706a2)

***
###### Maven的高级应用
maven高级应用：
1，maven基础回顾。
2，maven传统的web工程做一个数据查询操作。
3，maven工程拆分与聚合的思想。
4，把第二阶段做好的web工程修改成maven拆分与聚合的形式。
5，私服[远程仓库]。
6，如何安装第三方jar包。[把第三方jar包安装到本地仓库，把第三方jar包安装到私服。]

###### maven基础知识回顾：
maven是一个项目管理工具。
依赖管理：
maven对项目中jar包的管理过程。传统工程我们直接把jar包放置在项目中。
maven工程真正的jar包放置在仓库中，项目中只用放置jar包的坐标。

###### 仓库的种类：
本地仓库，远程仓库[私服]，中央仓库。
仓库之间的关系：当我们启动一个maven工程的时候，maven工程会通过pom文件中jar包的坐标去本地仓库找对应jar包。
默认情况下，如果本地仓库没有对应jar包，maven工程会自动去中央仓库下载jar包到本地仓库。
		在公司中，如果本地没有对应jar包，会先从私服下载jar包，
		如果私服没有jar包，可以从中央仓库下载，也可以从本地上传。
一键构建：maven自身集成了tomcat插件，可以对项目进行编译，测试，打包，安装，发布等操作。

###### maven常用命令：
clean，compile，test，package，install，deploy。

###### maven三套生命周期：
清理生命周期，默认生命周期，站点生命周期。

###### 首先新建一个Maven项目 现在 webapp 项目
pom.xml删除到下面配置  (注:但是我删除了之后出现问题 然后又把本地的仓库删除 重新导入 但是还是不行所以我就没有删除)

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>cn.icanci</groupId>
    <artifactId>maven</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>war</packaging>
</project>
```
修改web.xml
```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app xmlns="http://java.sun.com/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
                             http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         version="3.0">

</web-app>
```
导入依赖jar包 测试
```xml
<dependencies>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>5.2.2.RELEASE</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-beans</artifactId>
        <version>4.2.4.RELEASE</version>
    </dependency>
</dependencies>
```
此时的依赖 
![此时的依赖](https://upload-images.jianshu.io/upload_images/19921157-4ee2ffc4827aa532.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

但是修改位置之后

```xml
<dependencies>
    <groupId>org.springframework</groupId>
    <artifactId>spring-beans</artifactId>
    <version>4.2.4.RELEASE</version>
    </dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>5.2.2.RELEASE</version>
</dependency>
<dependency>
    </dependencies>
```
此时依赖就换了

![此时依赖就换了](https://upload-images.jianshu.io/upload_images/19921157-0465e42001ca9d98.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
其中就有 Maven 的 jar 包冲突 那么就必须解决 jar 冲突问题

###### 解决方法1
第一声明优先原则  那个jar包坐标靠上的位置,那么这个jar就是优先声明的 先声明的jar包坐标下的依赖包 可以优先进入到项目中

###### 解决方法2
maven导入jar包的一些概念
什么是直接依赖:项目中直接导入的jar包,就是项目的直接依赖包
什么是传递依赖:项目中没有直接导入的jar包,可以通过依赖jar包导进去
路径近者优先原则,直接依赖路径比传递依赖路径近.那么最终项目中进入的jar包就是路径近的直接路径
###### 解决方法3
当我们要排除某个依赖包的时候,在配置 exclusions 标签的时候 内部可以不写版本号 因为此时依赖包使用的版本和默认版本是一致的  如下依赖的写法

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-beans</artifactId>
        <version>4.2.4.RELEASE</version>
        <exclusions>
            <exclusion>
                <artifactId>spring-core</artifactId>
                <groupId>org.springframework</groupId>
            </exclusion>
        </exclusions>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>5.2.2.RELEASE</version>
    </dependency>
</dependencies>
```
此时 spring-core  4.2.4 就被排除 所以就选择 5.2.2 的版本 

![被排除了](https://upload-images.jianshu.io/upload_images/19921157-dcb1630700bba501.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

###### 此处推荐使用第三种

搭配好的依赖
```xml
<!-- 统一管理jar包版本 -->
<properties>
    <spring.version>5.0.2.RELEASE</spring.version>
    <slf4j.version>1.6.6</slf4j.version>
    <log4j.version>1.2.12</log4j.version>
    <shiro.version>1.2.3</shiro.version>
    <mysql.version>5.1.6</mysql.version>
    <mybatis.version>3.4.5</mybatis.version>
    <spring.security.version>5.0.1.RELEASE</spring.security.version>
</properties>

<!-- 锁定jar包版本 -->
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>${mybatis.version}</version>
        </dependency>
    </dependencies>
</dependencyManagement>

<!-- 项目依赖jar包 -->
<dependencies>
    <!-- spring -->
    <dependency>
        <groupId>org.aspectj</groupId>
        <artifactId>aspectjweaver</artifactId>
        <version>1.6.8</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aop</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context-support</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-web</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-orm</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-beans</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-core</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-tx</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>${mysql.version}</version>
    </dependency>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>javax.servlet-api</artifactId>
        <version>3.1.0</version>
        <scope>provided</scope>
    </dependency>
    <dependency>
        <groupId>javax.servlet.jsp</groupId>
        <artifactId>jsp-api</artifactId>
        <version>2.0</version>
        <scope>provided</scope>
    </dependency>
    <dependency>
        <groupId>jstl</groupId>
        <artifactId>jstl</artifactId>
        <version>1.2</version>
    </dependency>
    <!-- log start -->
    <dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>${log4j.version}</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${slf4j.version}</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-log4j12</artifactId>
        <version>${slf4j.version}</version>
    </dependency>
    <!-- log end -->
    <dependency>
        <groupId>org.mybatis</groupId>
        <artifactId>mybatis</artifactId>
        <version>${mybatis.version}</version>
    </dependency>
    <dependency>
        <groupId>org.mybatis</groupId>
        <artifactId>mybatis-spring</artifactId>
        <version>1.3.0</version>
    </dependency>
    <dependency>
        <groupId>c3p0</groupId>
        <artifactId>c3p0</artifactId>
        <version>0.9.1.2</version>
        <type>jar</type>
        <scope>compile</scope>
    </dependency>
    <dependency>
        <groupId>com.github.pagehelper</groupId>
        <artifactId>pagehelper</artifactId>
        <version>5.1.2</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-web</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-config</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-core</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-taglibs</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid</artifactId>
        <version>1.0.9</version>
    </dependency>
</dependencies>
<!-- 添加tomcat7插件 -->
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.tomcat.maven</groupId>
            <artifactId>tomcat7-maven-plugin</artifactId>
            <version>2.2</version>
        </plugin>
    </plugins>
</build>
```
我自己配置的 pom.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>cn..icanci</groupId>
    <artifactId>maven_test</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>war</packaging>

    <name>maven_test Maven Webapp</name>
    <!-- FIXME change it to the project's website -->
    <url>http://www.example.com</url>

    <!-- 统一管理jar包版本 -->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <spring.version>5.0.2.RELEASE</spring.version>
        <slf4j.version>1.6.6</slf4j.version>
        <log4j.version>1.2.12</log4j.version>
        <shiro.version>1.2.3</shiro.version>
        <mysql.version>5.1.6</mysql.version>
        <mybatis.version>3.4.5</mybatis.version>
        <spring.security.version>5.0.1.RELEASE</spring.security.version>
    </properties>
    <!-- 锁定jar包版本 -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-web</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-webmvc</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-tx</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-test</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mybatis</groupId>
                <artifactId>mybatis</artifactId>
                <version>${mybatis.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>
    <!-- 项目依赖jar包 -->
    <dependencies>
        <!-- spring -->
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.6.8</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context-support</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-orm</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>${mysql.version}</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>jstl</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>
        <!-- log start -->
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>${log4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <!-- log end -->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>${mybatis.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>1.3.0</version>
        </dependency>
        <dependency>
            <groupId>c3p0</groupId>
            <artifactId>c3p0</artifactId>
            <version>0.9.1.2</version>
            <type>jar</type>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>com.github.pagehelper</groupId>
            <artifactId>pagehelper</artifactId>
            <version>5.1.2</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-web</artifactId>
            <version>${spring.security.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
            <version>${spring.security.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-core</artifactId>
            <version>${spring.security.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-taglibs</artifactId>
            <version>${spring.security.version}</version>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.0.9</version>
        </dependency>
    </dependencies>
    <build>
        <finalName>maven_test</finalName>
        <pluginManagement><!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
            <plugins>
                <plugin>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>3.1.0</version>
                </plugin>
                <!-- see http://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_war_packaging -->
                <plugin>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>3.0.2</version>
                </plugin>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.8.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.22.1</version>
                </plugin>
                <plugin>
                    <artifactId>maven-war-plugin</artifactId>
                    <version>3.2.2</version>
                </plugin>
                <plugin>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>2.5.2</version>
                </plugin>
                <plugin>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>2.8.2</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
</project>
```
###### Maven项目是可以分父子依赖关系的  也就是 上述 pom.xml 中  [锁定jar包版本] 的作用
凡是依赖别的项目后,拿到的别的项目的依赖包 ,都属于传递依赖
比如 : 当前A项目,被B项目依赖,那么我们A项目中所有的jar都会传递到B项目中
B项目开发者,如果再在B项目中引入一套SSM框架的jar包 ,对于B项目是直接依赖
那么直接依赖的jar包就会把A项目传递的jar覆盖掉 
为了防止以上情况的出现 我们可以把A项目中的主要jar包的坐标锁住,那么在其他依赖该项目的项目中,即使又同名jar直接依赖,也不会覆盖掉 

***
##### 现在开始写一些案例
在 pom.xml 里面增加新的依赖   
[新的依赖 安装和使用以及作用看这里] Lombok插件 这里是使用和安装链接(包括idea和eclipse)  [@简单粗暴节省JavaBean代码插件 Lombok.jar](https://www.jianshu.com/p/2472e28706a2)

```xml
<!-- 其他非SSM必须的Jar依赖-->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.8</version>
    <scope>provided</scope>
</dependency>
```
###### 编写Dao层代码 
Items.java  

```Java
package cn.icanci.domain;

import lombok.Data;

import java.util.Date;

/**
 * @Author: icanci
 * @ProjectName: maven_test
 * @PackageName: cn.icanci.domain
 * @Date: Created in 2020/1/16 10:15
 * @ClassAction: Items 的实体类
 */
@Data
public class Items {
    private Integer id;
    private String name;
    private Float price;
    private String pic;
    private Date createtime;
    private String detail;
}
```
IItemsDao.java
```java
package cn.icanci.dao;

import cn.icanci.domain.Items;

/**
 * @Author: icanci
 * @ProjectName: maven_test
 * @PackageName: cn.icanci.dao
 * @Date: Created in 2020/1/16 10:18
 * @ClassAction: 操所数据库Items表的顶级接口
 */
public interface IItemsDao {
    /**
     * 根据 id 查询
     * @param id
     * @return
     */
    Items findByItemsId(Integer id);
}
```
在resources对应包下对应文件名的 IItemsDao.xml
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="cn.icanci.dao.IItemsDao">
    <select id="findByItemsId" resultType="cn.icanci.domain.Items" parameterType="int">
        select * from items where id = #{id}
    </select>
</mapper>
```
![对应配置文件](https://upload-images.jianshu.io/upload_images/19921157-f3ada325befc5e33.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

applicationContext.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context
                           http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/aop
                           http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/tx
                           http://www.springframework.org/schema/tx/spring-tx.xsd
                           http://www.springframework.org/schema/mvc
                           http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!--dao层配置 开始-->
    <!--配置连接池 使用阿里巴巴的 druid 连接池-->
    <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"></property>
        <property name="url" value="jdbc:mysql:///maven"></property>
        <property name="username" value="root"></property>
        <property name="password" value="ok"></property>
    </bean>
    <!--配置生产SqlSession工厂-->
    <bean id="sessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"></property>
        <!--扫描POJO包,给所有的POJO对象起别名-->
        <property name="typeAliasesPackage" value="cn.icanci.domain"></property>
    </bean>
    <!--扫描接口包路径,生成包下所有代理对象的接口,放在spring容器中-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="basePackage" value="cn.icanci.dao"></property>
    </bean>
    <!--dao层配置 结束-->
</beans>
```
log4j.properties
```properties
# Set root category priority to INFO and its only appender to CONSOLE.
#log4j.rootCategory=INFO, CONSOLE            debug   info   warn error fatal
log4j.rootCategory=debug, CONSOLE, LOGFILE

# Set the enterprise logger category to FATAL and its only appender to CONSOLE.
log4j.logger.org.apache.axis.enterprise=FATAL, CONSOLE

# CONSOLE is set to be a ConsoleAppender using a PatternLayout.
log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender
log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout
log4j.appender.CONSOLE.layout.ConversionPattern=%d{ISO8601} %-6r [%15.15t] %-5p %30.30c %x - %m\n

# LOGFILE is set to be a File appender using a PatternLayout.
log4j.appender.LOGFILE=org.apache.log4j.FileAppender
log4j.appender.LOGFILE.File=E:/AllLogs/idea2019/maven/ssm/ssm.log
log4j.appender.LOGFILE.Append=true
log4j.appender.LOGFILE.layout=org.apache.log4j.PatternLayout
log4j.appender.LOGFILE.layout.ConversionPattern=%d{ISO8601} %-6r [%15.15t] %-5p %30.30c %x - %m\n
```
测试
```Java
package cn.icanci.test;

import cn.icanci.dao.IItemsDao;
import cn.icanci.domain.Items;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author: icanci
 * @ProjectName: maven_test
 * @PackageName: cn.icanci.test
 * @Date: Created in 2020/1/16 10:39
 * @ClassAction: 测试 Mybatis
 */
public class TestMybatis {

    @Test
    public void findbyId(){
        //获取Spring容器
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        //获取Dao的对象
        IItemsDao itemsDao = ac.getBean(IItemsDao.class);
        System.out.println(itemsDao);
        Items item = itemsDao.findByItemsId(2);
        System.out.println(item);
    }
}
```
测试结果
![测试通过](https://upload-images.jianshu.io/upload_images/19921157-2fea5b59490f978c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

###### Service层的编写

IItemsService.java

```java
package cn.icanci.service;

import cn.icanci.domain.Items;

/**
 * @Author: icanci
 * @ProjectName: maven_test
 * @PackageName: cn.icanci.service
 * @Date: Created in 2020/1/16 11:16
 * @ClassAction:  Items 的 service 层
 */
public interface IItemsService {
    /**
     * 根据 id 查询
     * @param id
     * @return
     */
    Items findByItemsId(Integer id);
}
```
ItemsServiceImpl.java
```java
package cn.icanci.service.impl;

import cn.icanci.dao.IItemsDao;
import cn.icanci.domain.Items;
import cn.icanci.service.IItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: icanci
 * @ProjectName: maven_test
 * @PackageName: cn.icanci.service.impl
 * @Date: Created in 2020/1/16 11:18
 * @ClassAction:
 */
@Service
public class ItemsServiceImpl implements IItemsService {

    @Autowired
    private IItemsDao iItemsDao;
    @Override
    public Items findByItemsId(Integer id) {
        return iItemsDao.findByItemsId(id);
    }
}
```
applicationcontext.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context
                           http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/aop
                           http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/tx
                           http://www.springframework.org/schema/tx/spring-tx.xsd
                           http://www.springframework.org/schema/mvc
                           http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!--dao层配置 开始-->
    <!--配置连接池 使用阿里巴巴的 druid 连接池-->
    <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"></property>
        <property name="url" value="jdbc:mysql:///maven"></property>
        <property name="username" value="root"></property>
        <property name="password" value="ok"></property>
    </bean>
    <!--配置生产SqlSession工厂-->
    <bean id="sessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"></property>
        <!--扫描POJO包,给所有的POJO对象起别名-->
        <property name="typeAliasesPackage" value="cn.icanci.domain"></property>
    </bean>
    <!--扫描接口包路径,生成包下所有代理对象的接口,放在spring容器中-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="basePackage" value="cn.icanci.dao"></property>
    </bean>
    <!--dao层配置 结束-->

    <!--service配置文件 开始-->
    <!--组件扫描配置-->
    <context:component-scan base-package="cn.icanci.service"></context:component-scan>
    <!--aop面向切面编程 ,切面就是切入点和通知的组合-->
    <!--配置事务管理器-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"></property>
    </bean>
    <!--配置事务的通知-->
    <tx:advice id="txAdvice">
        <tx:attributes>
            <tx:method name="save*" propagation="REQUIRED"/>
            <tx:method name="update*" propagation="REQUIRED" />
            <tx:method name="delete*" propagation="REQUIRED" />
            <tx:method name="find*" propagation="SUPPORTS" read-only="true"/>
            <tx:method name="*" propagation="REQUIRED"/>
        </tx:attributes>
    </tx:advice>
    <!--配置切面-->
    <aop:config>
        <aop:pointcut id="pointcut" expression="execution(* cn.icanci.service.impl.*.*(..))"/>
        <aop:advisor advice-ref="txAdvice" pointcut-ref="pointcut"></aop:advisor>
    </aop:config>
    <!--service配置文件 结束-->
</beans>
```
**注意点 配置事务管理器的id必须为 transactionManager 否则测试会报错 如下**

```txt
org.springframework.beans.factory.BeanCreationException: 
Error creating bean with name 'itemsServiceImpl' defined in file 
[E:\IdeaHome\maven\maven_test\target\classes\cn\icanci\service\impl\ItemsServiceImpl.class]: 
Initialization of bean failed; nested exception is 
org.springframework.beans.factory.BeanCreationException: 
Error creating bean with name 'txAdvice': 
Cannot resolve reference to bean 'transactionManager' while setting bean property 'transactionManager';
 nested exception is 
org.springframework.beans.factory.NoSuchBeanDefinitionException:
 No bean named 'transactionManager' available
```
![报错截图](https://upload-images.jianshu.io/upload_images/19921157-06d7f0167f566492.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

测试类
```java
package cn.icanci.test;

import cn.icanci.dao.IItemsDao;
import cn.icanci.domain.Items;
import cn.icanci.service.IItemsService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author: icanci
 * @ProjectName: maven_test
 * @PackageName: cn.icanci.test
 * @Date: Created in 2020/1/16 10:39
 * @ClassAction: 测试 Mybatis
 */
public class TestMybatis {

    @Test
    public void findbyId(){
        //获取Spring容器
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        //Daoc测试
        //获取Dao的对象
        IItemsDao itemsDao = ac.getBean(IItemsDao.class);
        System.out.println(itemsDao);
        Items item = itemsDao.findByItemsId(2);
        System.out.println(item);

        //Service 测试
        //获取 Service对象
        IItemsService itemsService = ac.getBean(IItemsService.class);
        System.out.println(itemsService.findByItemsId(2));
    }
}
```
![测试通过](https://upload-images.jianshu.io/upload_images/19921157-fc2d9a1e3b149321.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
###### Web层的编写
ItemsController.java

```java
package cn.icanci.controller;
import	java.awt.Desktop.Action;

import cn.icanci.domain.Items;
import cn.icanci.service.IItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: icanci
 * @ProjectName: maven_test
 * @PackageName: cn.icanci.controller
 * @Date: Created in 2020/1/16 11:39
 * @ClassAction: Items 的 web层
 */
@Controller
@RequestMapping("/items")
public class ItemsController {
    @Autowired
    private IItemsService itemsService;

    @RequestMapping("/findDetail")
    public String findDetail(Model model){
        Items item = itemsService.findByItemsId(1);
        model.addAttribute("item",item);
        return "itemDetail";
    }
}
```
web.xml
```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app xmlns="http://java.sun.com/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
                             http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         version="3.0">

    <display-name>Archetype Created Web Application</display-name>

    <!--配置编码过滤器 -->
    <filter>
        <filter-name>characterEncodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
        <init-param>
            <param-name>forceEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>characterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <!--配置spring核心监听器 -->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
    <!--重新指定Spring配置文件的路径 -->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:applicationContext.xml</param-value>
    </context-param>
    <!-- SpringMVC的核心配置Servlet -->
    <servlet>
        <servlet-name>dispatcherServlet</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:springmvc.xml</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>dispatcherServlet</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
</web-app>
```
springmvc.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context
                           http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/aop
                           http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/tx
                           http://www.springframework.org/schema/tx/spring-tx.xsd
                           http://www.springframework.org/schema/mvc
                           http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!--web层(controller)配置文件 开始-->
    <!-- 配置组件扫描 -->
    <context:component-scan base-package="cn.icanci.controller"></context:component-scan>
    <!-- 处理器映射器 ,处理器适配器-->
    <mvc:annotation-driven />
    <!-- 视图扫描器 -->
    <bean id="internalResourceViewResolver" class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/pages/"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>
    <!-- 释放静态资源 -->
    <mvc:default-servlet-handler />
    <!--web层(controller)配置文件 结束-->
</beans>
```
index.jsp
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <a href="items/findDetail">测试 </a>
</body>
</html>
```
itemDetail.jsp
```jsp
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>    
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body> 
	<form>
		<table width="100%" border=1>
			<tr>
				<td>商品名称</td>
				<td> ${item.name } </td>
			</tr>
			<tr>
				<td>商品价格</td>
				<td> ${item.price } </td>
			</tr>
			<tr>
				<td>生成日期</td>
				<td> <fmt:formatDate value="${item.createtime}" pattern="yyyy-MM-dd HH:mm:ss"/> </td>
			</tr>
			<tr>
				<td>商品简介</td>
				<td>${item.detail} </td>
			</tr>
		</table>
	</form>
</body>
</html>
```
**注意点:此时 我写了 isELIgnored="false"**
如果是web.xml xsd是3.0 里面就要写 isELIgnored="false" 低于此版本不用写 否则识别不出来 JSTL 表达式 也就是如下原因
其实从后台取值并传值到前台来根本就没有错，而前台JSP页面EL表达式无效，解析不到EL表达式，引起的原因是web.xml中：

```xml
<web-app xmlns="http://java.sun.com/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
          http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         version="3.0">
```
注意里面的web-app_3_0.xsd，就是这个引起的，在web-app_2_4.xsd中就不会出现这种问题(这个版本的isELIgnored默认设置为false)。

在不改变web.xml3.0版本的情况下解决办法是：在jsp页面头加:<%@page isELIgnored="false"%> 问题得以解决。
或加在：

```jsp
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
```
报错信息如下
![报错](https://upload-images.jianshu.io/upload_images/19921157-ab7445aa1f75a58d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
修改之后 正常
![修改之后](https://upload-images.jianshu.io/upload_images/19921157-3616d2ab4dd7713c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

##### 分模块构建工程 [应用] 
基于上边的三个工程分析
继承：创建一个 parent 工程将所需的依赖都配置在 pom 中
聚合：聚合多个模块运行

###### 需求描述
将 SSM 工程拆分为多个模块开发：
ssm_dao
ssm_service
ssm_web
![分解](https://upload-images.jianshu.io/upload_images/19921157-b6035e602df6ed6a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

###### 理解 继承和聚合
通常继承和聚合同时使用。

**何为继承？**
继承是为了消除重复，如果将 dao、service、web 分开创建独立的工程则每个工程的 pom.xml
文件中的内容存在重复，比如：设置编译版本、锁定 spring的版本的等，可以将这些重复的
配置提取出来在父工程的 pom.xml 中定义。
 何为聚合？
项目开发通常是分组分模块开发，每个模块开发完成要运行整个工程需要将每个模块聚合在
一起运行，比如：dao、service、web 三个工程最终会打一个独立的war 运行。
**现在新建一个项目**
![新建项目](https://upload-images.jianshu.io/upload_images/19921157-798a286254837b51.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

这里不选择骨架  

![不选择骨架](https://upload-images.jianshu.io/upload_images/199*21157-631f34fe1e81426c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后填写 信息
![填写信息](https://upload-images.jianshu.io/upload_images/19921157-73f10e1be4277c64.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
下一步
![下一步](https://upload-images.jianshu.io/upload_images/19921157-84807b756d845505.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
此处我选择 新的窗口
![新的窗口](https://upload-images.jianshu.io/upload_images/19921157-4ae8fda35c1f9497.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
这个时候 删除不必要的文件 下方红色框框里面的都可以删除掉 当然 不删除也是可以的
![我选择删除不必要的文件](https://upload-images.jianshu.io/upload_images/19921157-6675c4777e9b8266.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
此时继续创建子模块  选中 项目右键 新建 module
![新建](https://upload-images.jianshu.io/upload_images/19921157-eb8fd3a0e7707ab1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
先创建Dao层 不需要界面交互,所以我不需要骨架创建
![Dao层](https://upload-images.jianshu.io/upload_images/19921157-6ea32b2a87245964.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后填写 id 信息 此时我这个是 dao 层
![dao层](https://upload-images.jianshu.io/upload_images/19921157-b197f1948927d44d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![下一步](https://upload-images.jianshu.io/upload_images/19921157-a959adfb45ba2f4c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后建立 service 层 步骤和上述的一样
![建立service层](https://upload-images.jianshu.io/upload_images/19921157-3163554a505632e1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后建立web层 此时选中 maven 的 webapp 骨架
![建立web层](https://upload-images.jianshu.io/upload_images/19921157-3fe56aa548c2827d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![填写信息](https://upload-images.jianshu.io/upload_images/19921157-534ed8648fa6bb4d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
增加键值对 为了快速创建 webapp 

```txt
archetypeCatalog
internal
```
![增加键值对](https://upload-images.jianshu.io/upload_images/19921157-34962d1551cda581.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后创建 web 层
![创建web层](https://upload-images.jianshu.io/upload_images/19921157-d43d69363106b693.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后web层的pom.xml可以删除到此 依据项目开发而定
![结束](https://upload-images.jianshu.io/upload_images/19921157-28ffe9f441a8b914.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
结束
![创建完成](https://upload-images.jianshu.io/upload_images/19921157-09cf40a2b6b1afde.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

###### 工程和模块的区别
​    工程不等于完整的项目,模块也不等同于完整的项目
​    一个完整的项目看的是代码,代码完整,就可以说这就是一个完整的项目
​    和此项目是工程还是模块没有关系

```txt
工程天生只能使用自己的内部资源,工程是独立的 后天可以和其他模块和工程建立关联关系
模块天生不是独立的,模块天生是属于父工程的,所有父工程的资源都可以使用

父子工程直接,子模块天生继承父工程,可以使用父工程所有资源
子模块之间天生独立 是没有任何关系的  但是可以通过坐标导入

父子工程直接不需要建立关系,继承关系是天生的,不需要手动建立

平级之间的引用叫做依赖,依赖不是天生的,依赖是需要后天建
```

###### 管理建立好的项目
首先在 父工程里面的 pom.xml 里面导入坐标 也就是刚刚之前的那个项目的坐标

```xml
<!-- 统一管理jar包版本 -->
<properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <spring.version>5.0.2.RELEASE</spring.version>
    <slf4j.version>1.6.6</slf4j.version>
    <log4j.version>1.2.12</log4j.version>
    <shiro.version>1.2.3</shiro.version>
    <mysql.version>5.1.6</mysql.version>
    <mybatis.version>3.4.5</mybatis.version>
    <spring.security.version>5.0.1.RELEASE</spring.security.version>
</properties>
<!-- 锁定jar包版本 -->
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>${mybatis.version}</version>
        </dependency>
    </dependencies>
</dependencyManagement>
<!-- 项目依赖jar包 -->
<dependencies>
    <!-- spring -->
    <dependency>
        <groupId>org.aspectj</groupId>
        <artifactId>aspectjweaver</artifactId>
        <version>1.6.8</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aop</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context-support</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-web</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-orm</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-beans</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-core</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-tx</artifactId>
        <version>${spring.version}</version>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>${mysql.version}</version>
    </dependency>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>javax.servlet-api</artifactId>
        <version>3.1.0</version>
        <scope>provided</scope>
    </dependency>
    <dependency>
        <groupId>javax.servlet.jsp</groupId>
        <artifactId>jsp-api</artifactId>
        <version>2.0</version>
        <scope>provided</scope>
    </dependency>
    <dependency>
        <groupId>jstl</groupId>
        <artifactId>jstl</artifactId>
        <version>1.2</version>
    </dependency>
    <!-- log start -->
    <dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>${log4j.version}</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${slf4j.version}</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-log4j12</artifactId>
        <version>${slf4j.version}</version>
    </dependency>
    <!-- log end -->
    <dependency>
        <groupId>org.mybatis</groupId>
        <artifactId>mybatis</artifactId>
        <version>${mybatis.version}</version>
    </dependency>
    <dependency>
        <groupId>org.mybatis</groupId>
        <artifactId>mybatis-spring</artifactId>
        <version>1.3.0</version>
    </dependency>
    <dependency>
        <groupId>c3p0</groupId>
        <artifactId>c3p0</artifactId>
        <version>0.9.1.2</version>
        <type>jar</type>
        <scope>compile</scope>
    </dependency>
    <dependency>
        <groupId>com.github.pagehelper</groupId>
        <artifactId>pagehelper</artifactId>
        <version>5.1.2</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-web</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-config</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-core</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-taglibs</artifactId>
        <version>${spring.security.version}</version>
    </dependency>
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid</artifactId>
        <version>1.0.9</version>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
        <scope>test</scope>
    </dependency>
    <!-- 其他非SSM必须的Jar依赖-->
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>1.18.8</version>
        <scope>provided</scope>
    </dependency>
</dependencies>
```
然后再在 service 模块引入 dao 模块的坐标
```xml
<dependencies>
    <dependency>
        <groupId>cn.icanci</groupId>
        <artifactId>maven_dao</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```
然后再在 web 模块引入 service 模块的坐标
```xml
<dependencies>
    <dependency>
        <groupId>cn.icanci</groupId>
        <artifactId>maven_service</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```
![了解](https://upload-images.jianshu.io/upload_images/19921157-6547e7f150cc8c3a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

那么现在整个之前的项目 首先看一下整合之后的目录结构
![目录结构](https://upload-images.jianshu.io/upload_images/19921157-9c21cbaf80e32a51.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![目录结构](https://upload-images.jianshu.io/upload_images/19921157-917c075eb14290aa.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
###### 拆分思路 属于那一层的内容就放在哪一层去
首先是dao层 把dao层的文件都放进去  如图所示
![把dao层的放到dao层](https://upload-images.jianshu.io/upload_images/19921157-b7c604606cb1a772.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
同时修改 applicationContext-dao.xml 文件 因为是dao层,所以只留下dao层的内容

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context
                           http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/aop
                           http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/tx
                           http://www.springframework.org/schema/tx/spring-tx.xsd
                           http://www.springframework.org/schema/mvc
                           http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!--dao层配置 开始-->
    <!--配置连接池 使用阿里巴巴的 druid 连接池-->
    <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"></property>
        <property name="url" value="jdbc:mysql:///maven"></property>
        <property name="username" value="root"></property>
        <property name="password" value="ok"></property>
    </bean>
    <!--配置生产SqlSession工厂-->
    <bean id="sessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"></property>
        <!--扫描POJO包,给所有的POJO对象起别名-->
        <property name="typeAliasesPackage" value="cn.icanci.domain"></property>
    </bean>
    <!--扫描接口包路径,生成包下所有代理对象的接口,放在spring容器中-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="basePackage" value="cn.icanci.dao"></property>
    </bean>
    <!--dao层配置 结束-->
</beans>
```
然后是 service 层  把service层的文件都放进去  如图所示
![service层](https://upload-images.jianshu.io/upload_images/19921157-909271a9d6df4867.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
修改 applicationContext-service.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context
                           http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/aop
                           http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/tx
                           http://www.springframework.org/schema/tx/spring-tx.xsd
                           http://www.springframework.org/schema/mvc
                           http://www.springframework.org/schema/mvc/spring-mvc.xsd">


    <!--    <import resource="applicationContext-dao.xml"></import>-->
    <!--service配置文件 开始-->
    <!--组件扫描配置-->
    <context:component-scan base-package="cn.icanci.service"></context:component-scan>
    <!--aop面向切面编程 ,切面就是切入点和通知的组合-->
    <!--配置事务管理器-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"></property>
    </bean>
    <!--配置事务的通知-->
    <tx:advice id="txAdvice">
        <tx:attributes>
            <tx:method name="save*" propagation="REQUIRED"/>
            <tx:method name="update*" propagation="REQUIRED" />
            <tx:method name="delete*" propagation="REQUIRED" />
            <tx:method name="find*" propagation="SUPPORTS" read-only="true"/>
            <tx:method name="*" propagation="REQUIRED"/>
        </tx:attributes>
    </tx:advice>
    <!--配置切面-->
    <aop:config>
        <aop:pointcut id="pointcut" expression="execution(* cn.icanci.service.impl.*.*(..))"/>
        <aop:advisor advice-ref="txAdvice" pointcut-ref="pointcut"></aop:advisor>
    </aop:config>
    <!--service配置文件 结束-->
</beans>
```
有的童鞋可能发现现在 dataSource 报错 ,这里是没关系的,可以正常跑起来的 
如有和我一样有强迫症的, 可以把 有 dataSource的xml文件导入进来 

```xml
<import resource="applicationContext-dao.xml"></import>
```
此时就不错了
![此时不报错](https://upload-images.jianshu.io/upload_images/19921157-331d2aac962f4d1c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后web层也是 如图 然后  applicationContext.xml只需要引入另外两个文件就可以
![ok](https://upload-images.jianshu.io/upload_images/19921157-19c28ca7ae587dcd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
此时部署运行本地的tomcat 
成功显示结果
![成功显示结果](https://upload-images.jianshu.io/upload_images/19921157-4851d61cde2efcf8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
另外两种方式是运行 安装的Tomcat插件
第一种:运行 root 下的Tomcat 插件 我,没有安装
![运行 root 下的Tomcat 插件](https://upload-images.jianshu.io/upload_images/19921157-fe28f9a0acca9e71.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
第二种:运行  web 模块下的 Tomcat 插件 但是运行之前必须 有这一步
![安装](https://upload-images.jianshu.io/upload_images/19921157-dab55f629ae09c31.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后再这里面的插件运行Tomcat
![然后再这里面的插件运行Tomcat](https://upload-images.jianshu.io/upload_images/19921157-f5ca27fc1d4ee77e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
这是3种服务器运行方式
##### 私服
Nexus 是 Maven 仓库管理器，通过 nexus 可以搭建 maven 仓库，同时 nexus 还提供强
大的仓库管理功能，构件搜索功能等。
下载 Nexus， 下载地址 [https://blog.sonatype.com/](https://blog.sonatype.com/)
把私服软件解压缩到一个没有中文 没有空格的目录下
![解压缩](https://upload-images.jianshu.io/upload_images/19921157-d2b2f247128d684a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
安装 需要以管理员身份运行 windows powershell 或者 以管理员的身份运行  win 的命令行 
此处我选择使用 以管理员身份运行 windows powershell 
![ 以管理员身份运行 windows powershell](https://upload-images.jianshu.io/upload_images/19921157-353a7b358ced21b2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后可以直接复制路径   以下是我安装的路径 

```shell
D:\nexus-2.12.0-01-bundle\nexus-2.12.0-01\bin
```
![复制安装的路径](https://upload-images.jianshu.io/upload_images/19921157-265589629bcbf20b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后在power shell 中  cd 此路径
```powershell
cd D:\nexus-2.12.0-01-bundle\nexus-2.12.0-01\bin
```
然后可以使用 ls 命令查看有哪些文件夹
```powershell
ls 
```
![ls](https://upload-images.jianshu.io/upload_images/19921157-501262429c0efc92.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后安装  两个文件都可以进行安装
![安装](https://upload-images.jianshu.io/upload_images/19921157-7b63332c5fb5a85e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
或者卸载
![卸载](https://upload-images.jianshu.io/upload_images/19921157-8971aa08f8577864.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
但是 如果不使用管理员身份运行就会失败 拒绝访问 如下
![拒绝访问](https://upload-images.jianshu.io/upload_images/19921157-4cff5a751c87a238.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
运行  nexus.bat 安装
![安装](https://upload-images.jianshu.io/upload_images/19921157-b53a880878cdec82.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后启动
![启动](https://upload-images.jianshu.io/upload_images/19921157-ff54b28f64d57b6e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后进入  D:\nexus-2.12.0-01-bundle\nexus-2.12.0-01\conf 查看里面的  nexus.properties 文件
```properties
D:\nexus-2.12.0-01-bundle\nexus-2.12.0-01\conf
# nexus.properties
```
![image.png](https://upload-images.jianshu.io/upload_images/19921157-8625107f9e9195bd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
默认端口是 8081 然后再浏览器输入
***
http://localhost:8081/nexus/
***
![image.png](https://upload-images.jianshu.io/upload_images/19921157-17e46c52b55e4327.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后右上角可以登陆 
账户和密码 
```txt
账户:  admin
密码:  admin123
```
![登陆](https://upload-images.jianshu.io/upload_images/19921157-f1211815916dee1b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
仓库类型 
1. hosted，宿主仓库，部署自己的 jar 到这个类型的仓库，包括 releases 和 snapshot 两部
分，Releases公司内部发布版本仓库、 Snapshots 公司内部测试版本仓库
2. proxy，代理仓库，用于代理远程的公共仓库，如 maven 中央仓库，用户连接私服，私
服自动去中央仓库下载 jar 包或者插件。
3. group，仓库组，用来合并多个 hosted/proxy 仓库，通常我们配置自己的 maven 连接仓
库组。
4. virtual(虚拟)：兼容 Maven1 版本的 jar 或者插件
![仓库类型](https://upload-images.jianshu.io/upload_images/19921157-c667f15a84cf2624.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
nexus 仓库默认在 sonatype-work 目录中
![默认仓库地址](https://upload-images.jianshu.io/upload_images/19921157-d176d1167a7a7c94.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- apache-snapshots ：代理仓库
- 存储 snapshots 构件，代理地址 https://repository.apache.org/snapshots/
- central-m1 ：virtual 类型仓库，兼容 Maven1 版本的 jar 或者插件
- releases ： 本地仓库，存储 releases 构件。
- snapshots ： 本地仓库，存储 snapshots 构件。
- thirdparty ：第三方仓库
- public  ：仓库组

###### 将项目发布到私服
企业中多个团队协作开发通常会将一些公用的组件、开发模块等发布到私服供其它团队
或模块开发人员使用。
本例子假设多团队分别开发 ssm_dao、ssm_service、ssm_web，某个团队开发完在
ssm_dao会将 ssm_dao 发布到私服供 ssm_service团队使用，本例子会将 ssm_dao 工程打成
jar 包发布到私服。
![发布](https://upload-images.jianshu.io/upload_images/19921157-9541041bc6658e9e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
配置
第一步： 需要在客户端即部署 ssm_dao工程的电脑上配置 maven环境，并 修改 settings.xml
文件，配置连接私服的用户和密码 。
此用户名和密码用于私服校验，因为私服需要知道上传的账号和密码是否和私服中的账号和密码一致。

```xml
<servers>
    <server>
        <id>releases</id>
        <username>admin</username>
        <password>admin123</password>
    </server>
    <server>
        <id>snapshots</id>
        <username>admin</username>
        <password>admin123</password>
    </server>
</servers>
```
![配置server](https://upload-images.jianshu.io/upload_images/19921157-264eee09ad5af347.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
第二步： 配置项目 pom.xml
配置私服仓库的地址，本公司的自己的 jar 包会上传到私服的宿主仓库，根据工程的版本号
决定上传到哪个宿主仓库，如果版本为 release 则上传到私服的 release 仓库，如果版本为
snapshot 则上传到私服的 snapshot仓库
```xml
<distributionManagement>
    <repository>
        <id>releases</id>
        <url>http://localhost:8081/nexus/content/repositories/releases/</url>
    </repository>
    <snapshotRepository>
        <id>snapshots</id>
        <url>http://localhost:8081/nexus/content/repositories/snapshots/</url>
    </snapshotRepository>
</distributionManagement>
```
此处我放在 dao 里面的 pom.xml
注意：pom.xml 这里<id> 和 settings.xml 配置 <id> 对应！
第三步:测试
将项目 dao 工程打成 jar 包发布到私服：
1、首先启动 nexus
2、对 dao工程执行 deploy 命令
![测试](https://upload-images.jianshu.io/upload_images/19921157-238724f910a927da.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
根据本项目pom.xml中version定义决定发布到哪个仓库，如果version定义为snapshot，
执行deploy后查看nexus的snapshot仓库，如果version定义为release则项目将发布到nexus
的 release仓库，本项目将发布到 snapshot仓库
查看结果 
![成功](https://upload-images.jianshu.io/upload_images/19921157-c61e8b988d977c36.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
此时本地仓库也是有的
![本地仓库也是有的](https://upload-images.jianshu.io/upload_images/19921157-e6f7f0947bf3d56a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后把私服和本地仓库关联 修改 maven 的 conf 的setting.xml文件 增加以下配置

```xml
<profiles>
    <!-- 下载jar包配置 -->
    <profile> 
        <!--profile的id -->
        <id>dev</id>
        <repositories>
            <repository> <!--仓库id，repositories可以配置多个仓库，保证id不重复 -->
                <id>nexus</id> <!--仓库地址，即nexus仓库组的地址 -->
                <url>http://localhost:8081/nexus/content/groups/public/</url> <!--是否下载releases构件 -->
                <releases>
                    <enabled>true</enabled>
                </releases> <!--是否下载snapshots构件 -->
                <snapshots>
                    <enabled>true</enabled>
                </snapshots>
            </repository>
        </repositories>
        <pluginRepositories> <!-- 插件仓库，maven的运行依赖插件，也需要从私服下载插件 -->
            <pluginRepository> <!-- 插件仓库的id不允许重复，如果重复后边配置会覆盖前边 -->
                <id>public</id>
                <name>Public Repositories</name>
                <url>http://localhost:8081/nexus/content/groups/public/</url>
            </pluginRepository>
        </pluginRepositories>
    </profile>
</profiles>

<activeProfiles>
    <activeProfile>dev</activeProfile>
</activeProfiles>
```
此时配置就完成了
我的整体的 setting.xml文件 我是做了修改的之前  
```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

    <localRepository>E:\MavenHome\repository</localRepository>
    <mirrors>
        <mirror>
            <id>alimaven</id>
            <name>aliyun maven</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <mirrorOf>central</mirrorOf>
        </mirror>
        <mirror>
            <id>central</id>
            <name>Maven Repository Switchboard</name>
            <url>http://repo1.maven.org/maven2/</url>
            <mirrorOf>central</mirrorOf>
        </mirror>
        <mirror>
            <id>repo2</id>
            <mirrorOf>central</mirrorOf>
            <name>Human Readable Name for this Mirror.</name>
            <url>http://repo2.maven.org/maven2/</url>
        </mirror>
        <mirror>
            <id>ibiblio</id>
            <mirrorOf>central</mirrorOf>
            <name>Human Readable Name for this Mirror.</name>
            <url>http://mirrors.ibiblio.org/pub/mirrors/maven2/</url>
        </mirror>
        <mirror>
            <id>jboss-public-repository-group</id>
            <mirrorOf>central</mirrorOf>
            <name>JBoss Public Repository Group</name>
            <url>http://repository.jboss.org/nexus/content/groups/public</url>
        </mirror>
        <!-- 中央仓库在中国的镜像 -->
        <mirror>
            <id>maven.net.cn</id>
            <name>oneof the central mirrors in china</name>
            <url>http://maven.net.cn/content/groups/public/</url>
            <mirrorOf>central</mirrorOf>
        </mirror>
    </mirrors>
    <servers>
        <server>
            <id>releases</id>
            <username>admin</username>
            <password>admin123</password>
        </server>
        <server>
            <id>snapshots</id>
            <username>admin</username>
            <password>admin123</password>
        </server>
    </servers>
    <profiles>
        <profile>
            <id>jdk-1.8</id>
            <activation>
                <activeByDefault>true</activeByDefault>
                <jdk>1.8</jdk>
            </activation>
            <properties>
                <maven.compiler.source>1.8</maven.compiler.source>
                <maven.compiler.target>1.8</maven.compiler.target>
                <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
            </properties>
        </profile>
        <!-- 下载jar包配置 -->
        <profile> 
            <!--profile的id -->
            <id>dev</id>
            <repositories>
                <repository> <!--仓库id，repositories可以配置多个仓库，保证id不重复 -->
                    <id>nexus</id> <!--仓库地址，即nexus仓库组的地址 -->
                    <url>http://localhost:8081/nexus/content/groups/public/</url> <!--是否下载releases构件 -->
                    <releases>
                        <enabled>true</enabled>
                    </releases> <!--是否下载snapshots构件 -->
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                </repository>
            </repositories>
            <pluginRepositories> <!-- 插件仓库，maven的运行依赖插件，也需要从私服下载插件 -->
                <pluginRepository> <!-- 插件仓库的id不允许重复，如果重复后边配置会覆盖前边 -->
                    <id>public</id>
                    <name>Public Repositories</name>
                    <url>http://localhost:8081/nexus/content/groups/public/</url>
                </pluginRepository>
            </pluginRepositories>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>dev</activeProfile>
    </activeProfiles>
</settings>
```
###### 安装第三方Jar到本地仓库
--安装第三方jar包到本地仓库

----进入jar包所在目录运行
```powershell
mvn install:install-file -DgroupId=com.alibaba -DartifactId=fastjson -Dversion=1.1.37 -Dfile=fastjson-1.1.37.jar -Dpackaging=jar
````
----打开cmd直接运行
```powershell
mvn install:install-file -DgroupId=com.alibaba -DartifactId=fastjson -Dversion=1.1.37 -Dpackaging=jar -Dfile=H:\java主流框架\主流框架\1-6资料\05.Maven高级\maven高级\资料\安装第三方jar包\fastjson-1.1.37.jar
```
安装成功的界面 
![安装成功](https://upload-images.jianshu.io/upload_images/19921157-14e331a7342586c0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
本地仓库也有了
![本地仓库也有了](https://upload-images.jianshu.io/upload_images/19921157-68e7f7451cb43958.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
###### 安装第三方jar包到私服
--安装第三方jar包到私服
--在settings配置文件中添加登录私服第三方登录信息

```xml
<server>
    <id>thirdparty</id>
    <username>admin</username>
    <password>admin123</password>
</server>
```
![setting.xml配置文件](https://upload-images.jianshu.io/upload_images/19921157-e6b225744faaded2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

----进入jar包所在目录运行
```powershell
mvn deploy:deploy-file -DgroupId=com.alibaba -DartifactId=fastjson -Dversion=1.1.37 -Dpackaging=jar -Dfile=fastjson-1.1.37.jar -Durl=http://localhost:8081/nexus/content/repositories/thirdparty/ -DrepositoryId=thirdparty
```
----打开cmd直接运行
```powershell
mvn deploy:deploy-file -DgroupId=com.alibaba -DartifactId=fastjson -Dversion=1.1.37 -Dpackaging=jar -Dfile=H:\java主流框架\主流框架\1-6资料\05.Maven高级\maven高级\资料\安装第三方jar包\fastjson-1.1.37.jar -Durl=http://localhost:8081/nexus/content/repositories/thirdparty/ -DrepositoryId=thirdparty
```
运行之后 安装成功
![安装成功](https://upload-images.jianshu.io/upload_images/19921157-fc7b8910b22c97ff.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
私服上也有了
![私服上也有了](https://upload-images.jianshu.io/upload_images/19921157-1ed2935c3842d91a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

###### 信息说明
DgroupId 和 DartifactId 构成了该 jar 包在 pom.xml 的坐标，项目就是依靠这两个属性定位。
自己起名字也行。
Dfile 表示需要上传的 jar 包的绝对路径。
Durl 私服上仓库的位置，打开 nexus——>repositories菜单，可以看到该路径。
DrepositoryId 服务器的表示 id，在 nexus 的configuration 可以看到。
Dversion 表示版本信息，
关于 jar 包准确的版本：
包的名字上一般会带版本号，如果没有那可以解压该包，会发现一个叫 MANIFEST.MF 的文件，这个文件就有描述该包的版本信息。
比如 Specification-Version: 2.2 可以知道该包的版本了。
上传成功后，在 nexus 界面点击3rd party 仓库可以看到这包。

###### 至此 Maven 高级的笔记到底告一段落 之后有需要补充的会继续补充

###### 此处 [@我自己](http://wpa.qq.com/msgrd?v=3&uin=1845666903&site=qq&menu=yes)  这是QQ聊天的链接 欢迎一起吹🐮学习鸭