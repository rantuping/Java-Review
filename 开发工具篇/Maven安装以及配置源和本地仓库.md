##### Maven 安装以及配置源和本地仓库

本文介绍安装Maven Maven是一个项目管理工具,也可以说是规范.此处不详细说.
假如你已经对安装Maven有了了解或者很熟悉 那么可以看下一篇 [@Maven 高级](https://www.jianshu.com/p/b1154ed4d608)

###### 环境和版本

###### 首先必须安装JDK 没有安装的或者不会安装的小伙伴戳 [@安装JDK](https://www.jianshu.com/p/a56cf16b0c2c)
我安装的版本是JDK8,具体版本如下: 
![JDK版本](https://upload-images.jianshu.io/upload_images/19921157-0efccddcc3b49214.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后下载Maven的安装包 此处 [@Maven安装包下载地址](https://maven.apache.org/download.cgi)
如下:
![Maven安装包下载地址](https://upload-images.jianshu.io/upload_images/19921157-3910c1b68966bccc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后把文件解压到:D:\apache-maven-3.0.5
![D:\apache-maven-3.0.5](https://upload-images.jianshu.io/upload_images/19921157-1bee50254ea8a1f2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后新建环境变量,步骤如下:
第一步:右键此电脑点击属性
![第一步](https://upload-images.jianshu.io/upload_images/19921157-c40c1e44bb389df9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
第二步选择:高级系统设置
![第二步](https://upload-images.jianshu.io/upload_images/19921157-bd4dd11e5acbc4e6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
第三步:键入环境变量
![第三步](https://upload-images.jianshu.io/upload_images/19921157-82de1a26dccceb47.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
第四步:设置Maven的环境变量
![第四步](https://upload-images.jianshu.io/upload_images/19921157-4fba0e4f16a095b1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

```
MAVEN_HOME
D:\apache-maven-3.0.5
```
第五步:编辑环境变量Path，追加%MAVEN_HOME%\bin\
![第五步](https://upload-images.jianshu.io/upload_images/19921157-385cf28773e88018.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![第五步](https://upload-images.jianshu.io/upload_images/19921157-401afde7b56872c5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
此时已经完成了Maven的安装,现在来测试一下:
同时按下: Win+R,进入命令行 输入cmd 然后回车
![同时按下: Win+R,进入命令行 输入cmd 然后回车](https://upload-images.jianshu.io/upload_images/19921157-621a0fdc3b348892.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后输入 mvn -v 之后回车
会出现如下界面就是安装成功了:包含安装的基本信息
![成功界面](https://upload-images.jianshu.io/upload_images/19921157-633fa35c18ed47dd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

###### 然后接下来就是一些环境的基本的配置了

1. 打开安装的目录 如下图
![安装目录](https://upload-images.jianshu.io/upload_images/19921157-728f8417a439f9ea.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
2. 进入conf文件夹 里面会有一个setting.xml文件
![conf文件夹](https://upload-images.jianshu.io/upload_images/19921157-4cbebf8c8c40f88f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
3. 打开这个文件夹 可以使用记事本打开 我这里使用EditPlus打开
4.第一个需要修改的地方 是配置本地 Maven 仓库  我这放在 F:\MavenHome\repository 目录
```
<localRepository>F:\MavenHome\repository</localRepository>
```
![第一个需要修改的地方](https://upload-images.jianshu.io/upload_images/19921157-e55feb53dab3f3ee.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
如下图:这样下载的jar版本就不会重复下载了
![maven本地仓库](https://upload-images.jianshu.io/upload_images/19921157-b1bdb0e9da764a00.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
5. 第二个需要修改的地方 如下图
![第二个需要修改的地方](https://upload-images.jianshu.io/upload_images/19921157-02bc08b591ad1271.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
更换源,外国的源太慢,我这里使用阿里的源
```xml
<mirrors>
    <mirror>
        <id>aliyun</id>
        <mirrorOf>*</mirrorOf>
        <name>aliyun Maven</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
    </mirror>
</mirrors>
```
换了之后在 idea 里面就可以很快了.
ok,over.

###### 如果小伙伴需要什么软件的安装,或需要软件,或者安装过程出现问题的,可以 [@我自己](http://wpa.qq.com/msgrd?v=3&uin=1845666903&site=qq&menu=yes) QQ联系我的.知无不解. 会陆续更新