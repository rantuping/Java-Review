### Gradle的学习和使用

#### 首先下载 Groovy 的安装包

下载地址： http://groovy.apache.org/download.html 

![1594130141484](images/1594130141484.png)



#### 下载之后解压到本地文件夹

**注意：必须先安装JDK才能使用**

![1594130204329](images/1594130204329.png)

#### 然后进入解压之后的 bin 目录 

D:\groovy-3.0.4\bin

然后命令行进入此文件夹，输入 `groovy -v` 命令 会出现以下内容

![1594130537023](images/1594130537023.png)

此时输入 `groovyConsole` 命令，会弹出Groovy的代码输入框如下

输入代码

```groovy
println("hello")
System.out.println("hello by java")
// 然后 Ctrl+R 执行代码
// Grovvy 是兼容Java语法的
```

![1594130720557](images/1594130720557.png)