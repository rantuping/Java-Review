### Java - 多线程详解

#### 线程简介

多任务，比如一边吃饭一边玩手机，看起来是很多个任务都在做，但是实际上我们的大脑在同一时间依旧只做了一件事情

**原本的调用方法：**

主线程 调用 run() 方法，然后主线程执行run()

其只有一条执行路径

**多线程调用方法：**

主线程调用 t.start() 方法启动线程，然后子线程执行run()方法

此时有多条执行路径交替进行

**程序、进程、线程**

在操作系统中使用的的程序就是进程，比如你的QQ、播放器、游戏、IDE等

一个进程可以有多个线程，如视频中同时听声音，看图像，看弹幕等待

**Process和Thread**

- 说起进程就要说一下**程序**，程序是指令和数据的有序集合，其本身没有任何运行的意义，是一个静态的概念
-  而**进程**则是执行程序的一次执行过程，它是一个动态的概念，是系统资源分配的单位、
- 通常在一个进程中可以包含若干个**线程**，当然一个进程中至少有一个线程，不然没有存在的意义。线程是CPU调度和执行的单位
- 注意：很多多线程是模拟出来的，真正的多线程是指有多个CPU。即多核，如服务器。如果是模拟出来的多线程，即在一个CPU的情况下，在通过一个时间点，CPU只能执行一个代码，因为切换的很快，所以就有同时执行的错觉。

**核心概念**

-  线程就是独立的执行路径
- 在程序运行的时候，即使没有自己创建线程，后台也会有多个线程，如主线程。gc线程
- main()称之为主线程。为系统的入口，用于执行整个程序
- 在一个进程中，如果开辟了多个线程，线程的运行是由调度器安排调度，调度器是与操作系统紧密相关的，先后顺序是不能人为的干预的
- 对同一份资源进行操作的时候，会存在资源抢夺的问题，需要加入并发控制
- 线程会带来额外的开销，如CPU调度，并发控制开销
- 每个线程在自己的工作内存交互，内存控制不当会造成数据不一致

#### 线程实现（重点）

线程的创建方法：继承Thread、实现Runnable接口、实现Callable接口、使用线程池创建

**继承Thread类**

- 自定义线程类继承 **Thread类**
- 重写**run()**方法，编写线程执行体
- 创建线程对象，调用 **start()**方法启动线程
- 注意：线程不一定执行，由CPU安排调度

```java
// 注意：线程开启不一定应立即执行，由CPU调度执行
public class TestThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(i + " Look Code");
        }
    }

    public static void main(String[] args) {

        // 创建线程对象
        TestThread testThread = new TestThread();
        testThread.start();

        // main 线程，主线程
        for (int i = 0; i < 10; i++) {
            System.out.println("main " + i);
        }
    }
}

```

**案例：下载**

```java
// 多线程下载
public class TestThread2 extends Thread {
    private String url;
    private String name;

    public TestThread2() {
    }

    public TestThread2(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            new WebDownloader().download(this.url, this.name);
            System.out.println("文件已经下载好了....");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String url = "https://img0.utuku.china.com/0x0/ent/20161011/7232c15c-7bff-45c8-9a62-db0983e56a07.jpg";
        TestThread2 testThread1 = new TestThread2(url, "x1.jpg");
        TestThread2 testThread2 = new TestThread2(url, "x2.jpg");
        TestThread2 testThread3 = new TestThread2(url, "x3.jpg");
        TestThread2 testThread4 = new TestThread2(url, "x4.jpg");
        testThread1.start();
        testThread2.start();
        testThread3.start();
        testThread4.start();
    }
}

class WebDownloader {
    // 下载方法
    public void download(String url, String name) throws IOException {
        FileUtils.copyURLToFile(new URL(url), new File(name));
    }
}
```

**实现 Runnable 接口**

- 定义 MyRunnable 类实现Runnable接口
- 实现run()方法，编写线程执行体
- 创建线程对象，调用start()方法启动线程

```java
public class TestThread3 implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("TestThread3.run " + i);
        }
    }

    public static void main(String[] args) {
        //创建 TestThread3 对象
        TestThread3 testThread3 = new TestThread3();
        // 创建线程对象，通过线程开启我们的线程，代理模式
        Thread thread = new Thread(testThread3);
        thread.start();
        System.out.println("????");
    }
}

```

**小结**

- 继承Thread类
  - 子类继承Thread类具有多线程的能力
  - 启动线程：子类对象.start()
  - 不建议使用：避免OOP单继承局限性
- 实现Runnable接口 
  - 实现Runnable接口具有多线程能力
  - 启动线程：船都目标对象+Thread对象.start()
  - 推荐使用：避免单继承局限性，灵活方便，方便再同一个对象被多个线程使用

**并发访问案例：买车票问题**

```java
public class TestThread4 implements Runnable {

    // 票数
    private int ticketNum = 10;


    @Override
    public void run() {
        while (true) {
            System.out.println(Thread.currentThread().getName() + " 拿到了票：" + ticketNum--);
            if (ticketNum <= 0) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        TestThread4 th = new TestThread4();
        new Thread(th).start();
        new Thread(th).start();
        new Thread(th).start();
    }
}

```

**案例：龟兔赛跑**

- 首先来个赛道距离，然后距离重点越来越近
- 判断比赛是否结束
- 打印处胜利者
- 故事中是乌龟赢得，兔子需要睡觉，所以模拟兔子睡觉
- 终于，乌龟赢得比赛

```java
public class Race implements Runnable {

    private static String winner;

    @Override
    public void run() {
        for (int i = 0; i <= 100; i++) {
            if ("兔子".equals(TestThread.currentThread().getName())){
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // 判断是否比赛结束
            boolean flag = gameOver(i);
            if (flag) {
                break;
            }
            System.out.println(Thread.currentThread().getName() + " run " + i + " m");
        }
    }

    /**
     * 是否完成比赛
     *
     * @return
     */
    private boolean gameOver(int steps) {
        if (null != winner) {
            // 存在胜利者
            return true;
        }
        if (steps >= 100) {
            winner = Thread.currentThread().getName();
            System.out.println("winner is " + winner);
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Race race = new Race();
        new Thread(race, "兔子").start();
        new Thread(race, "乌龟").start();
    }
}

```

**实现Callable接口**

- 实现Callable接口，需要返回值类型
- 重写call方法，可以抛出异常
- 创建目标对象
- 创建执行服务：ExecutorService service = Executors.newFixedThreadPool(1)
- 提交执行：Future<Boolean> res = service.submit(1);
- 获取结果：boolean r1 = res.get()
- 关闭服务：service.shutdownNow()

```java
public class TestThread5 implements Callable<Boolean> {

    private String url;
    private String name;

    public TestThread5() {
    }

    public TestThread5(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public Boolean call() throws Exception {
        WebDownloader webDownloader = new WebDownloader();
        webDownloader.download(this.url, this.name);
        System.out.println(this.name);
        return true;
    }


    public static void main(String[] args) {
        String url = "https://img0.utuku.china.com/0x0/ent/20161011/7232c15c-7bff-45c8-9a62-db0983e56a07.jpg";
        TestThread5 testThread1 = new TestThread5(url, "c1.jpg");
        TestThread5 testThread2 = new TestThread5(url, "c2.jpg");
        TestThread5 testThread3 = new TestThread5(url, "c3.jpg");
        TestThread5 testThread4 = new TestThread5(url, "c4.jpg");

        // 创建按执行服务
        ExecutorService service = Executors.newFixedThreadPool(1);
        Future<Boolean> submit1 = service.submit(testThread1);
        Future<Boolean> submit2 = service.submit(testThread2);
        Future<Boolean> submit3 = service.submit(testThread3);
        Future<Boolean> submit4 = service.submit(testThread4);
        try {
            System.out.println(submit1.get());
            System.out.println(submit2.get());
            System.out.println(submit3.get());
            System.out.println(submit4.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        service.shutdownNow();
    }
}

class WebDownloader {
    // 下载方法
    public void download(String url, String name) throws IOException {
        FileUtils.copyURLToFile(new URL(url), new File(name));
    }
}

```

**Thread 类的静态代理模式**

```java
/**
 * 静态代理总结：
 * 静态代理模式，代理对象和真实对象都要实现同一个接口
 * 代理对象代理真实对象
 */
public class StaticProxy {
    public static void main(String[] args) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("我爱你");
            }
        }).start();

        new WeddingMarry(new You()).happyMarry();
    }
}

interface Marry {
    void happyMarry();
}

/**
 * 真实角色，你去结婚
 */
class You implements Marry {

    @Override
    public void happyMarry() {
        System.out.println("You.happyMarry");
    }
}

/**
 * 代理角色，帮助结婚
 */
class WeddingMarry implements Marry {

    // 真实目标角色
    private Marry target;

    public WeddingMarry(Marry target) {
        this.target = target;
    }

    @Override
    public void happyMarry() {
        before();
        // 这就是真实对象
        this.target.happyMarry();
        after();
    }

    private void before() {
        System.out.println("WeddingMarry.before");
        System.out.println("结婚之前的准备");
    }


    private void after() {
        System.out.println("WeddingMarry.after");
        System.out.println("结婚之后的收拾");
    }
}
```

**Lamda表达式**

- Java8 之后的新特性：`new Thread(() -> { System.out.println("hello"); }).start();`

- 理解Functional Interface （函数式接口）是学习Java8 Lamda表达式的关键
- 函数式接口的定义
  - 任何接口，只要只包含唯一一个抽象方法，那么他就是函数式接口
  - 对于函数式接口，我们可以通过Lambda表达式来创建该接口的对象

```java
public class TestLambda01 {
    // 静态内部类

    // 3.静态内部类
    static class Like2 implements ILike {

        @Override
        public void lambda() {
            System.out.println("Like2.lambda");
        }
    }

    public static void main(String[] args) {
        ILike like2 = new Like2();
        like2.lambda();

        // 4. 局部内部类
        class Like3 implements ILike {
            @Override
            public void lambda() {
                System.out.println("Like3.lambda");
            }
        }
        ILike like3 = new Like3();
        like3.lambda();

        // 5. 匿名内部类
        ILike like4 = new ILike() {
            @Override
            public void lambda() {
                System.out.println("TestLambda01.lambda");
            }
        };
        like4.lambda();

        // 6. 用Lambda 简化
        like4 = () -> System.out.println("TestLambda5.lambda");
        like4.lambda();
    }
}

// 1.定义一个函数式接口
interface ILike {
    void lambda();
}

// 2.实现类
class Like implements ILike {

    @Override
    public void lambda() {
        System.out.println("Like.lambda");
    }
}
```

- 为什么要使用lambda表达式
  - 避免匿名类定义过多
  - 可以让代码看起来很简洁
  - 去掉了一堆没有意义的代码，只留下核心的逻辑

```java
public class TestLambda02 {

    public static void main(String[] args) {

        // 省略方法体
        ILove love = (int a, String b) -> {
            System.out.println("TestLambda02.love" + a);
        };

        // 省略类型
        ILove love2 = (a, b) -> {
            System.out.println("TestLambda02.love" + a);
        };

        //省略花括号，注意，只有一个语句才能省略
        ILove love3 = (a, b) -> System.out.println("TestLambda02.love" + a);
        love.love(1, "");
        love2.love(2, "");
        love2.love(3, "");

    }
}

interface ILove {
    void love(int a, String b);
}
```

#### 线程状态

- 创建状态
- 就绪状态
- 阻塞状态
- 运行状态
- 死亡状态

![1596078208898](images/1596078208898.png)

![](draw.io/线程状态.png)

**线程方法**

|              方法              |                    说明                    |
| :----------------------------: | :----------------------------------------: |
|  setPriority(int newPriority)  |              更改线程的优先级              |
| static void sleep(long millis) |   在指定的毫秒数让当前正在执行的线程休眠   |
|          void join()           |               等待该线程终止               |
|      static void yield()       | 暂停当前正在执行的线程对象，并执行其他线程 |
|        void interrupt()        |         中断线程，不要使用这个方式         |
|       boolean isAlive()        |          测试线程是否处于活动状态          |

**停止线程**

- 不推荐使用JDK提供的stop()、destroy()方法[ 已经废弃 ]
- 推荐线程自己停下来
- 建议使用一个标志位进行终止变量。当flag= false，则终止线程运行

```java
/**
 * 建议线程正常停止 - 利用次数，不建议死循环
 * 建议使用标志位 - 设置一个标志位
 * 不要使用stop或者destroy方法或者JDK官方不建议使用的方法
 */

public class TestStop implements Runnable {

    // 1.设置一个标志位
    private boolean flag = true;

    @Override
    public void run() {
        int i = 0;
        while (flag) {
            System.out.println(i++);
        }
    }

    // 2. 设置一个公开的方法转换标志位
    public void stop() {
        this.flag = false;
    }

    public static void main(String[] args) {
        TestStop testStop = new TestStop();
        new Thread(testStop).start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        testStop.stop();
    }
}

```

**线程休眠**

模拟网络延迟、倒计时

- sleep(时间) 指定当前线程阻塞的毫秒数
- sleep存在异常 InterruptedException
- sleep时间达到后线程进入就绪状态
- sleep可以模拟网络延迟和倒计时等
- 每个对象都有一个锁，sleep不会释放锁

**线程礼让**

- 礼让线程，让当前正在执行的线程暂停，但不阻塞
- 将线程从运行状态转为就绪状态
- **让CPU重新调度、礼让不一定成功！看CPU心情**

**join方法**

join合并线程，待此线程执行完成之后，再执行其他线程，其他线程阻塞

可以想象为插队

```java
public class TestJoin implements Runnable {

    public static void main(String[] args) throws Exception {
        TestJoin testJoin = new TestJoin();
        Thread thread = new Thread(testJoin);
        thread.start();
        for (int i = 0; i < 100; i++) {
            if (i == 50) {
                // 强行加入，我不完成你不能走
                thread.join();
            }
            System.out.println("mian ... " + i);
        }
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("join... " + i);
        }
    }
}

```

**线程状态观测**

***Thread.State***

线程状态 ，线程可以处于以下状态之一

- NEW 尚未启动的线程处于此状态
- RUNNABLE 在Java虚拟机中执行的线程处于此状态
- BLOCKED 被阻塞等待监听器锁定的线程处于此状态
- WAITING 正在等待另一个线程执行动作线程处于此状态
- TIMED_WAITING 正在等待另一个线程执行动作达到指定等待时间的线程处于此状态
- TERMINATED 已经退出的线程处于此状态

一个线程可以在给定时间点处于一个状态，这些状态时不反映任何操作系统线程状态的虚拟机状态

![1596090928897](images/1596090928897.png)

```java
public class TestState {
    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("TestState.main");
        });

        // 观测状态
        Thread.State state = thread.getState();
        System.out.println(state);
        thread.start();
        state = thread.getState();
        System.out.println(state);
        while (state != Thread.State.TERMINATED) {
            try {
                Thread.sleep(100);
                state = thread.getState();
                System.out.println(state);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

```

**线程的优先级**

- Java提供一个线程调度器来监控中启动进入就绪状态的所有线程，线程调度器按照优先级决定应该调度哪个线程来执行
- 线程的优先级使用数字表示 范围从 1~10
  - Thread.MIN_PRIORITY = 1;
  - Thread.MAX_PRIORITY = 10;
  - Thread.NORM_PRIORITY = 5;
- 使用以下方式改变或获取优先级
  - getPriority
  - setPriority(int xxx)
- 优先级的设定在start()调度之前
- 优先级低只是意味着获得调度的概率低，并不是优先级低就不会被调用了，这都是看CPU的调度

**守护（daemon）线程**

- 线程分为**用户线程**和**守护线程**
- 虚拟机必须确保用户线程执行完毕
- 虚拟机不用等待守护线程执行完毕
- 如：后台记录操作日志、监控内存、垃圾回收等待

#### 线程同步（重点）

多个线程操作同一个资源

**并发**：同一个对象被多个线程同时操作

**锁机制**：synchronized，当一个线程获得对象的排他锁，独占资源，其他线程必须等待，使用后释放锁即可

- 一个线程持有锁会导致其他所有需要此锁的线程挂起
- 在多线程竞争下，加锁，释放锁会导致比较多的上下文切换和调度延时，引起性能问题
- 如果一个优先级高的线程等待一个优先级低的线程释放锁，会导致优先级倒置，引起性能问题

```java
/**
 * 不安全的买票
 * 线程不安全
 */
public class UnSafeBuyTicket {
    public static void main(String[] args) {
        BuyTicket buyTicket = new BuyTicket();
        new Thread(buyTicket, "A").start();
        new Thread(buyTicket, "B").start();
        new Thread(buyTicket, "C").start();
    }
}

class BuyTicket implements Runnable {
    // 外部停止方式
    boolean flag = true;
    // 票
    private int ticketNum = 10;

    @Override
    public void run() {
        // 买票
        while (flag) {
            buy();
        }
    }

    private void buy() {
        // 判断是否有票
        if (ticketNum <= 0) {
            flag = false;
            return;
        }
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 买票
        System.out.println(Thread.currentThread().getName() + " 拿到了 " + ticketNum--);
    }
}

```

```java
/**
 * 不安全取钱
 * 两个人都去银行取钱
 */
public class UnSafeBack {
    public static void main(String[] args) {
        // 账户
        Account account = new Account(100, "结婚基金");
        Drawing you = new Drawing(account, 10, "ic");
        Drawing girlFriend = new Drawing(account, 100, "妻子");
        new Thread(you).start();
        new Thread(girlFriend).start();

    }
}

// 账户
class Account {
    // 余额
    int money;
    // 卡名
    String name;

    public Account(int money, String name) {
        this.money = money;
        this.name = name;
    }
}

// 银行：模拟取款
class Drawing extends Thread {
    Account account;
    int drawingMoney;
    int nowMoney;

    public Drawing(Account account, int drawingMoney, String name) {
        this.account = account;
        this.drawingMoney = drawingMoney;
        this.setName(name);
    }

    @Override
    public void run() {
        if (account.money - drawingMoney < 0) {
            System.out.println(Thread.currentThread().getName() + " 钱不够了");
            return;
        }
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        account.money = account.money - drawingMoney;
        System.out.println(account.name + "余额：" + account.money);
        // Thread.currentThread().getName() = this.getName()
        System.out.println(this.getName() + "手里的钱：" + nowMoney);
    }
}

```

 **同步方法**

- 由于我们可以通过private关键字来保证数据对象只能被方法访问，所以我们只需要针对方法提出一套机制，这套机制就是 synchronized 关键字，它包括两种用法：synchronized 方法和synchronized 代码块
- synchronized方法控制“对象”的访问，每个对象对应一把锁，每个synchronized方法都必须获得锁才能执行，否则就会阻塞，同时，其拿到锁之后也会阻塞，知道方法返回才释放锁，后面被阻塞的线程才能获得这个锁继续执行

**同步代码块**

- synchronized{Obj}
- Obj 称之为 **同步监视器**
  - **Obj** 可以是任何对象，但是推荐使用共享资源作为同步监视器
  - 同步方法中无需指定同步监视器，因为同步方法的同步监视器就是this，就是这个对象本身，或者是class
- 同步监视器的执行过程
  - 第一个线程访问，锁定同步监视器，执行其中的代码
  - 第二个线程访问，发现同步监视器被锁定，无法访问
  - 第一个线程访问完毕，解锁同步监视器
  - 第二个线程访问，发现同步监视器没有锁，然后锁定并访问
- synchronized 默认锁的对象是 **this**
- 锁的对象就是需要变化的量 如银行案例的 **account** 对象

**死锁**

- 多个线程各自占有一些共享资源，并且相互等待其他的线程占有的资源才能运行，二导致两个或者多个线程都在等待对方释放资源，都停止执行的情形。某一个同步块通知拥有"两个以上对象的锁"的时候，就可能会发现“死锁问题”

```java
/**
 * 死锁：多个线程，互相抱着对方需要的资源，然后形成僵持
 */
public class DeadLock {
    public static void main(String[] args) {
        Makeup g1 = new Makeup(0, "小红");
        Makeup g2 = new Makeup(1, "幺妹");
        new Thread(g1).start();
        new Thread(g2).start();
    }
}

// 口红
class Lipstick {

}

// 镜子
class Mirror {
}

class Makeup implements Runnable {

    //需要的资源
    static Lipstick lipstick = new Lipstick();
    static Mirror mirror = new Mirror();

    int choice;
    String girlName;

    public Makeup(int choice, String girlName) {
        this.choice = choice;
        this.girlName = girlName;
    }

    @Override
    public void run() {
        // 化妆
        try {
            makeUp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 化妆，互相持有对方的锁，就是要拿到对方的资源
    private void makeUp() throws Exception {
        if (choice == 0) {
            synchronized (lipstick) {
                System.out.println(this.girlName + " 获得口红的锁 ");
                Thread.sleep(1000);
                synchronized (mirror) {
                    System.out.println(this.girlName + " 获得镜子的锁 ");
                    Thread.sleep(1000);
                }
            }
        } else {
            synchronized (mirror) {
                System.out.println(this.girlName + " 获得口红的锁 ");
                Thread.sleep(1000);
                synchronized (lipstick) {
                    System.out.println(this.girlName + " 获得镜子的锁 ");
                    Thread.sleep(1000);
                }
            }
        }
    }
}
```

**解决方法**

```java
/**
 * 死锁：多个线程，互相抱着对方需要的资源，然后形成僵持
 */
public class DeadLock {
    public static void main(String[] args) {
        Makeup g1 = new Makeup(0, "小红");
        Makeup g2 = new Makeup(1, "幺妹");
        new Thread(g1).start();
        new Thread(g2).start();
    }
}

// 口红
class Lipstick {

}

// 镜子
class Mirror {
}

class Makeup implements Runnable {

    //需要的资源
    static Lipstick lipstick = new Lipstick();
    static Mirror mirror = new Mirror();

    int choice;
    String girlName;

    public Makeup(int choice, String girlName) {
        this.choice = choice;
        this.girlName = girlName;
    }

    @Override
    public void run() {
        // 化妆
        try {
            makeUp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 化妆，互相持有对方的锁，就是要拿到对方的资源
    private void makeUp() throws Exception {
        if (choice == 0) {
            synchronized (lipstick) {
                System.out.println(this.girlName + " 获得口红的锁 ");
                Thread.sleep(1000);
            }
            synchronized (mirror) {
                System.out.println(this.girlName + " 获得镜子的锁 ");
                Thread.sleep(1000);
            }
        } else {
            synchronized (mirror) {
                System.out.println(this.girlName + " 获得口红的锁 ");
                Thread.sleep(1000);
            }
            synchronized (lipstick) {
                System.out.println(this.girlName + " 获得镜子的锁 ");
                Thread.sleep(1000);
            }
        }
    }
}
```

**避免死锁的方法**

- 产生死锁的四个必要条件
  - 互斥条件：一个资源每次只能被一个进行使用
  - 请求与保持条件：一个进程因请求资源而阻塞的时候，对已获得的资源保持不放
  - 不可剥夺条件：进程已经获得的资源，在未使用完之前，不能强行剥夺
  - 循环等待条件：若干进程之前形成了一种头尾相接的循环等待资源关系

**Lock锁**

- 从JDK5.0开始，Java提供了更加强大的线程同步机制——通过显示定义同步锁对象来实现同步，同步锁使用了Lock对象充当
- java.util.concurrent.locks.Lock 接口时控制多个线程对共享资源进行访问的工具，锁提供了对共享资源的独立访问，每次只能有一个线程对Lock对象加锁，线程开始访问共享资源之前应该先获得Lock对象
- ReentrantLock，它拥有与synchronized相同的并发性和内存语义，在实现线程安全的控制中，比较常用的就是ReentrantLock，可以显示加锁，释放锁

```java
/**
 * 测试Lock锁
 */
public class TestLock {
    public static void main(String[] args) {
        TestLock2 testLock2 = new TestLock2();
        new Thread(testLock2).start();
        new Thread(testLock2).start();
        new Thread(testLock2).start();
    }
}

class TestLock2 implements Runnable {

    int ticket = 10;

    private Lock lock = new ReentrantLock();

    @Override
    public void run() {
        lock.lock();
        try {
            while (true) {
                if (ticket > 0) {
                    Thread.sleep(1000);
                    System.out.println(ticket--);
                } else {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
```

**synchronized 和 Lock的对比**

- Lock是显示锁（手动开启和关闭锁，不可忘记关闭锁）synchronized 是隐式锁，除了作用域自动释放
- Lock只有代码锁，没有方法锁，synchronized有代码锁和方法锁
- 使用Lock锁，JVM将花费较少的时间来调度线程，性能更好，并且具有更好的拓展性（提供更多的子类）
- 优先使用的顺序 
  - lock > 同步代码块(已经进入可方法体，分配了相应资源) > 同步方法 (在方法体之外)

#### 线程通信问题

- 应用场景：生产者和消费者问题
  - 假设仓库中只能存放意见产品，生产者将生产出来的产品放入仓库，消费者将仓库中产品取走消费
  - 假如仓库中没有产品，则生产者将产品放入仓库，否则停止生产并等待，直到仓库中的产品被消费者取走为止
  - 如果仓库中放有产品，则消费者可以将产品取走消费，否则停止消费并等待，直到仓库中再次放入产品为止
- 这是一个线程同步的问题，生产者和消费者共享一个资源，并且生产者和消费者之间相互依赖，互为条件
  - 对于生产者，没有生产消费者产品之前，要通知消费者等待，而生产了产品之后，=又需要马上通知消费者消费
  - 对于消费者，在消费之后，要通知生产者已经结束消费，需要生产新的产品以供消费
  - 在生产者消费者问题中，仅仅只有synchronized式不够的
    - synchronized可阻止并发更新一个共享资源，实现了同步
    - synchronized 不能用来实现不同线程之间的消息传递
- Java提供了几个方法解决线程之间的通信问题

| 方法名             | 作用                                                         |
| ------------------ | ------------------------------------------------------------ |
| wait()             | 表示线程会一直等待直到其他线程通知，与sleep不同，会释放锁    |
| wait(long timeout) | 指定等待的毫秒数                                             |
| notify()           | 唤醒一个处于等待状态的线程                                   |
| notifyAll()        | 唤醒同一个对象上的所有调用wait()方法的线程，优先级高的优先调度 |

***注意：都是Object类的方法。都只能在同步方法或者同步代码块中使用，否则会抛出异常 IllegalMonitorStateException***

- **解决方式1**
- **并发协作模型“生产者 / 消费者模式” --> 管程法**
  - 生产者：负责生产数据的模块(可能式方法、对象、线程、进程)
  - 消费者：负责处理数据的模块(可能式方法、对象、线程、进程)
  - 缓冲区：消费者不能直接使用生产者数据，他们之间有个缓冲区
  - **生产者将生产好的数据放入缓存区，消费者从缓冲区拿出数据**

```java
/**
 * 测试：生产者消费者模型 利用缓冲区解决 管程法
 * 生产者 消费者 产品 缓冲区
 */
public class TestPC {
    public static void main(String[] args) {
        SysnContainer sysnContainer = new SysnContainer();
        new Productor(sysnContainer).start();
        new Comsumer(sysnContainer).start();
    }
}

// 生产者
class Productor extends Thread {
    SysnContainer container;

    public Productor(SysnContainer container) {
        this.container = container;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("do " + i + " ji");
            container.push(new Chicken(i));
        }
    }
}

// 消费者
class Comsumer extends Thread {
    SysnContainer container;

    public Comsumer(SysnContainer container) {
        this.container = container;
    }

    //消费

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("eat " + container.pop().id + " ji");
        }
    }
}

// 产品
class Chicken {
    int id;

    public Chicken(int id) {
        this.id = id;
    }
}

// 缓冲区
class SysnContainer {
    Chicken[] chickens = new Chicken[10];
    //生产者放入产品
    int count = 0;

    public synchronized void push(Chicken chicken) {
        // 满了
        if (count == chickens.length) {
            // 通知消费者
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // 没满
        chickens[count++] = chicken;
        // 可以通知消费者消费了
        this.notifyAll();
    }

    // 消费者消费产品
    public synchronized Chicken pop() {
        // 判断能否消费
        if (count == 0) {
            // 等待生产者生产
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // 如果可以消费
        count--;
        Chicken chicken = chickens[count];

        // 通知生产者
        this.notifyAll();
        return chicken;
    }
}
```



- **解决方式2**
  - **并发协作模型“生产者 / 消费者模式” --> 信号灯法**

```java
/**
 * 测试生产者消费者问题：信号灯法 标志位
 */
public class TestPC2 {
    public static void main(String[] args) {
        TV tv = new TV();
        new Player(tv).start();
        new Watcher(tv).start();
    }
}

// 生产者 -> 演员

class Player extends Thread {
    TV tv;

    public Player(TV tv) {
        this.tv = tv;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            if (i % 2 == 0) {
                this.tv.play("开了");
            } else {
                this.tv.play("开了个p");
            }
        }
    }
}
// 消费者 -> 观众

class Watcher extends Thread {
    TV tv;

    public Watcher(TV tv) {
        this.tv = tv;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            tv.watch();
        }
    }
}

// 产品 -> 节目
class TV {
    // 演员，
    String voice;
    boolean flag = true;

    // 表演
    public synchronized void play(String voice) {
        if (!flag) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("演员表演:" + voice);
        this.notifyAll();
        this.voice = voice;
        this.flag = !this.flag;
    }
    // 观看

    public synchronized void watch() {
        if (flag) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("看了：" + voice);
        this.notifyAll();
        this.flag = !this.flag;
    }
}
```

#### 总结与彩蛋

**下一篇：Java - 多线程 - 线程池 和 Java - JUC - 并发编程**

- 敬请期待...