### Java - 多线程 - 增强篇 - 阻塞队列

**所有的阻塞队列都实现了 BlockingQueue接口**

#### BlockingQueue接口

```java
public interface BlockingQueue<E> extends Queue<E> {

    boolean add(E e);

    boolean offer(E e);

    void put(E e) throws InterruptedException;

    boolean offer(E e, long timeout, TimeUnit unit)
        throws InterruptedException;

    E take() throws InterruptedException;

    E poll(long timeout, TimeUnit unit)
        throws InterruptedException;
    
    int remainingCapacity();

    boolean remove(Object o);
    
    public boolean contains(Object o);

    int drainTo(Collection<? super E> c);

    int drainTo(Collection<? super E> c, int maxElements);
}
```

#### ArrayBlockingQueue实现类

- 线程安全的 因为方法使用Lock锁
- 根据源码来看，BlockingQueue的实现类都是使用Lock来保证线程安全

**添加、移除**

**四组API**

| 方式         | 抛出异常  | 有返回值，不抛出异常 | 阻塞异常 | 超时等待             |
| ------------ | --------- | -------------------- | -------- | -------------------- |
| 添加         | add(ele)  | offer(ele)           | put(ele) | offer(ele,time,unit) |
| 移除         | remove()  | poll()               | take()   | poll(time,unit)      |
| 判断队列首部 | element() | peek()               | -        | -                    |

- 抛出异常

```java
	/**
     * 抛出异常
     * java.lang.IllegalStateException: Queue full
     * java.util.NoSuchElementException
     */
public static void test1() {
    // 队列的大小
    ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
    System.out.println(blockingQueue.add("a"));
    System.out.println(blockingQueue.add("b"));
    System.out.println(blockingQueue.add("c"));
    System.out.println(blockingQueue.remove());
    System.out.println(blockingQueue.remove());
    System.out.println(blockingQueue.remove());
    System.out.println(blockingQueue.remove());
    System.out.println(blockingQueue.add("D"));
}
```

- 不抛出异常

```java
	/**
     * 不抛出异常
     * 添加 不抛出异常 返回 false
     * 没有元素可弹出，就返回 null
     */
public static void test2() {
    // 队列的大小
    ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
    System.out.println(blockingQueue.offer("a"));
    System.out.println(blockingQueue.offer("b"));
    System.out.println(blockingQueue.offer("c"));
    System.out.println(blockingQueue.offer("d"));

    System.out.println(blockingQueue.poll());
    System.out.println(blockingQueue.poll());
    System.out.println(blockingQueue.poll());
    System.out.println(blockingQueue.poll());
}
```

- 阻塞异常

```java
	/**
     * 等待 阻塞
     */
public static void test3() throws Exception {
    // 队列的大小
    ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
    // 一直阻塞
    blockingQueue.put("a");
    blockingQueue.put("b");
    blockingQueue.put("c");
    System.out.println(blockingQueue.take());
    System.out.println(blockingQueue.take());
    System.out.println(blockingQueue.take());
    System.out.println(blockingQueue.take());
}
```

- 超时等待

```java
	/**
     * 等待 超时退出
     */
public static void test4() throws Exception {
    // 队列的大小
    ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
    blockingQueue.offer("a");
    blockingQueue.offer("b");
    blockingQueue.offer("c");
    blockingQueue.offer("d", 2, TimeUnit.SECONDS);

    System.out.println(blockingQueue.poll(2, TimeUnit.SECONDS));
    System.out.println(blockingQueue.poll(2, TimeUnit.SECONDS));
    System.out.println(blockingQueue.poll(2, TimeUnit.SECONDS));
    System.out.println(blockingQueue.poll(2, TimeUnit.SECONDS));
}
```

**注意：ArrayBlockingQueue的阻塞是通过 Lock 锁实现的**

```java
public E poll() {
    final ReentrantLock lock = this.lock;
    lock.lock();
    try {
        return (count == 0) ? null : dequeue();
    } finally {
        lock.unlock();
    }
}

public E take() throws InterruptedException {
    final ReentrantLock lock = this.lock;
    lock.lockInterruptibly();
    try {
        while (count == 0)
            notEmpty.await();
        return dequeue();
    } finally {
        lock.unlock();
    }
}

public E poll(long timeout, TimeUnit unit) throws InterruptedException {
    long nanos = unit.toNanos(timeout);
    final ReentrantLock lock = this.lock;
    lock.lockInterruptibly();
    try {
        while (count == 0) {
            if (nanos <= 0)
                return null;
            nanos = notEmpty.awaitNanos(nanos);
        }
        return dequeue();
    } finally {
        lock.unlock();
    }
}

public E peek() {
    final ReentrantLock lock = this.lock;
    lock.lock();
    try {
        return itemAt(takeIndex); // null when queue is empty
    } finally {
        lock.unlock();
    }
}
```



#### SynchronousQueue：同步队列

- 没有容量
- 进去一个元素，必须等待取出来之后，才能往里放一个元素

```java
/**
 * 同步队列
 *
 * 和其他的BlockingQueue 不一样，不能存储元素 
 * 放进去的必须取出来，也可以理解就只能放一个元素
 */
public class SynchronousQueueDemo {
    public static void main(String[] args) {
        SynchronousQueue<Object> sync = new SynchronousQueue<>();
        new Thread(() -> {
            try {
                System.out.println(Thread.currentThread().getName() + " put 1");
                sync.put("1");
                System.out.println(Thread.currentThread().getName() + " put 2");
                sync.put("2");
                System.out.println(Thread.currentThread().getName() + " put 3");
                sync.put("3");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName() + " 取出 " + sync.take());
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName() + " 取出 " + sync.take());
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName() + " 取出 " + sync.take());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
}

```