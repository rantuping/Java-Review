### Java - 常用类 - Object 

#### Object 类相对比较简单，我们直接看源码即可

```java
package java.lang;

public class Object {

    private static native void registerNatives();
    static {
        registerNatives();
    }

    // 本地方法，获取当前类的Class对象，此对象属于Class的，是唯一的
    public final native Class<?> getClass();

    // 本地方法，计算HashCode的值，
    public native int hashCode();

    // equals方法 判断调用此方法的对象和参数列表中的对象是否相等
    public boolean equals(Object obj) {
        return (this == obj);
    }

    // 本地方法 用来克隆一个对象，需要克隆的对象必须实现 Cloneable 接口
    // 否则抛出 CloneNotSupportedException
    protected native Object clone() throws CloneNotSupportedException;

    // toString 方法 输入一个对象默认就是输出的当前对象的toString方法
    public String toString() {
        return getClass().getName() + "@" + Integer.toHexString(hashCode());
    }

    // 本地方法，唤醒一个线程
    public final native void notify();

    // 本地方法 唤醒所有线程
    public final native void notifyAll();

    // 本地方法 线程等待，会发生线程中断异常
    public final native void wait(long timeout) throws InterruptedException;

    // 含有具体时间的等待方法
    public final void wait() throws InterruptedException {
        wait(0);
    }

    // 此方法是由 JVM调用，用来判断回收垃圾的
    protected void finalize() throws Throwable { }
}   
```

#### 一些注意点

- Object是所有类的间接或者直接父类
- 但是接口不是Object的子类
- 在后面学习的一些方法将会来自Object类
- 一般的面试题就是说一说Object类中的方法和作用