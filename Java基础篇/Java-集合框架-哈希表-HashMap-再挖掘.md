### Java 集合框架 - 哈希表 HashMap 再挖掘

**阅读本篇之前，请先看上一篇 [Java 集合框架 - 哈希表 HashMap]，再来看这篇更舒畅~**

#### 1.概述

我们先来看一张图，回顾一下之前学习的`ArrayList、LinkedList、HashMap`

![](images/数组和链表1.png)

**这些数据结构相信已经很熟悉了，我就不在赘述**

回顾一个小知识点：

> **补充说明**
>
> 数组和链表迭代的方式不同
> ArrayList实现了RandomAccess接口
> 这是一个标记接口，标注是否可以随机访问
> ArrayList使用数组实现，可以随机访问
> 经过测试 使用for循环遍历ArrayList更快
> 而LinkedList没有实现这个RandomAccess接口
> 不支持随机访问，使用迭代器遍历更快
> RandomAccess接口的作用
> 就是用来判断选择哪个方式遍历 

#### 2.什么是Hash

**哈希：英文是Hash，也称为散列**
基本原理就是把任意长度输入，转化为固定长度输出
这个映射的规则就是Hash算法，而原始数据映射的二进制串就是Hash值

**Hash的特点**

- 1.从Hash值不可以反向推导出原始数据
- 2.输入数据的微小变化会得到完全不同的Hash值相同的数据一定可以得到相同的值
- 3.哈希算法的执行效率要高效，长的文本也能快速计算Hash值
- 4.Hash算法的冲突概率要小

由于Hash原理就是将输入空间映射成Hash空间，而Hash空间远远小于输入空间，根据抽屉原理，一定存在不同输出有相同的映射

**抽屉原理**
桌子上有10个苹果，将其放在9个抽屉里面，那必有一个抽屉不少于2个苹果 

#### 3.HashMap原理讲解

**HashMap的继承体系**

HashMap继承了AbstractMap，实现了Cloneable接口、Serializable接口、Map<K,V>接口

**Node的数据结构分析**

```java
final int hash;
final K key;
V value;
Node<K,V> next;
```

**底层数据结构**

![](images/数组和链表-jdk8.png)

**put数据原理分析**

![](images/数组和链表-第3页.png)

**什么是Hash碰撞**

假如我有存储一个元素，发现其Key的Hash值还是1122，那么经过扰动之后，其位置还是2，所以此时，就有冲突，这个时候就要解决冲突。

**解决Hash碰撞的方法**

- 开放寻址法
- 拉链法 [HashMap就是使用了此方法]

**什么是链化**

 在JDK1.7之前，假如数据量很大，那么碰撞的概率也很大，此时，拉链法的链子就会很长，那么就会降低查找速度

所以在JDK1.8之后引入红黑树

**HashMap的扩容原理**

因为当数据表很多的时候，碰撞使得冲突和查找速度都上升，此时就要扩容

#### 4.手撕源码

**HashMap核心属性分析（threshold、loadFactor、size、modCount）**

 threshold：扩容阈值

loadFactor：负载因子

size：map实际的元素个数

modCount：map修改元素的次数，如删除和增加，但是对同一个位置进行修改value，不增加

**构造方法分析**

**一个有4个构造函数**

```java
public HashMap(int initialCapacity, float loadFactor) {
    if (initialCapacity < 0)
        throw new IllegalArgumentException("Illegal initial capacity: " +
                                           initialCapacity);
    if (initialCapacity > MAXIMUM_CAPACITY)
        initialCapacity = MAXIMUM_CAPACITY;
    if (loadFactor <= 0 || Float.isNaN(loadFactor))
        throw new IllegalArgumentException("Illegal load factor: " +
                                           loadFactor);
    this.loadFactor = loadFactor;
    this.threshold = tableSizeFor(initialCapacity);
}

public HashMap(int initialCapacity) {
    this(initialCapacity, DEFAULT_LOAD_FACTOR);
}
public HashMap() {
    this.loadFactor = DEFAULT_LOAD_FACTOR; // all other fields defaulted
}   
public HashMap(Map<? extends K, ? extends V> m) {
    this.loadFactor = DEFAULT_LOAD_FACTOR;
    putMapEntries(m, false);
}

// 此方法核心功能就是求出，大于等于输入长度的2次幂的值
// 如输出：8，输出为8
// 如输出：9，输出为16
static final int tableSizeFor(int cap) {
    int n = cap - 1;
    n |= n >>> 1; //  n = n | (n >>> 1);
    n |= n >>> 2;
    n |= n >>> 4;
    n |= n >>> 8;
    n |= n >>> 16;
    return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
}
```

**核心知识点：为什么 table 的长度 一定是2的幂**

计算Hash值得算法，实际就是取模，**hash%length**，

计算机中直接求余效率不如位移运算，源码中做了优化**hash&(length-1)**

要想保证**hash%length==hash&(length-1)**

那么**length**必须是**2的n次方**； 

**HashMap put 方法分析 - putVal**

```java
public V put(K key, V value) {
    return putVal(hash(key), key, value, false, true);
}

// 扰动函数
// 作用：如何table比较短的时候，让key的hash值得高16位也参加路由运算
// 异或：相同为0，不同返回1

// h = 0b 0010 0101 1010 1100 0011 1111 0010 1110

// 0b 0010 0101 1010 1100 0011 1111 0010 1110 [h]
// ^
// 0b 0000 0000 0000 0000 0010 0101 1010 1100 [h >>> 16]
// => 0010 0101 1010 1100 0001 1010 1000 0010
// 在 table 还不是很长的情况下，让高16位也参与进来，为了减少冲突和碰撞
static final int hash(Object key) {
    int h;
    return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
}

// put的 核心方法
// hash：key的hash值
// key：key
// value: value
// onlyIfAbsent：如果为true，则不要更改现有值
// evict：如果为false，则表处于创建模式。
final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
               boolean evict) {
    // tab：引用当前hashMap的散列表
    // p：当前散列表的元素
    // n：表示当前散列表的长度
    // i：表示路由寻址的结果
    Node<K,V>[] tab; Node<K,V> p; int n, i;
    
    // 延迟初始化逻辑，第一次调用 putVal 的时候会初始化hashMap对象中最耗费内存的散列表
    // 如果 table 为null，或者长度为0，就开始创建
    if ((tab = table) == null || (n = tab.length) == 0)
        // 第一次插入数据的时候才会初始化
        n = (tab = resize()).length;
    
    // 最简单的一种情况，寻址找到的桶位，刚好是 null 此时就直接赋值到计算的位置
    // tab 和 n 在上一个 if 赋值
    // 执行一次路由运算 (n - 1) & hash] 得到hash的地址
    // 如果 tab 中没有这个元素或者等于null
    if ((p = tab[i = (n - 1) & hash]) == null)
        // 创建一个新的Node 就把 k-v 封装一个Node放在 tab的i位置
        tab[i] = newNode(hash, key, value, null);
    
    // 此时可能是数组、可能是链表、可能是红黑树
    else {
        // e：不为null的话，找到了一个与当前要插入的k-v一致的元素
        // k：表示临时的k
        Node<K,V> e; K k;
        // p：在另一个分支if中获得
        // 表示桶为中的该元素，与你当前插入的元素的key完全一致，后续进行替换操作
        if (p.hash == hash &&
            ((k = p.key) == key || (key != null && key.equals(k))))
            e = p;
        // p：已经树化了
        else if (p instanceof TreeNode)
            e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
        //是链表
        else {
            // 链表的头元素与要插入的key不一致，
            for (int binCount = 0; ; ++binCount) {
                // 如果到末尾了，就把p加到最后一个位置
                if ((e = p.next) == null) {
                    p.next = newNode(hash, key, value, null);
                    // 如果当前链表的大小 binCount 大于基准树化的值，就执行树化操作
                    if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                        // 树化操作
                        treeifyBin(tab, hash);
                    break;
                }
                // 如何 hash 相等 且key也相等，需要进行替换操作
                if (e.hash == hash &&
                    ((k = e.key) == key || (key != null && key.equals(k))))
                    break;
                // 循环
                p = e;
            }
        }
        // 如果e不为null，找到老的值，返回
        if (e != null) { // existing mapping for key
            V oldValue = e.value;
            if (!onlyIfAbsent || oldValue == null)
                // 把新的值覆写
                e.value = value;
            afterNodeAccess(e);
            return oldValue;
        }
    }
    //修改次数增加，替换Node元素替换不算
    ++modCount;
    // 如果table大小大于阈值，就执行 resize()，进行扩容
    if (++size > threshold)
        resize();
    afterNodeInsertion(evict);
    return null;
}

```

**HashMap resize 扩容方法分析 核心**

```java
// resize()  方法
// 为什么需要扩容？
// 当元素越来越多的时候，hashMap的查找速度就从O(1)升到O(n)，导致链化严重，
// 为了解决冲突带来的查询效率的下降，因此需要扩容[扩容是一个很不好的动作]
final Node<K,V>[] resize() {
    // oldTab：引用扩容之前的哈希表
    Node<K,V>[] oldTab = table;
    // oldCap：表示扩容之前table数组的长度
    int oldCap = (oldTab == null) ? 0 : oldTab.length;
    // oldThr：表示扩容之前的阈值，触发本次扩容的预祝
    int oldThr = threshold;
    // newCap：扩容之后table数组的大小
    // newThr：扩容之后下次触发扩容的条件
    int newCap, newThr = 0;
    // 条件如果成立，说明hashMap中的散列表已经初始化过了，是一次正常的扩容
    if (oldCap > 0) {
        // 当前数组的长度已经大于 hashMap所能容纳的最大大小 就不在扩容，直接返回原数组
        // 设置扩容最大阈值为 int 的最大值
        if (oldCap >= MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return oldTab;
        }
        // 新的table大小为源table的2倍
        // 通知扩容之后的newCap小于数组的最大值限制，其扩容之前的阈值为16
        // 这种情况下，则下一次的扩容的阈值，翻倍
        else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                 oldCap >= DEFAULT_INITIAL_CAPACITY)
            // 扩容的阈值也变为原来的2倍
            newThr = oldThr << 1; // double threshold
    }
    // oldCap == 0 [说明hashMap中的散列表式null] 
    // 1.new HashMap(initCap,loadFactor)
    // 2.new HashMap(intiCap)
    // 3.new HashMap(map) 并且Map有数据
    else if (oldThr > 0) // initial capacity was placed in threshold
        newCap = oldThr;
    
    // oldCap == 0 && oldThr == 0
    // new HashMap();的时候
    else {               // zero initial threshold signifies using defaults
        newCap = DEFAULT_INITIAL_CAPACITY; // 16
        newThr = (int)(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY); //12
    }
    // newThr为0的时候，通过newCap和loadFactor计算出一个newThr
    if (newThr == 0) {
        float ft = (float)newCap * loadFactor;
        newThr = (newCap < MAXIMUM_CAPACITY && ft < (float)MAXIMUM_CAPACITY ?
                  (int)ft : Integer.MAX_VALUE);
    }
    // 更新阈值为计算出来的 newThr
    threshold = newThr;
    @SuppressWarnings({"rawtypes","unchecked"})
    // 创建一个很大的数组
    Node<K,V>[] newTab = (Node<K,V>[])new Node[newCap];
    // 然后更新 table 的引用
    table = newTab;
    // oldTab不为null，说明hashMap本次扩容之前，table不为null
    if (oldTab != null) {
        // 迭代 一个一个位置去处理
        for (int j = 0; j < oldCap; ++j) {
            // e：当前的node节点
            Node<K,V> e;
            // 迭代桶节点，如果节点不为空，才需要计算
            // 但是桶里面的数据具体式哪种（单个数据、链表、树）不确定，需要继续判断
            if ((e = oldTab[j]) != null) {
                // 把原来的数组数据置空，等待GC回收，原来的数据已经存在e里面
                oldTab[j] = null;
                // 说明式单个元素数据，
                if (e.next == null)
                    // 直接计算hash值放入即可
                    newTab[e.hash & (newCap - 1)] = e;
                // 如果已经树化
                else if (e instanceof TreeNode)
                    // 在红黑树部分讲解
                    ((TreeNode<K,V>)e).split(this, newTab, j, oldCap);
                // 如果是链表
                else { // preserve order
                    // 桶位已经形成链表
                    
                    // 低位链表：存放在扩容之后的数组的下标的位置，与当前数组下标位置一致
                    Node<K,V> loHead = null, loTail = null;
                  	// 高位链表：存放在扩容之后数组的下标位置，
                    // 当前数组下标位置 + 扩容之前数组的长度
                    Node<K,V> hiHead = null, hiTail = null;
                    
                    // 当前链表的一个元素
                    Node<K,V> next;
                    do {
                        next = e.next;
                        // hash -> .... 1 1111 
                        // hash -> .... 0 1111 
                        // 0b 1 0000
                        if ((e.hash & oldCap) == 0) {
                            if (loTail == null)
                                loHead = e;
                            else
                                loTail.next = e;
                            loTail = e;
                        }
                        else {
                            if (hiTail == null)
                                hiHead = e;
                            else
                                hiTail.next = e;
                            hiTail = e;
                        }
                    } while ((e = next) != null);
                    // 低位链表有数据
                    if (loTail != null) {
                        loTail.next = null;
                        newTab[j] = loHead;
                    }
                    // 高位链表有数据
                    if (hiTail != null) {
                        hiTail.next = null;
                        newTab[j + oldCap] = hiHead;
                    }
                }
            }
        }
    }
    // 结束之后返回新的
    return newTab;
}

```

**HashMap get 方法分析**

```java
// 获得一个方法
public V get(Object key) {
    Node<K,V> e;
    // 先调用 hash(key)计算hash值，然后调用 getNode方法
    return (e = getNode(hash(key), key)) == null ? null : e.value;
}

// getNode方法
final Node<K,V> getNode(int hash, Object key) {
    // tab：引用当前hashMap的散列表
    // first：桶位中的头元素
    // e：临时node元素
   	// n：table数组元素
    Node<K,V>[] tab; Node<K,V> first, e; int n; K k;
    // 首先判断 table 不是空且长度不为0，并且first部位null
    if ((tab = table) != null && (n = tab.length) > 0 &&
        (first = tab[(n - 1) & hash]) != null) {
        // 对第一个first进行判断，如果hash值相等并且 key 相等，返回当前节点
        if (first.hash == hash && // always check first node
            ((k = first.key) == key || (key != null && key.equals(k))))
            return first;
        // 如果first的下一个不为null
        if ((e = first.next) != null) {
            // 如果是树，就调用树的查找方法
            if (first instanceof TreeNode)
                return ((TreeNode<K,V>)first).getTreeNode(hash, key);
            // 如果是链表，就循环进行判断
            do {
                if (e.hash == hash &&
                    ((k = e.key) == key || (key != null && key.equals(k))))
                    return e;
            } while ((e = e.next) != null);
        }
    }
    // 都没有，就返回null
    return null;
}
```

**HashMap remove方法分析**

```java
// 移除元素的方法
public V remove(Object key) {
    Node<K,V> e;
    // 调用hash方法，获得哈希值，然后调用removeNode
    return (e = removeNode(hash(key), key, null, false, true)) == null ?
        null : e.value;
}

// 核心方法 removeNode
// hash：hash值
// key：key
// value：value 如果matchValue则匹配的值，否则忽略
// matchValue：如果为true，则仅在值相等时删除
// movable：如果删除虚假不动其他节点
final Node<K,V> removeNode(int hash, Object key, Object value,
                           boolean matchValue, boolean movable) {
    // tab：引用当前hashMap的散列表
    // p：当前node元素
    // n：表示散列表数组长度
    // index：表示寻址结果
    Node<K,V>[] tab; Node<K,V> p; int n, index;
    
    // 判断 table 是否为空，是否长度为0，且对应的hash值在数组里面存在，才继续向下走
    if ((tab = table) != null && (n = tab.length) > 0 &&
        (p = tab[index = (n - 1) & hash]) != null) {
        // 说明桶位是由数据的，需要进行查找操作，并且删除
        // node：查找到的结果
        // e：当前Node的下一个元素
        Node<K,V> node = null, e; K k; V v;
        // 判断头元素是不是要删除的元素，如果是就放进去node
        if (p.hash == hash &&
            ((k = p.key) == key || (key != null && key.equals(k))))
            node = p;
        // 桶的第一个不是
        else if ((e = p.next) != null) {
            // 树化结构
            if (p instanceof TreeNode)
                // 调用树化的结果
                node = ((TreeNode<K,V>)p).getTreeNode(hash, key);
            else {
                // 链表结构 循环遍历得到结构
                do {
                    if (e.hash == hash &&
                        ((k = e.key) == key ||
                         (key != null && key.equals(k)))) {
                        node = e;
                        break;
                    }
                    p = e;
                } while ((e = e.next) != null);
            }
        }
        // 判断是否得到了目标要删除的节点
        if (node != null && (!matchValue || (v = node.value) == value ||
                             (value != null && value.equals(v)))) {
            // 如果是树节点，调用树的删除操作
            if (node instanceof TreeNode)
                ((TreeNode<K,V>)node).removeTreeNode(this, tab, movable);
            // 如果node = p 表示是第一个数据
            else if (node == p)
                // 更新地址为下一个数据，放到桶
                tab[index] = node.next;
            else
                // 如果node 不等于 p 那就直接指向链表的下一个元素地址
                p.next = node.next;
            // 修改次数增加
            ++modCount;
            // 大小减1
            --size;
            afterNodeRemoval(node);
            // 返回删除的node
            return node;
        }
    }
    // 如果都没有执行，那么就返回null
    return null;
}
```

**HashMap replace方法分析**

```java
// 根据 k 和 v 替换
@Override
public V replace(K key, V value) {
    Node<K,V> e;
    if ((e = getNode(hash(key), key)) != null) {
        V oldValue = e.value;
        e.value = value;
        afterNodeAccess(e);
        return oldValue;
    }
    return null;
}

// 根据 k oldValue newValue 替换 
@Override
public boolean replace(K key, V oldValue, V newValue) {
    Node<K,V> e; V v;
    if ((e = getNode(hash(key), key)) != null &&
        ((v = e.value) == oldValue || (v != null && v.equals(oldValue)))) {
        e.value = newValue;
        afterNodeAccess(e);
        return true;
    }
    return false;
}

```

