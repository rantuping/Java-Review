### SSM - Mybatis3 - 源码解读 - 第4篇 - 反射模块

#### 概述

- Mybatis的反射模块对应 `reflection` 包，如下所示

![1599017578804](images/1599017578804.png)

- 相比较 `parsing` 包来说，`reflection` 包代码量更多一些
- 这篇讲解将会很多。**如果想要比较好的理解整个模块，多调试一下**
- Java中的反射虽然功能强大，但是高质量的反射还是很有难度，Mybatis对Java反射进行封装，提供了更加简单易用的API，方便上层调用。并且对 **反射操作进行了一系列的优化**，例如缓存了类的元数据，提高了反射操作的性能

#### Reflector

-  `org.apache.ibatis.reflection.Reflector` ，反射器， 每个Reflector对应一个类，Reflector会缓存反射操作需要的类的信息，如：构造方法、属性名、setting/getting方法等。

```java
// Reflector.java
public class Reflector {

    // 对应的类
    private final Class<?> type;
    // 可读属性数组
    private final String[] readablePropertyNames;
    // 可写属性数组
    private final String[] writablePropertyNames;
    // 属性对应set方法的映射
    // String指的是 属性名称 Invoker对象为value
    private final Map<String, Invoker> setMethods = new HashMap<>();
    // 属性对应的get方法
    // String指的是 属性名称 Invoker对象为value
    private final Map<String, Invoker> getMethods = new HashMap<>();
    // 对应的是set方法参数类型的映射
    private final Map<String, Class<?>> setTypes = new HashMap<>();
    // 对应的是get方法类型的映射
    private final Map<String, Class<?>> getTypes = new HashMap<>();
    // 对应类的默认构造器 、
    // - 默认是无参构造方法
    private Constructor<?> defaultConstructor;
    // 不区分大小写的属性集合
    private Map<String, String> caseInsensitivePropertyMap = new HashMap<>();

    // 构造方法
    public Reflector(Class<?> clazz) {
        // 设置对应的类
        type = clazz;
        // 初始化默认构造器
        addDefaultConstructor(clazz);
        // 添加get方法
        addGetMethods(clazz);
        // 添加set方法
        addSetMethods(clazz);
        // 添加属性
        addFields(clazz);
        // 初始化 可读数组、可写数组、不区分大小写的属性集合
        readablePropertyNames = getMethods.keySet().toArray(new String[0]);
        writablePropertyNames = setMethods.keySet().toArray(new String[0]);
        for (String propName : readablePropertyNames) {
            caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
        }
        for (String propName : writablePropertyNames) {
            caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
        }
    }

    // 添加默认构造器
    // 默认就是无参构造器
    private void addDefaultConstructor(Class<?> clazz) {
        // 获得所有的构造方法
        Constructor<?>[] constructors = clazz.getDeclaredConstructors();
        // 遍历所有的构造方法 查找无参的构造方法 通过JDK8新特性的Stream流
        Arrays.stream(constructors).filter(constructor -> constructor.getParameterTypes().length == 0)
            // 可以访问，就赋值给 defaultConstructor
            .findAny().ifPresent(constructor -> this.defaultConstructor = constructor);
    }

    // 初始化getMethods和geyTypes，通过遍历类所有的get方法
    private void addGetMethods(Class<?> clazz) {
        // 属性和get方法的映射
        Map<String, List<Method>> conflictingGetters = new HashMap<>();
        // 获取所有的方法
        Method[] methods = getClassMethods(clazz);
        // 遍历所有方法，通过方法的名字判断是不是get方法
        Arrays.stream(methods).filter(m -> m.getParameterTypes().length == 0 && PropertyNamer.isGetter(m.getName()))
            // 满足条件的，添加到 conflictingGetters 中，
            .forEach(m -> addMethodConflict(conflictingGetters, PropertyNamer.methodToProperty(m.getName()), m));
        // 解决 conflictingGetters 冲突
        resolveGetterConflicts(conflictingGetters);
    }

    // 调用 addMethodConflict 方法，将符合条件的方法放入集合
    private void addMethodConflict(Map<String, List<Method>> conflictingMethods, String name, Method method) {
        if (isValidPropertyName(name)) {
            List<Method> list = conflictingMethods.computeIfAbsent(name, k -> new ArrayList<>());
            list.add(method);
        }
    }

    // 为了解决 conflictingGetters 冲突
    // 一个属性只能留下一个冲突
    private void resolveGetterConflicts(Map<String, List<Method>> conflictingGetters) {
        // 遍历集合 查找最匹配的方法，因为子类可能复写父类的方法，所以一个属性，可能对应多个get方法
        for (Entry<String, List<Method>> entry : conflictingGetters.entrySet()) {
            // 创建胜出的方法对象
            Method winner = null;
            // 获取名字
            String propName = entry.getKey();
            boolean isAmbiguous = false;
            for (Method candidate : entry.getValue()) {
                // winner 为空，则为最匹配的方法
                if (winner == null) {
                    winner = candidate;
                    continue;
                }
                // 基于返回值类型进行比较
                Class<?> winnerType = winner.getReturnType();
                Class<?> candidateType = candidate.getReturnType();
                // 类型相同
                if (candidateType.equals(winnerType)) {
                    if (!boolean.class.equals(candidateType)) {
                        isAmbiguous = true;
                        break;
                        // Boolean类型的is方法
                    } else if (candidate.getName().startsWith("is")) {
                        winner = candidate;
                    }
                    // 不符合子类
                } else if (candidateType.isAssignableFrom(winnerType)) {
                    // OK getter type is descendant
                    // 符合选择子类，因为子类修改可以放大返回值
                } else if (winnerType.isAssignableFrom(candidateType)) {
                    winner = candidate;
                } else {
                    isAmbiguous = true;
                    break;
                }
            }
            // 执行添加方法的方法
            addGetMethod(propName, winner, isAmbiguous);
        }
    }

    // 添加方法
    private void addGetMethod(String name, Method method, boolean isAmbiguous) {
        // 方法调用判断。不符合条件就抛出异常
        MethodInvoker invoker = isAmbiguous
            ? new AmbiguousMethodInvoker(method, MessageFormat.format(
                "Illegal overloaded getter method with ambiguous type for property ''{0}'' in class ''{1}''. This breaks the JavaBeans specification and can cause unpredictable results.",
                name, method.getDeclaringClass().getName()))
            // 否则就创建
            : new MethodInvoker(method);
        // 没有抛出异常，说明成功吗，添加
        getMethods.put(name, invoker);
        // 同时添加 类型
        Type returnType = TypeParameterResolver.resolveReturnType(method, type);
        getTypes.put(name, typeToClass(returnType));
    }

    // 获取类的方法
    private Method[] getClassMethods(Class<?> clazz) {
        // 方法签名与方法对象映射对象
        Map<String, Method> uniqueMethods = new HashMap<>();
        Class<?> currentClass = clazz;
        // 循环类直到父类为Object
        while (currentClass != null && currentClass != Object.class) {
            // 记录当前类定义的方法
            addUniqueMethods(uniqueMethods, currentClass.getDeclaredMethods());

            // we also need to look for interface methods -
            // because the class may be abstract
            // 记录接口中的方法
            Class<?>[] interfaces = currentClass.getInterfaces();
            for (Class<?> anInterface : interfaces) {
                addUniqueMethods(uniqueMethods, anInterface.getMethods());
            }

            // 获得父类
            currentClass = currentClass.getSuperclass();
        }

        // 转换为 Method 数组返回
        Collection<Method> methods = uniqueMethods.values();

        return methods.toArray(new Method[0]);
    }

    private void addUniqueMethods(Map<String, Method> uniqueMethods, Method[] methods) {
        // 迭代方法
        for (Method currentMethod : methods) {
            // 忽略 bridge 方法，这是考虑在接口中的，因为接口不是Object的子类，但是在接口内存不桥接了Object类的方法
            if (!currentMethod.isBridge()) {
                // 获取方法签名
                String signature = getSignature(currentMethod);
                // check to see if the method is already known
                // if it is known, then an extended class must have
                // overridden a method
                // 当方法签名不存在的时候，进行添加
                if (!uniqueMethods.containsKey(signature)) {
                    uniqueMethods.put(signature, currentMethod);
                }
            }
        }
    }

    // 初始化 setMethods和setTypes
    private void addSetMethods(Class<?> clazz) {
        // 得到数组
        Map<String, List<Method>> conflictingSetters = new HashMap<>();
        // 获取所有的方法
        Method[] methods = getClassMethods(clazz);
        // 进行判断
        Arrays.stream(methods).filter(m -> m.getParameterTypes().length == 1 && PropertyNamer.isSetter(m.getName()))
            .forEach(m -> addMethodConflict(conflictingSetters, PropertyNamer.methodToProperty(m.getName()), m));
        // 解决set冲突的方法
        resolveSetterConflicts(conflictingSetters);
    }

    // 解决set冲突
    private void resolveSetterConflicts(Map<String, List<Method>> conflictingSetters) {
        // 迭代每个属性，查找最匹配的方法
        for (Entry<String, List<Method>> entry : conflictingSetters.entrySet()) {
            String propName = entry.getKey();
            List<Method> setters = entry.getValue();
            Class<?> getterType = getTypes.get(propName);
            boolean isGetterAmbiguous = getMethods.get(propName) instanceof AmbiguousMethodInvoker;
            boolean isSetterAmbiguous = false;
            Method match = null;
            for (Method setter : setters) {
                if (!isGetterAmbiguous && setter.getParameterTypes()[0].equals(getterType)) {
                    // should be the best match
                    match = setter;
                    break;
                }
                if (!isSetterAmbiguous) {
                    match = pickBetterSetter(match, setter, propName);
                    isSetterAmbiguous = match == null;
                }
            }
            // 最匹配的不为null
            if (match != null) {
                // 添加到属性里面取
                addSetMethod(propName, match);
            }
        }
    }

    // 添加方法
    private void addSetMethod(String name, Method method) {
        MethodInvoker invoker = new MethodInvoker(method);
        setMethods.put(name, invoker);
        Type[] paramTypes = TypeParameterResolver.resolveParamTypes(method, type);
        setTypes.put(name, typeToClass(paramTypes[0]));
    }

    // 初始化 getMethods + getTypes 和 setMethods + setTypes
    // 因为有些field不存在对应的set/get方法
    private void addFields(Class<?> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (!setMethods.containsKey(field.getName())) {
                // issue #379 - removed the check for final because JDK 1.5 allows
                // modification of final fields through reflection (JSR-133). (JGB)
                // pr #16 - final static can only be set by the classloader
                int modifiers = field.getModifiers();
                if (!(Modifier.isFinal(modifiers) && Modifier.isStatic(modifiers))) {
                    addSetField(field);
                }
            }
            if (!getMethods.containsKey(field.getName())) {
                addGetField(field);
            }
        }
        if (clazz.getSuperclass() != null) {
            addFields(clazz.getSuperclass());
        }
    }

    // 设置字段
    private void addSetField(Field field) {
        // 先判断名字是否合法
        if (isValidPropertyName(field.getName())) {
            setMethods.put(field.getName(), new SetFieldInvoker(field));
            Type fieldType = TypeParameterResolver.resolveFieldType(field, type);
            setTypes.put(field.getName(), typeToClass(fieldType));
        }
    }


    // 获得方法签名
    private String getSignature(Method method) {
        // StringBuilder对象
        StringBuilder sb = new StringBuilder();
        Class<?> returnType = method.getReturnType();
        if (returnType != null) {
            sb.append(returnType.getName()).append('#');
        }
        // 添加方法名
        sb.append(method.getName());
        // 获取方法的所有的形参列表然后拼接字符串
        // 格式：returnType#方法名:参数名1,参数名2,参数名3 。
        // void#checkPackageAccess:java.lang.ClassLoader,boolean 。
        Class<?>[] parameters = method.getParameterTypes();
        for (int i = 0; i < parameters.length; i++) {
            sb.append(i == 0 ? ':' : ',').append(parameters[i].getName());
        }
        return sb.toString();
    }

    // 判断是否可以需改可访问性
    public static boolean canControlMemberAccessible() {
        try {
            SecurityManager securityManager = System.getSecurityManager();
            if (null != securityManager) {
                securityManager.checkPermission(new ReflectPermission("suppressAccessChecks"));
            }
        } catch (SecurityException e) {
            return false;
        }
        return true;
    }

    // 判断是不是合理的属性值
    private boolean isValidPropertyName(String name) {
        return !(name.startsWith("$") || "serialVersionUID".equals(name) || "class".equals(name));
    }

    // 寻找Type真正的类
    private Class<?> typeToClass(Type src) {
        Class<?> result = null;
        // 普通类，直接使用
        if (src instanceof Class) {
            result = (Class<?>) src;
            // 泛型类型，使用泛型
        } else if (src instanceof ParameterizedType) {
            result = (Class<?>) ((ParameterizedType) src).getRawType();
            // 泛型数组，获得具体类
        } else if (src instanceof GenericArrayType) {
            Type componentType = ((GenericArrayType) src).getGenericComponentType();
            // 普通类型
            if (componentType instanceof Class) {
                result = Array.newInstance((Class<?>) componentType, 0).getClass();
            } else {
                // 递归
                Class<?> componentClass = typeToClass(componentType);
                result = Array.newInstance(componentClass, 0).getClass();
            }
        }
        // 都不符合使用Object类
        if (result == null) {
            result = Object.class;
        }
        return result;
    }
    
    // 其他方法大致一样 省略 ... ...
}
```

#### ReflectorFactory

-  `org.apache.ibatis.reflection.ReflectorFactory` ，Reflector 工厂接口，用于创建和缓存 Reflector 对象。代码如下： 

```java
// ReflectorFactory.java

public interface ReflectorFactory {

    // 是否缓存 Reflector对象
    boolean isClassCacheEnabled();

    // 设置是否缓存 Reflector 对象
    void setClassCacheEnabled(boolean classCacheEnabled);

    // 获取 Reflector对象
    Reflector findForClass(Class<?> type);
}
```

##### DefaultReflectorFactory

-  `org.apache.ibatis.reflection.DefaultReflectorFactory` ，实现 ReflectorFactory 接口，默认的 ReflectorFactory 实现类。代码如下： 

```java
// DefaultReflectorFactory.java

public class DefaultReflectorFactory implements ReflectorFactory {
    // 是否缓存
    private boolean classCacheEnabled = true;
    // 缓存映射
    // key： 类
    // value：Reflector对象
    private final ConcurrentMap<Class<?>, Reflector> reflectorMap = new ConcurrentHashMap<>();

    public DefaultReflectorFactory() {
    }

    @Override
    public boolean isClassCacheEnabled() {
        return classCacheEnabled;
    }

    @Override
    public void setClassCacheEnabled(boolean classCacheEnabled) {
        this.classCacheEnabled = classCacheEnabled;
    }

    @Override
    public Reflector findForClass(Class<?> type) {
        // 开启缓存 从 reflectorMap中获取
        if (classCacheEnabled) {
            // synchronized (type) removed see issue #461
            // 不存在就创建
            return reflectorMap.computeIfAbsent(type, Reflector::new);
        } else {
            // 关闭缓存就创建 Reflector对象 
            return new Reflector(type);
        }
    }

}

```

#### Invoker

-  `org.apache.ibatis.reflection.invoker.Invoker` ，调用者接口。代码如下： 

```java
public interface Invoker {
    // 执行调用
    // target 目标
    // args 参数
    // return 结果
    Object invoke(Object target, Object[] args) throws IllegalAccessException, InvocationTargetException;

    // 返回类型
    Class<?> getType();
}

```

-  核心是 `#invoke(Object target, Object[] args)` 方法，执行一次调用。而具体调用什么方法，由子类来实现。 

##### GetFieldInvoker

-  `org.apache.ibatis.reflection.invoker.GetFieldInvoker` ，实现 Invoker 接口，获得 Field 调用者。代码如下： 

```java
public class GetFieldInvoker implements Invoker {
    // Field 对象
    private final Field field;

    public GetFieldInvoker(Field field) {
        this.field = field;
    }

    // 获得属性
    @Override
    public Object invoke(Object target, Object[] args) throws IllegalAccessException {
        try {
            return field.get(target);
        } catch (IllegalAccessException e) {
            if (Reflector.canControlMemberAccessible()) {
                field.setAccessible(true);
                return field.get(target);
            } else {
                throw e;
            }
        }
    }

    // 返回属性类型
    @Override
    public Class<?> getType() {
        return field.getType();
    }
}

```

##### SetFieldInvoker

-  `org.apache.ibatis.reflection.invoker.SetFieldInvoker` ，实现 Invoker 接口，设置 Field 调用者。代码如下： 

```java
public class SetFieldInvoker implements Invoker {
    // Field 对象
    private final Field field;

    public SetFieldInvoker(Field field) {
        this.field = field;
    }

    // 设置Field属性
    @Override
    public Object invoke(Object target, Object[] args) throws IllegalAccessException {
        try {
            field.set(target, args[0]);
        } catch (IllegalAccessException e) {
            if (Reflector.canControlMemberAccessible()) {
                field.setAccessible(true);
                field.set(target, args[0]);
            } else {
                throw e;
            }
        }
        return null;
    }

    // 返回属性类型
    @Override
    public Class<?> getType() {
        return field.getType();
    }
}

```

##### MethodInvoker

-  `org.apache.ibatis.reflection.invoker.MethodInvoker` ，实现 Invoker 接口，指定方法的调用器。代码如下： 

```java
public class MethodInvoker implements Invoker {

    // 类型
    private final Class<?> type;
    // 指定方法
    private final Method method;

    public MethodInvoker(Method method) {
        this.method = method;

        // 当参数大小为1的时候，一般是set方法，设置type为方法参数0
        if (method.getParameterTypes().length == 1) {
            type = method.getParameterTypes()[0];
            // 否则就是get方法，设置type为返回值类型
        } else {
            type = method.getReturnType();
        }
    }

    // 指定执行方法
    @Override
    public Object invoke(Object target, Object[] args) throws IllegalAccessException, InvocationTargetException {
        try {
            return method.invoke(target, args);
        } catch (IllegalAccessException e) {
            if (Reflector.canControlMemberAccessible()) {
                method.setAccessible(true);
                return method.invoke(target, args);
            } else {
                throw e;
            }
        }
    }

    @Override
    public Class<?> getType() {
        return type;
    }
}

```

#### ObjectFactory

- `org.apache.ibatis.reflection.factory.ObjectFactory` ，Object 工厂接口，用于创建指定类的对象。代码如下：

```java
public interface ObjectFactory {

    // 设置 Properties 的配置
    default void setProperties(Properties properties) {
        // NOP
    }


    // Creates a new object with default constructor. 
    // 指定创建类的对象，默认是无参构造方法
    // 返回创建的对象
    <T> T create(Class<T> type);

    // 创建指定类的对象，使用特殊的构造方法
    // constructorArgTypes 构造方法参数列表
    // 参数数组
    <T> T create(Class<T> type, List<Class<?>> constructorArgTypes, List<Object> constructorArgs);

    // 判断指定类是否为集合类
    <T> boolean isCollection(Class<T> type);

}

```

##### DefaultObjectFactory

-  `org.apache.ibatis.reflection.factory.DefaultObjectFactory` ，实现 ObjectFactory、Serializable 接口，默认 ObjectFactory 实现类。 

```java
public class DefaultObjectFactory implements ObjectFactory, Serializable {

    private static final long serialVersionUID = -8855120656740914948L;

    // 创建指定的对象
    @Override
    public <T> T create(Class<T> type) {
        // 调用create有参构造函数，无参构造方法创建对象
        return create(type, null, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T create(Class<T> type, List<Class<?>> constructorArgTypes, List<Object> constructorArgs) {
        // 获得需要创建的类
        Class<?> classToCreate = resolveInterface(type);
        // we know types are assignable
        // 创建指定的类
        return (T) instantiateClass(classToCreate, constructorArgTypes, constructorArgs);
    }

    // 创建对象
    private  <T> T instantiateClass(Class<T> type, List<Class<?>> constructorArgTypes, List<Object> constructorArgs) {
        try {
            Constructor<T> constructor;
            // 如果参数列表为null或者参数值为null
            if (constructorArgTypes == null || constructorArgs == null) {
                // 就创建无参构造函数
                constructor = type.getDeclaredConstructor();
                try {
                    // 返回创建的无参 构造函数
                    return constructor.newInstance();
                } catch (IllegalAccessException e) {
                    if (Reflector.canControlMemberAccessible()) {
                        constructor.setAccessible(true);
                        return constructor.newInstance();
                    } else {
                        throw e;
                    }
                }
            }
            // 使用特定的构造方法，创建指定类的对象
            constructor = type.getDeclaredConstructor(constructorArgTypes.toArray(new Class[0]));
            try {
                return constructor.newInstance(constructorArgs.toArray(new Object[0]));
            } catch (IllegalAccessException e) {
                if (Reflector.canControlMemberAccessible()) {
                    constructor.setAccessible(true);
                    return constructor.newInstance(constructorArgs.toArray(new Object[0]));
                } else {
                    throw e;
                }
            }
        } catch (Exception e) {
            // 拼接 argTypes
            String argTypes = Optional.ofNullable(constructorArgTypes).orElseGet(Collections::emptyList)
                .stream().map(Class::getSimpleName).collect(Collectors.joining(","));
            // 拼接 argValues
            String argValues = Optional.ofNullable(constructorArgs).orElseGet(Collections::emptyList)
                .stream().map(String::valueOf).collect(Collectors.joining(","));
            throw new ReflectionException("Error instantiating " + type + " with invalid types (" + argTypes + ") or values (" + argValues + "). Cause: " + e, e);
        }
    }

    // 创建需要的类
    protected Class<?> resolveInterface(Class<?> type) {
        Class<?> classToCreate;
        // 对参数的类型进行判断
        if (type == List.class || type == Collection.class || type == Iterable.class) {
            classToCreate = ArrayList.class;
        } else if (type == Map.class) {
            classToCreate = HashMap.class;
        } else if (type == SortedSet.class) { // issue #510 Collections Support
            classToCreate = TreeSet.class;
        } else if (type == Set.class) {
            classToCreate = HashSet.class;
        } else {
            classToCreate = type;
        }
        return classToCreate;
    }

    // 判断是不是集合 `java.util.Collection`的子类
    @Override
    public <T> boolean isCollection(Class<T> type) {
        return Collection.class.isAssignableFrom(type);
    }

}

```

#### Property工具类

-  `org.apache.ibatis.reflection.property` 包下，提供了 PropertyCopier、PropertyNamer、PropertyTokenizer 三个属性相关的工具类。

##### PropertyCopier

-  `org.apache.ibatis.reflection.property.PropertyCopier` ，属性复制器。代码如下： 

```java
public final class PropertyCopier {

    private PropertyCopier() {
        // Prevent Instantiation of Static Class
    }

    // 将 sourceBean 的属性，复制到 destinationBean 中
    public static void copyBeanProperties(Class<?> type, Object sourceBean, Object destinationBean) {
        // 循环，从当前类开始，一直复制到父类，直到父类不存在
        Class<?> parent = type;
        while (parent != null) {
            // 获取当前 parent 类定义的属性
            final Field[] fields = parent.getDeclaredFields();
            for (Field field : fields) {
                try {
                    try {
                        // 执行复制
                        field.set(destinationBean, field.get(sourceBean));
                    } catch (IllegalAccessException e) {
                        if (Reflector.canControlMemberAccessible()) {
                            field.setAccessible(true);
                            field.set(destinationBean, field.get(sourceBean));
                        } else {
                            throw e;
                        }
                    }
                } catch (Exception e) {
                    // Nothing useful to do, will only fail on final fields, which will be ignored.
                }
            }
            // 获得父类
            parent = parent.getSuperclass();
        }
    }

}

```

##### PropertyNamer

-  `org.apache.ibatis.reflection.property.PropertyNamer` ，属性名相关的工具类方法。代码如下： 

```java
public final class PropertyNamer {

    private PropertyNamer() {
        // Prevent Instantiation of Static Class
    }

    // 根据方法名，获得对应的属性名
    // name：方法名
    // return：属性名
    public static String methodToProperty(String name) {
        // 判断是不是is方法
        if (name.startsWith("is")) {
            name = name.substring(2);
            // get或者set方法
        } else if (name.startsWith("get") || name.startsWith("set")) {
            name = name.substring(3);
        } else {
            throw new ReflectionException("Error parsing property name '" + name + "'.  Didn't start with 'is', 'get' or 'set'.");
        }

        // 设置首字母小写
        if (name.length() == 1 || (name.length() > 1 && !Character.isUpperCase(name.charAt(1)))) {
            name = name.substring(0, 1).toLowerCase(Locale.ENGLISH) + name.substring(1);
        }

        // 返回名字
        return name;
    }

    // 判断是不是 is、get、set 方法
    public static boolean isProperty(String name) {
        return isGetter(name) || isSetter(name);
    }

    // 判断是不是get方法
    public static boolean isGetter(String name) {
        return (name.startsWith("get") && name.length() > 3) || (name.startsWith("is") && name.length() > 2);
    }

    // 判断是不是set方法
    public static boolean isSetter(String name) {
        return name.startsWith("set") && name.length() > 3;
    }

}

```

##### PropertyTokenizer

- `org.apache.ibatis.reflection.property.PropertyTokenizer` ，实现 Iterator 接口，属性分词器，支持迭代器的访问方式。
- 举个例子，在访问 `"order[0].item[0].name"` 时，我们希望拆分成 `"order[0]"`、`"item[0]"`、`"name"` 三段，那么就可以通过 PropertyTokenizer 来实现。

```java
public class PropertyTokenizer implements Iterator<PropertyTokenizer> {
    // 当前字符串
    private String name;
    // 索引的名字
    private final String indexedName;
    // 编号
    // 对于数组 name[0] 则 index = 0
    // 对于 Map map[key,value] 则 index = key
    private String index;
    // 剩余字符串
    private final String children;

    // 构造函数
    public PropertyTokenizer(String fullname) {
        // 初始化name、children字符串，使用 '.' 作为分隔
        int delim = fullname.indexOf('.');
        if (delim > -1) {
            name = fullname.substring(0, delim);
            children = fullname.substring(delim + 1);
        } else {
            name = fullname;
            children = null;
        }
        // 记录当前 name
        indexedName = name;
        // 如果存在 [，则获得 index ，并修改 name
        delim = name.indexOf('[');
        if (delim > -1) {
            index = name.substring(delim + 1, name.length() - 1);
            name = name.substring(0, delim);
        }
    }

    public String getName() {
        return name;
    }

    public String getIndex() {
        return index;
    }

    public String getIndexedName() {
        return indexedName;
    }

    public String getChildren() {
        return children;
    }

    // 判断是否还有下一个元素
    @Override
    public boolean hasNext() {
        return children != null;
    }

    // next 方法，得带获得下一个 PropertyTokenizer 对象
    @Override
    public PropertyTokenizer next() {
        // 然后又会继续执行构造方法的流程
        return new PropertyTokenizer(children);
    }

    // 不可以 remove ，调用就抛出异常
    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove is not supported, as it has no meaning in the context of properties.");
    }
}

```

#### MetaClass

- `org.apache.ibatis.reflection.MetaClass` ，类的元数据，基于 Reflector 和 PropertyTokenizer ，提供对指定类的各种骚操作。

```java
public class MetaClass {

    private final ReflectorFactory reflectorFactory;
    private final Reflector reflector;

    // 构造函数 ，一个MetaClass对象，对应一个Class对象
    private MetaClass(Class<?> type, ReflectorFactory reflectorFactory) {
        this.reflectorFactory = reflectorFactory;
        this.reflector = reflectorFactory.findForClass(type);
    }

    // 静态方法，创建指定类的 MetaClass 对象
    public static MetaClass forClass(Class<?> type, ReflectorFactory reflectorFactory) {
        return new MetaClass(type, reflectorFactory);
    }

    // 创建类指定属性的 MetaClass 对象
    public MetaClass metaClassForProperty(String name) {
        Class<?> propType = reflector.getGetterType(name);
        return MetaClass.forClass(propType, reflectorFactory);
    }

    // 获得属性
    public String findProperty(String name) {
        StringBuilder prop = buildProperty(name, new StringBuilder());
        return prop.length() > 0 ? prop.toString() : null;
    }

    // 根据表达，获取属性
    public String findProperty(String name, boolean useCamelCaseMapping) {
        // 是否使用下划线驼峰
        if (useCamelCaseMapping) {
            name = name.replace("_", "");
        }
        return findProperty(name);
    }

    // 获得get方法的名字数组
    public String[] getGetterNames() {
        return reflector.getGetablePropertyNames();
    }

    // 获得set方法的名字数组
    public String[] getSetterNames() {
        return reflector.getSetablePropertyNames();
    }

    // 根据名字获取set方法的Class对象
    public Class<?> getSetterType(String name) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            MetaClass metaProp = metaClassForProperty(prop.getName());
            return metaProp.getSetterType(prop.getChildren());
        } else {
            return reflector.getSetterType(prop.getName());
        }
    }

    // 根据名字获取get方法对应的对象
    // 获得指定属性的 getting 方法的返回值的类型
    public Class<?> getGetterType(String name) {
        // 创建分词器对象，对name进行分词
        PropertyTokenizer prop = new PropertyTokenizer(name);
        // 有子表达式
        if (prop.hasNext()) {
            // 创建MetaClass对象
            MetaClass metaProp = metaClassForProperty(prop);
            // 递归判断子表达式 children 获得返回值类型
            return metaProp.getGetterType(prop.getChildren());
        }
        // issue #506. Resolve the type inside a Collection Object
        // 直接获得返回值类型
        return getGetterType(prop);
    }

    // 通过 PropertyTokenizer 获取MetaClass对象
    private MetaClass metaClassForProperty(PropertyTokenizer prop) {
        // 获得 get方法返回的类型
        Class<?> propType = getGetterType(prop);
        // 创建 MetaClass对象
        return MetaClass.forClass(propType, reflectorFactory);
    }

    private Class<?> getGetterType(PropertyTokenizer prop) {
        // 获得返回类型
        Class<?> type = reflector.getGetterType(prop.getName());
        // 如果获取数组的某个位置的元素，则获取其泛型。例如说：list[0].field 
        // 那么就会解析filed是什么类型，然后再进行判断
        if (prop.getIndex() != null && Collection.class.isAssignableFrom(type)) {
            // 获得返回类型
            Type returnType = getGenericGetterType(prop.getName());
            // 如果是泛型，就进行解析真正的类型
            if (returnType instanceof ParameterizedType) {
                Type[] actualTypeArguments = ((ParameterizedType) returnType).getActualTypeArguments();
                if (actualTypeArguments != null && actualTypeArguments.length == 1) {
                    returnType = actualTypeArguments[0];
                    if (returnType instanceof Class) {
                        type = (Class<?>) returnType;
                    } else if (returnType instanceof ParameterizedType) {
                        type = (Class<?>) ((ParameterizedType) returnType).getRawType();
                    }
                }
            }
        }
        return type;
    }

    private Type getGenericGetterType(String propertyName) {
        // 调用流程：metaClassForProperty => getGetterType => getGenericGetterType 。
        try {
            // 获得Invoker对象
            Invoker invoker = reflector.getGetInvoker(propertyName);
            // 如果是 MethodInvoker 对象，则说明是 get方法，解析方法返回类型
            if (invoker instanceof MethodInvoker) {
                Field declaredMethod = MethodInvoker.class.getDeclaredField("method");
                declaredMethod.setAccessible(true);
                Method method = (Method) declaredMethod.get(invoker);
                return TypeParameterResolver.resolveReturnType(method, reflector.getType());
                // 如果是 GetFieldInvoker 对象，说明是field ，直接访问
            } else if (invoker instanceof GetFieldInvoker) {
                Field declaredField = GetFieldInvoker.class.getDeclaredField("field");
                declaredField.setAccessible(true);
                Field field = (Field) declaredField.get(invoker);
                return TypeParameterResolver.resolveFieldType(field, reflector.getType());
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            // Ignored
        }
        return null;
    }

    // 判断是否有set方法 逻辑和下面的get方法一致
    public boolean hasSetter(String name) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            if (reflector.hasSetter(prop.getName())) {
                MetaClass metaProp = metaClassForProperty(prop.getName());
                return metaProp.hasSetter(prop.getChildren());
            } else {
                return false;
            }
        } else {
            return reflector.hasSetter(prop.getName());
        }
    }

    // 判断属性是否有get方法
    public boolean hasGetter(String name) {
        // 创建分词器
        PropertyTokenizer prop = new PropertyTokenizer(name);
        // 有子表达式
        if (prop.hasNext()) {
            // 判断是否有该属性的 get方法
            if (reflector.hasGetter(prop.getName())) {
                // 创建 MetaClass对象
                MetaClass metaProp = metaClassForProperty(prop);
                // 递归判断
                return metaProp.hasGetter(prop.getChildren());
            } else {
                return false;
            }
            // 没有子表达式
        } else {
            // 是否有get方法
            return reflector.hasGetter(prop.getName());
        }
    }

    public Invoker getGetInvoker(String name) {
        return reflector.getGetInvoker(name);
    }

    public Invoker getSetInvoker(String name) {
        return reflector.getSetInvoker(name);
    }

    private StringBuilder buildProperty(String name, StringBuilder builder) {
        // 获取 PropertyToenizer 对象，对name进行分词
        PropertyTokenizer prop = new PropertyTokenizer(name);
        // 有子表达式
        if (prop.hasNext()) {
            // 获得属性名，并添加到 builder 中
            String propertyName = reflector.findPropertyName(prop.getName());
            if (propertyName != null) {
                // 拼接属性到 builder 中
                builder.append(propertyName);
                builder.append(".");
                // 创建 MetaClass对象
                MetaClass metaProp = metaClassForProperty(propertyName);
                // 递归解析子表达式 childre ，并将结果添加到 builder中
                metaProp.buildProperty(prop.getChildren(), builder);
            }
            // 无子表达式
        } else {
            // 获得属性名，并添加到 builder中
            String propertyName = reflector.findPropertyName(name);
            if (propertyName != null) {
                builder.append(propertyName);
            }
        }
        return builder;
    }

    public boolean hasDefaultConstructor() {
        return reflector.hasDefaultConstructor();
    }

}

```

#### ObjectWrapper

-  `org.apache.ibatis.reflection.wrapper.ObjectWrapper` ，对象包装器接口，基于 MetaClass 工具类，定义对指定对象的各种操作。或者可以说，ObjectWrapper 是 MetaClass 的指定类的具象化。代码如下： 

```java
public interface ObjectWrapper {

    // 获得值
    // prop 相当于是键
    Object get(PropertyTokenizer prop);

    // 设置值，porp 相当于是键
    // value 相当于是值
    void set(PropertyTokenizer prop, Object value);

    // 下面这几个部分的方法和上面的很类似
    String findProperty(String name, boolean useCamelCaseMapping);

    String[] getGetterNames();

    String[] getSetterNames();

    Class<?> getSetterType(String name);

    Class<?> getGetterType(String name);

    boolean hasSetter(String name);

    boolean hasGetter(String name);

    MetaObject instantiatePropertyValue(String name, PropertyTokenizer prop, ObjectFactory objectFactory);

    // 判断是不是集合
    boolean isCollection();

    // 添加元素
    void add(Object element);

    // 添加多个元素
    <E> void addAll(List<E> element);

}

```

##### BaseWrapper

-  `org.apache.ibatis.reflection.wrapper.BaseWrapper` ，实现 ObjectWrapper 接口，ObjectWrapper 抽象类，为子类 BeanWrapper 和 MapWrapper 提供属性值的获取和设置的公用方法。代码如下 

```java
public abstract class BaseWrapper implements ObjectWrapper {

    protected static final Object[] NO_ARGUMENTS = new Object[0];
    // Object对象
    protected final MetaObject metaObject;

    protected BaseWrapper(MetaObject metaObject) {
        this.metaObject = metaObject;
    }

    // 获得属性的值，
    // porp 
    // object指定Object对象
    protected Object resolveCollection(PropertyTokenizer prop, Object object) {
        if ("".equals(prop.getName())) {
            return object;
        } else {
            return metaObject.getValue(prop.getName());
        }
    }

    // 获取集合中指的位置的值
    // collection 集合
    protected Object getCollectionValue(PropertyTokenizer prop, Object collection) {
        if (collection instanceof Map) {
            return ((Map) collection).get(prop.getIndex());
        } else {
            int i = Integer.parseInt(prop.getIndex());
            if (collection instanceof List) {
                return ((List) collection).get(i);
            } else if (collection instanceof Object[]) {
                return ((Object[]) collection)[i];
            } else if (collection instanceof char[]) {
                return ((char[]) collection)[i];
            } else if (collection instanceof boolean[]) {
                return ((boolean[]) collection)[i];
            } else if (collection instanceof byte[]) {
                return ((byte[]) collection)[i];
            } else if (collection instanceof double[]) {
                return ((double[]) collection)[i];
            } else if (collection instanceof float[]) {
                return ((float[]) collection)[i];
            } else if (collection instanceof int[]) {
                return ((int[]) collection)[i];
            } else if (collection instanceof long[]) {
                return ((long[]) collection)[i];
            } else if (collection instanceof short[]) {
                return ((short[]) collection)[i];
            } else {
                throw new ReflectionException("The '" + prop.getName() + "' property of " + collection + " is not a List or Array.");
            }
        }
    }

    // 设置集合中指定位置的值
    protected void setCollectionValue(PropertyTokenizer prop, Object collection, Object value) {
        if (collection instanceof Map) {
            ((Map) collection).put(prop.getIndex(), value);
        } else {
            int i = Integer.parseInt(prop.getIndex());
            if (collection instanceof List) {
                ((List) collection).set(i, value);
            } else if (collection instanceof Object[]) {
                ((Object[]) collection)[i] = value;
            } else if (collection instanceof char[]) {
                ((char[]) collection)[i] = (Character) value;
            } else if (collection instanceof boolean[]) {
                ((boolean[]) collection)[i] = (Boolean) value;
            } else if (collection instanceof byte[]) {
                ((byte[]) collection)[i] = (Byte) value;
            } else if (collection instanceof double[]) {
                ((double[]) collection)[i] = (Double) value;
            } else if (collection instanceof float[]) {
                ((float[]) collection)[i] = (Float) value;
            } else if (collection instanceof int[]) {
                ((int[]) collection)[i] = (Integer) value;
            } else if (collection instanceof long[]) {
                ((long[]) collection)[i] = (Long) value;
            } else if (collection instanceof short[]) {
                ((short[]) collection)[i] = (Short) value;
            } else {
                throw new ReflectionException("The '" + prop.getName() + "' property of " + collection + " is not a List or Array.");
            }
        }
    }

}

```

##### BeanWrapper

-  `org.apache.ibatis.reflection.wrapper.BeanWrapper` ，继承 BaseWrapper 抽象类，**普通对象**的 ObjectWrapper 实现类，例如 User、Order 这样的 POJO 类 

```java
public class BeanWrapper extends BaseWrapper {

    // 普通对象
    private final Object object;
    private final MetaClass metaClass;

    public BeanWrapper(MetaObject metaObject, Object object) {
        super(metaObject);
        this.object = object;
        // 创建 MetaClass对象
        this.metaClass = MetaClass.forClass(object.getClass(), metaObject.getReflectorFactory());
    }

    // 获得指定属性的值
    @Override
    public Object get(PropertyTokenizer prop) {
        // 获取集合指定位置的值
        if (prop.getIndex() != null) {
            // 获取集合类型的属性
            Object collection = resolveCollection(prop, object);
            // 返回指定位置的值
            return getCollectionValue(prop, collection);
        } else {
            // 返回指定位置的值
            return getBeanProperty(prop, object);
        }
    }

    // 设置属性的值
    @Override
    public void set(PropertyTokenizer prop, Object value) {
        // 设置指定位置
        if (prop.getIndex() != null) {
            // 获取集合
            Object collection = resolveCollection(prop, object);
            // 设置值
            setCollectionValue(prop, collection, value);
        } else {
            // 设置值
            setBeanProperty(prop, object, value);
        }
    }

    @Override
    public String findProperty(String name, boolean useCamelCaseMapping) {
        return metaClass.findProperty(name, useCamelCaseMapping);
    }

    @Override
    public String[] getGetterNames() {
        return metaClass.getGetterNames();
    }

    @Override
    public String[] getSetterNames() {
        return metaClass.getSetterNames();
    }

    @Override
    public Class<?> getSetterType(String name) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            MetaObject metaValue = metaObject.metaObjectForProperty(prop.getIndexedName());
            if (metaValue == SystemMetaObject.NULL_META_OBJECT) {
                return metaClass.getSetterType(name);
            } else {
                return metaValue.getSetterType(prop.getChildren());
            }
        } else {
            return metaClass.getSetterType(name);
        }
    }

    @Override
    public Class<?> getGetterType(String name) {
        // 创建分词对象
        PropertyTokenizer prop = new PropertyTokenizer(name);
        有子表达式
        if (prop.hasNext()) {
            // 创建 MetaObject对象
            MetaObject metaValue = metaObject.metaObjectForProperty(prop.getIndexedName());
            // 如果为空，则基于MetaClass获得返回类型
            if (metaValue == SystemMetaObject.NULL_META_OBJECT) {
                return metaClass.getGetterType(name);
               // 如果非空，则基于metaValue获得类型
            } else {
                // 递归判断
                return metaValue.getGetterType(prop.getChildren());
            }
        } else {
            // 无子表达式，直接返回
            return metaClass.getGetterType(name);
        }
    }

    @Override
    public boolean hasSetter(String name) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            if (metaClass.hasSetter(prop.getIndexedName())) {
                MetaObject metaValue = metaObject.metaObjectForProperty(prop.getIndexedName());
                if (metaValue == SystemMetaObject.NULL_META_OBJECT) {
                    return metaClass.hasSetter(name);
                } else {
                    return metaValue.hasSetter(prop.getChildren());
                }
            } else {
                return false;
            }
        } else {
            return metaClass.hasSetter(name);
        }
    }

    // 是否有指定属性的get方法
    @Override
    public boolean hasGetter(String name) {
        // 创建分词器
        PropertyTokenizer prop = new PropertyTokenizer(name);
        // 有子表达式
        if (prop.hasNext()) {
            if (metaClass.hasGetter(prop.getIndexedName())) {
                MetaObject metaValue = metaObject.metaObjectForProperty(prop.getIndexedName());
                if (metaValue == SystemMetaObject.NULL_META_OBJECT) {
                    return metaClass.hasGetter(name);
                } else {
                    return metaValue.hasGetter(prop.getChildren());
                }
            } else {
                return false;
            }
        } else {
            // 无子表达式，直接返回
            return metaClass.hasGetter(name);
        }
    }

    // 创建指定属性的值
    @Override
    public MetaObject instantiatePropertyValue(String name, PropertyTokenizer prop, ObjectFactory objectFactory) {
        MetaObject metaValue;
        // 获得set方法的方法参数类型
        Class<?> type = getSetterType(prop.getName());
        try {
            // 创建对象
            Object newObject = objectFactory.create(type);
            // 创建MetaObject对象
            metaValue = MetaObject.forObject(newObject, metaObject.getObjectFactory(), metaObject.getObjectWrapperFactory(), metaObject.getReflectorFactory());
            // 设置当前对象的值             
            set(prop, newObject);
        } catch (Exception e) {
            throw new ReflectionException("Cannot set value of property '" + name + "' because '" + name + "' is null and cannot be instantiated on instance of " + type.getName() + ". Cause:" + e.toString(), e);
        }
        return metaValue;
    }

    // 获取属性的值
    // 通过 调用 Invoker方法，获得属性的值
    private Object getBeanProperty(PropertyTokenizer prop, Object object) {
        try {
            Invoker method = metaClass.getGetInvoker(prop.getName());
            try {
                return method.invoke(object, NO_ARGUMENTS);
            } catch (Throwable t) {
                throw ExceptionUtil.unwrapThrowable(t);
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable t) {
            throw new ReflectionException("Could not get property '" + prop.getName() + "' from " + object.getClass() + ".  Cause: " + t.toString(), t);
        }
    }

    // 设置值
    private void setBeanProperty(PropertyTokenizer prop, Object object, Object value) {
        try {
            Invoker method = metaClass.getSetInvoker(prop.getName());
            Object[] params = {value};
            try {
                method.invoke(object, params);
            } catch (Throwable t) {
                throw ExceptionUtil.unwrapThrowable(t);
            }
        } catch (Throwable t) {
            throw new ReflectionException("Could not set property '" + prop.getName() + "' of '" + object.getClass() + "' with value '" + value + "' Cause: " + t.toString(), t);
        }
    }

    // 不是集合
    @Override
    public boolean isCollection() {
        return false;
    }

    // 不支持add和addAll方法
    @Override
    public void add(Object element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <E> void addAll(List<E> list) {
        throw new UnsupportedOperationException();
    }

}

```

##### MapWrapper

- `org.apache.ibatis.reflection.wrapper.MapWrapper` ，继承 BaseWrapper 抽象类，**Map** 对象的 ObjectWrapper 实现类。
- MapWrapper 和 BeanWrapper 的大体逻辑是一样的，差异点主要如下：

  - // MapWrapper.java
  - // object 变成了 map
  - private final Map<String, Object> map;

  - // 属性的操作变成了
  - map.put(prop.getName(), value);
  - map.get(prop.getName());

```java
public class MapWrapper extends BaseWrapper {

    private final Map<String, Object> map;

    public MapWrapper(MetaObject metaObject, Map<String, Object> map) {
        super(metaObject);
        this.map = map;
    }

    @Override
    public Object get(PropertyTokenizer prop) {
        if (prop.getIndex() != null) {
            Object collection = resolveCollection(prop, map);
            return getCollectionValue(prop, collection);
        } else {
            return map.get(prop.getName());
        }
    }

    @Override
    public void set(PropertyTokenizer prop, Object value) {
        if (prop.getIndex() != null) {
            Object collection = resolveCollection(prop, map);
            setCollectionValue(prop, collection, value);
        } else {
            map.put(prop.getName(), value);
        }
    }

    @Override
    public String findProperty(String name, boolean useCamelCaseMapping) {
        return name;
    }

    @Override
    public String[] getGetterNames() {
        return map.keySet().toArray(new String[map.keySet().size()]);
    }

    @Override
    public String[] getSetterNames() {
        return map.keySet().toArray(new String[map.keySet().size()]);
    }

    @Override
    public Class<?> getSetterType(String name) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            MetaObject metaValue = metaObject.metaObjectForProperty(prop.getIndexedName());
            if (metaValue == SystemMetaObject.NULL_META_OBJECT) {
                return Object.class;
            } else {
                return metaValue.getSetterType(prop.getChildren());
            }
        } else {
            if (map.get(name) != null) {
                return map.get(name).getClass();
            } else {
                return Object.class;
            }
        }
    }

    @Override
    public Class<?> getGetterType(String name) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            MetaObject metaValue = metaObject.metaObjectForProperty(prop.getIndexedName());
            if (metaValue == SystemMetaObject.NULL_META_OBJECT) {
                return Object.class;
            } else {
                return metaValue.getGetterType(prop.getChildren());
            }
        } else {
            if (map.get(name) != null) {
                return map.get(name).getClass();
            } else {
                return Object.class;
            }
        }
    }

    @Override
    public boolean hasSetter(String name) {
        return true;
    }

    @Override
    public boolean hasGetter(String name) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            if (map.containsKey(prop.getIndexedName())) {
                MetaObject metaValue = metaObject.metaObjectForProperty(prop.getIndexedName());
                if (metaValue == SystemMetaObject.NULL_META_OBJECT) {
                    return true;
                } else {
                    return metaValue.hasGetter(prop.getChildren());
                }
            } else {
                return false;
            }
        } else {
            return map.containsKey(prop.getName());
        }
    }

    @Override
    public MetaObject instantiatePropertyValue(String name, PropertyTokenizer prop, ObjectFactory objectFactory) {
        HashMap<String, Object> map = new HashMap<>();
        set(prop, map);
        return MetaObject.forObject(map, metaObject.getObjectFactory(), metaObject.getObjectWrapperFactory(), metaObject.getReflectorFactory());
    }

    @Override
    public boolean isCollection() {
        return false;
    }

    @Override
    public void add(Object element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <E> void addAll(List<E> element) {
        throw new UnsupportedOperationException();
    }

}

```

##### CollectionWrapper

-  `org.apache.ibatis.reflection.wrapper.CollectionWrapper` ，实现 ObjectWrapper 接口，集合 ObjectWrapper 实现类。比较简单，直接看代码： 

```java
public class CollectionWrapper implements ObjectWrapper {

    private final Collection<Object> object;

    public CollectionWrapper(MetaObject metaObject, Collection<Object> object) {
        this.object = object;
    }

    @Override
    public Object get(PropertyTokenizer prop) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void set(PropertyTokenizer prop, Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String findProperty(String name, boolean useCamelCaseMapping) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String[] getGetterNames() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String[] getSetterNames() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Class<?> getSetterType(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Class<?> getGetterType(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasSetter(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasGetter(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public MetaObject instantiatePropertyValue(String name, PropertyTokenizer prop, ObjectFactory objectFactory) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isCollection() {
        return true;
    }

    // 仅仅支持 #add(Object element) 和 #addAll(List<E> element) 两个操作方法。
    @Override
    public void add(Object element) {
        object.add(element);
    }

    @Override
    public <E> void addAll(List<E> element) {
        object.addAll(element);
    }

}

```

#### ObjectWrapperFactory

-  `org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory` ，ObjectWrapper 工厂接口 

```java
public interface ObjectWrapperFactory {

    // 是否包装了指定对象
    // object  指定对象
    boolean hasWrapperFor(Object object);

    // 获取指定的 ObjectWrapper 对象
    // metaObject对象
    // 指定对象
    ObjectWrapper getWrapperFor(MetaObject metaObject, Object object);

}

```

##### DefaultObjectWrapperFactory

- `org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory` ，实现 ObjectWrapperFactory 接口，**默认** ObjectWrapperFactory 实现类 
- 空实现，一般不会使用

```java
public class DefaultObjectWrapperFactory implements ObjectWrapperFactory {

    @Override
    public boolean hasWrapperFor(Object object) {
        return false;
    }

    @Override
    public ObjectWrapper getWrapperFor(MetaObject metaObject, Object object) {
        throw new ReflectionException("The DefaultObjectWrapperFactory should never be called to provide an ObjectWrapper.");
    }

}

```

#### MetaObject

-  `org.apache.ibatis.reflection.MetaObject` ，对象元数据，提供了对象的属性值的获得和设置等等方法。可以理解成，对 BaseWrapper 操作的进一步**增强**。 

```java
public class MetaObject {

    // 封装过的 Object 对象
    private final Object originalObject;
    private final ObjectWrapper objectWrapper;
    private final ObjectFactory objectFactory;
    private final ObjectWrapperFactory objectWrapperFactory;
    private final ReflectorFactory reflectorFactory;

    private MetaObject(Object object, ObjectFactory objectFactory, ObjectWrapperFactory objectWrapperFactory, ReflectorFactory reflectorFactory) {
        this.originalObject = object;
        this.objectFactory = objectFactory;
        this.objectWrapperFactory = objectWrapperFactory;
        this.reflectorFactory = reflectorFactory;

        if (object instanceof ObjectWrapper) {
            this.objectWrapper = (ObjectWrapper) object;
        } else if (objectWrapperFactory.hasWrapperFor(object)) {
            // 创建 ObjectWrapper对象
            this.objectWrapper = objectWrapperFactory.getWrapperFor(this, object);
        } else if (object instanceof Map) {
            // 创建 MapWrapper对象
            this.objectWrapper = new MapWrapper(this, (Map) object);
        } else if (object instanceof Collection) {
            // 创建 CollectionWrapper对象
            this.objectWrapper = new CollectionWrapper(this, (Collection) object);
        } else {
            // 创建 BeanWrapper对象
            this.objectWrapper = new BeanWrapper(this, object);
        }
    }

    public static MetaObject forObject(Object object, ObjectFactory objectFactory, ObjectWrapperFactory objectWrapperFactory, ReflectorFactory reflectorFactory) {
        if (object == null) {
            return SystemMetaObject.NULL_META_OBJECT;
        } else {
            return new MetaObject(object, objectFactory, objectWrapperFactory, reflectorFactory);
        }
    }

    public ObjectFactory getObjectFactory() {
        return objectFactory;
    }

    public ObjectWrapperFactory getObjectWrapperFactory() {
        return objectWrapperFactory;
    }

    public ReflectorFactory getReflectorFactory() {
        return reflectorFactory;
    }

    public Object getOriginalObject() {
        return originalObject;
    }

    public String findProperty(String propName, boolean useCamelCaseMapping) {
        return objectWrapper.findProperty(propName, useCamelCaseMapping);
    }

    public String[] getGetterNames() {
        return objectWrapper.getGetterNames();
    }

    public String[] getSetterNames() {
        return objectWrapper.getSetterNames();
    }

    public Class<?> getSetterType(String name) {
        return objectWrapper.getSetterType(name);
    }

    public Class<?> getGetterType(String name) {
        return objectWrapper.getGetterType(name);
    }

    public boolean hasSetter(String name) {
        return objectWrapper.hasSetter(name);
    }

    public boolean hasGetter(String name) {
        return objectWrapper.hasGetter(name);
    }

    // 互殴去指定属性的值
    public Object getValue(String name) {
        // 分词
        PropertyTokenizer prop = new PropertyTokenizer(name);
        // 如果有子表达式
        if (prop.hasNext()) {
            MetaObject metaValue = metaObjectForProperty(prop.getIndexedName());
            if (metaValue == SystemMetaObject.NULL_META_OBJECT) {
                return null;
            } else {
                return metaValue.getValue(prop.getChildren());
            }
        } else {
            // 没有子表达式，直接创建返回
            return objectWrapper.get(prop);
        }
    }

    // 设置指定属性的值
    public void setValue(String name, Object value) {
        PropertyTokenizer prop = new PropertyTokenizer(name);
        if (prop.hasNext()) {
            MetaObject metaValue = metaObjectForProperty(prop.getIndexedName());
            if (metaValue == SystemMetaObject.NULL_META_OBJECT) {
                if (value == null) {
                    // don't instantiate child path if value is null
                    return;
                } else {
                    metaValue = objectWrapper.instantiatePropertyValue(name, prop, objectFactory);
                }
            }
            metaValue.setValue(prop.getChildren(), value);
        } else {
            objectWrapper.set(prop, value);
        }
    }

    // 创建指定属性的 MetaObject 对象
    public MetaObject metaObjectForProperty(String name) {
        // 获取属性值
        Object value = getValue(name);
        // 创建 MetaObject 对象
        return MetaObject.forObject(value, objectFactory, objectWrapperFactory, reflectorFactory);
    }

    public ObjectWrapper getObjectWrapper() {
        return objectWrapper;
    }

    // 判断是否为集合
    public boolean isCollection() {
        return objectWrapper.isCollection();
    }

    public void add(Object element) {
        objectWrapper.add(element);
    }

    public <E> void addAll(List<E> list) {
        objectWrapper.addAll(list);
    }

}

```

#### SystemMetaObject

-  `org.apache.ibatis.reflection.SystemMetaObject` ，系统级的 MetaObject 对象，主要提供了 ObjectFactory、ObjectWrapperFactory、空 MetaObject 的单例 

```java
public final class SystemMetaObject {

    // ObjectFactory的单例
    public static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();
    
    // ObjectWrapperFactory的单例
    public static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();
    
    // MetaObject的空对象单例
    public static final MetaObject NULL_META_OBJECT = MetaObject.forObject(NullObject.class, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY, new DefaultReflectorFactory());

    private SystemMetaObject() {
        // Prevent Instantiation of Static Class
    }

    private static class NullObject {
    }

    // 创建 MetaObject对象
    public static MetaObject forObject(Object object) {
        return MetaObject.forObject(object, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY, new DefaultReflectorFactory());
    }

}

```

#### ParamNameUtil

-  `org.apache.ibatis.reflection.ParamNameUtil` ，参数名工具类，获得构造方法、普通方法的参数列表 

```java
public class ParamNameUtil {
    // 获得普通方法的参数列表
    // method 普通方法
    public static List<String> getParamNames(Method method) {
        return getParameterNames(method);
    }

    // 获取构造方法的参数列表
    // constructor 
    public static List<String> getParamNames(Constructor<?> constructor) {
        return getParameterNames(constructor);
    }

    private static List<String> getParameterNames(Executable executable) {
        // 获得 Parameter 数组
        return 
      Arrays.stream(executable.getParameters())
            // 获得参数名字 添加到流中返回
            .map(Parameter::getName).collect(Collectors.toList());
    }

    private ParamNameUtil() {
        super();
    }
}

```

#### ParamNameResolver

-  `org.apache.ibatis.reflection.ParamNameResolver` ，参数名解析器。 

```java
public class ParamNameResolver {

    public static final String GENERIC_NAME_PREFIX = "param";

    private final boolean useActualParamName;

    // 参数名映射
    // key：参数顺序
    // value：参数名
    private final SortedMap<Integer, String> names;

    // 是否有注解的参数
    private boolean hasParamAnnotation;

    public ParamNameResolver(Configuration config, Method method) {
        this.useActualParamName = config.isUseActualParamName();
        final Class<?>[] paramTypes = method.getParameterTypes();
        final Annotation[][] paramAnnotations = method.getParameterAnnotations();
        final SortedMap<Integer, String> map = new TreeMap<>();
        int paramCount = paramAnnotations.length;
        // get names from @Param annotations
        for (int paramIndex = 0; paramIndex < paramCount; paramIndex++) {
            // 如果是特殊参数，就跳过
            if (isSpecialParameter(paramTypes[paramIndex])) {
                // skip special parameters
                continue;
            }
            String name = null;
            // 从注解中获取参数
            for (Annotation annotation : paramAnnotations[paramIndex]) {
                if (annotation instanceof Param) {
                    hasParamAnnotation = true;
                    name = ((Param) annotation).value();
                    break;
                }
            }
            if (name == null) {
                // @Param was not specified.
                if (useActualParamName) {
                    // 获取参数的真实参数名
                    name = getActualParamName(method, paramIndex);
                }
                // 最差，使用 map 的排序，作为编号
                if (name == null) {
                    // use the parameter index as the name ("0", "1", ...)
                    // gcode issue #71
                    name = String.valueOf(map.size());
                }
            }
            // 添加到map中
            map.put(paramIndex, name);
        }
        // 构建不可变集合
        names = Collections.unmodifiableSortedMap(map);
    }

    private String getActualParamName(Method method, int paramIndex) {
        return ParamNameUtil.getParamNames(method).get(paramIndex);
    }

    private static boolean isSpecialParameter(Class<?> clazz) {
        return RowBounds.class.isAssignableFrom(clazz) || ResultHandler.class.isAssignableFrom(clazz);
    }

    /**
   * Returns parameter names referenced by SQL providers.
   *
   * @return the names
   */
    public String[] getNames() {
        return names.values().toArray(new String[0]);
    }

    // 获取参数名和值的映射
    public Object getNamedParams(Object[] args) 
        // 获取数组大小
        final int paramCount = names.size();
    // 如果为null 或者为空，就返回null
    if (args == null || paramCount == 0) {
        return null;
    } else if (!hasParamAnnotation && paramCount == 1) {
        // 只有一个非注解参数，直接返回首元素
        Object value = args[names.firstKey()];
        return wrapToMapIfCollection(value, useActualParamName ? names.get(0) : null);
    } else {
        // 集合
        // 组合1：KEY：参数名 VALUE：参数值
        // 组合2：KEY：GENERIC_NAME_PREFIX+参数顺序 VALUE：参数值
        final Map<String, Object> param = new ParamMap<>();
        int i = 0;
        // 遍历集合
        for (Map.Entry<Integer, String> entry : names.entrySet()) {
            // 组合1
            param.put(entry.getValue(), args[entry.getKey()]);
            // add generic param names (param1, param2, ...)
            // 组合2
            final String genericParamName = GENERIC_NAME_PREFIX + (i + 1);
            // ensure not to overwrite parameter named with @Param
            if (!names.containsValue(genericParamName)) {
                param.put(genericParamName, args[entry.getKey()]);
            }
            i++;
        }
        return param;
    }
}

/**
   * Wrap to a {@link ParamMap} if object is {@link Collection} or array.
   *
   * @param object a parameter object
   * @param actualParamName an actual parameter name
   *                        (If specify a name, set an object to {@link ParamMap} with specified name)
   * @return a {@link ParamMap}
   * @since 3.5.5
   */
public static Object wrapToMapIfCollection(Object object, String actualParamName) {
    if (object instanceof Collection) {
        ParamMap<Object> map = new ParamMap<>();
        map.put("collection", object);
        if (object instanceof List) {
            map.put("list", object);
        }
        Optional.ofNullable(actualParamName).ifPresent(name -> map.put(name, object));
        return map;
    } else if (object != null && object.getClass().isArray()) {
        ParamMap<Object> map = new ParamMap<>();
        map.put("array", object);
        Optional.ofNullable(actualParamName).ifPresent(name -> map.put(name, object));
        return map;
    }
    return object;
}

}

```

#### TypeParameterResolver

- `org.apache.ibatis.reflection.TypeParameterResolver` ，工具类，`java.lang.reflect.Type` 参数解析器。因为 Type 是相对冷门的知识，我表示也不会，所以推荐先阅读如下任一文章：

  - 贾博岩 [《我眼中的 Java-Type 体系(1)》](https://www.jianshu.com/p/7649f86614d3)
  - xujun9411 [《java Type 详解》](https://www.jianshu.com/p/cae76008b36b)
-  当存在复杂的继承关系以及泛型定义时， TypeParameterResolver 可以帮助我们解析字段、方法参数或方法返回值的类型。 

##### 暴漏方法

- TypeParameterResolver 暴露了**三个** 公用静态方法，分别用于解析 Field 类型、Method 返回类型、方法参数类型。代码如下 

```java
// 解析属性类型
public static Type resolveFieldType(Field field, Type srcType) {
    // 属性类型
    Type fieldType = field.getGenericType();
    // 定义的类
    Class<?> declaringClass = field.getDeclaringClass();
    // 解析类型
    return resolveType(fieldType, srcType, declaringClass);
}

// 解析方法返回值类型
public static Type resolveReturnType(Method method, Type srcType) {
    // 属性类型
    Type returnType = method.getGenericReturnType();
    // 定义的类
    Class<?> declaringClass = method.getDeclaringClass();
    // 解析类型
    return resolveType(returnType, srcType, declaringClass);
}

// 解析方法参数的类型数组
public static Type[] resolveParamTypes(Method method, Type srcType) {
    // 获得方法参数类型数组
    Type[] paramTypes = method.getGenericParameterTypes();
    // 定义的类
    Class<?> declaringClass = method.getDeclaringClass();
    // 解析类型们
    Type[] result = new Type[paramTypes.length];
    for (int i = 0; i < paramTypes.length; i++) {
        result[i] = resolveType(paramTypes[i], srcType, declaringClass);
    }
    return result;
}
```

##### resolveType

-  `#resolveType(Type type, Type srcType, Class declaringClass)` 方法，解析 `type` 类型。 

```java
// TypeParameterResolver.java

/**
 * 解析类型
 *
 * @param type 类型
 * @param srcType 来源类型
 * @param declaringClass 定义的类
 * @return 解析后的类型
 */
private static Type resolveType(Type type, Type srcType, Class<?> declaringClass) {
    if (type instanceof TypeVariable) {
        return resolveTypeVar((TypeVariable<?>) type, srcType, declaringClass);
    } else if (type instanceof ParameterizedType) {
        return resolveParameterizedType((ParameterizedType) type, srcType, declaringClass);
    } else if (type instanceof GenericArrayType) {
        return resolveGenericArrayType((GenericArrayType) type, srcType, declaringClass);
    } else {
        return type;
    }
}
```

###### resolveParameterizedType

-  `#resolveParameterizedType(ParameterizedType parameterizedType, Type srcType, Class declaringClass)` 方法，解析 ParameterizedType 类型。代码如下 

```java
// TypeParameterResolver.java

/**
 * 解析 ParameterizedType 类型
 *
 * @param parameterizedType ParameterizedType 类型
 * @param srcType 来源类型
 * @param declaringClass 定义的类
 * @return 解析后的类型
 */
private static ParameterizedType resolveParameterizedType(ParameterizedType parameterizedType, Type srcType, Class<?> declaringClass) {
    Class<?> rawType = (Class<?>) parameterizedType.getRawType();
    // 【1】解析 <> 中实际类型
    Type[] typeArgs = parameterizedType.getActualTypeArguments();
    Type[] args = new Type[typeArgs.length];
    for (int i = 0; i < typeArgs.length; i++) {
        if (typeArgs[i] instanceof TypeVariable) {
            args[i] = resolveTypeVar((TypeVariable<?>) typeArgs[i], srcType, declaringClass);
        } else if (typeArgs[i] instanceof ParameterizedType) {
            args[i] = resolveParameterizedType((ParameterizedType) typeArgs[i], srcType, declaringClass);
        } else if (typeArgs[i] instanceof WildcardType) {
            args[i] = resolveWildcardType((WildcardType) typeArgs[i], srcType, declaringClass);
        } else {
            args[i] = typeArgs[i];
        }
    }
    // 【2】
    return new ParameterizedTypeImpl(rawType, null, args);
}

// TypeParameterResolver.java 内部静态类

/**
 * ParameterizedType 实现类
 *
 * 参数化类型，即泛型。例如：List<T>、Map<K, V>等带有参数化的配置
 */
static class ParameterizedTypeImpl implements ParameterizedType {

    // 以 List<T> 举例子

    /**
     * <> 前面实际类型
     *
     * 例如：List
     */
    private Class<?> rawType;
    /**
     * 如果这个类型是某个属性所有，则获取这个所有者类型；否则，返回 null
     */
    private Type ownerType;
    /**
     * <> 中实际类型
     *
     * 例如：T
     */
    private Type[] actualTypeArguments;

    public ParameterizedTypeImpl(Class<?> rawType, Type ownerType, Type[] actualTypeArguments) {
        super();
        this.rawType = rawType;
        this.ownerType = ownerType;
        this.actualTypeArguments = actualTypeArguments;
    }

    @Override
    public Type[] getActualTypeArguments() {
        return actualTypeArguments;
    }

    @Override
    public Type getOwnerType() {
        return ownerType;
    }

    @Override
    public Type getRawType() {
        return rawType;
    }

    @Override
    public String toString() {
        return "ParameterizedTypeImpl [rawType=" + rawType + ", ownerType=" + ownerType + ", actualTypeArguments=" + Arrays.toString(actualTypeArguments) + "]";
    }

}
```

###### resolveWildcardType

-  `#resolveWildcardType(WildcardType wildcardType, Type srcType, Class declaringClass)` 方法，解析 WildcardType 类型。 

```java
// TypeParameterResolver.java

private static Type resolveWildcardType(WildcardType wildcardType, Type srcType, Class<?> declaringClass) {
    // <1.1> 解析泛型表达式下界（下限 super）
    Type[] lowerBounds = resolveWildcardTypeBounds(wildcardType.getLowerBounds(), srcType, declaringClass);
    // <1.2> 解析泛型表达式上界（上限 extends）
    Type[] upperBounds = resolveWildcardTypeBounds(wildcardType.getUpperBounds(), srcType, declaringClass);
    // <2> 创建 WildcardTypeImpl 对象
    return new WildcardTypeImpl(lowerBounds, upperBounds);
}

private static Type[] resolveWildcardTypeBounds(Type[] bounds, Type srcType, Class<?> declaringClass) {
    Type[] result = new Type[bounds.length];
    for (int i = 0; i < bounds.length; i++) {
        if (bounds[i] instanceof TypeVariable) {
            result[i] = resolveTypeVar((TypeVariable<?>) bounds[i], srcType, declaringClass);
        } else if (bounds[i] instanceof ParameterizedType) {
            result[i] = resolveParameterizedType((ParameterizedType) bounds[i], srcType, declaringClass);
        } else if (bounds[i] instanceof WildcardType) {
            result[i] = resolveWildcardType((WildcardType) bounds[i], srcType, declaringClass);
        } else {
            result[i] = bounds[i];
        }
    }
    return result;
}

// TypeParameterResolver.java 内部静态类

/**
 * WildcardType 实现类
 *
 * 泛型表达式（或者通配符表达式），即 ? extend Number、? super Integer 这样的表达式。
 * WildcardType 虽然是 Type 的子接口，但却不是 Java 类型中的一种。
 */
static class WildcardTypeImpl implements WildcardType {

    /**
     * 泛型表达式下界（下限 super）
     */
    private Type[] lowerBounds;
    /**
     * 泛型表达式上界（上界 extends）
     */
    private Type[] upperBounds;

    WildcardTypeImpl(Type[] lowerBounds, Type[] upperBounds) {
        super();
        this.lowerBounds = lowerBounds;
        this.upperBounds = upperBounds;
    }

    @Override
    public Type[] getLowerBounds() {
        return lowerBounds;
    }

    @Override
    public Type[] getUpperBounds() {
        return upperBounds;
    }
}

static class GenericArrayTypeImpl implements GenericArrayType {
    private Type genericComponentType;

    GenericArrayTypeImpl(Type genericComponentType) {
        super();
        this.genericComponentType = genericComponentType;
    }

    @Override
    public Type getGenericComponentType() {
        return genericComponentType;
    }
}
```

######  resolveGenericArrayType

```java
// TypeParameterResolver.java

private static Type resolveGenericArrayType(GenericArrayType genericArrayType, Type srcType, Class<?> declaringClass) {
    // 【1】解析 componentType
    Type componentType = genericArrayType.getGenericComponentType();
    Type resolvedComponentType = null;
    if (componentType instanceof TypeVariable) {
        resolvedComponentType = resolveTypeVar((TypeVariable<?>) componentType, srcType, declaringClass);
    } else if (componentType instanceof GenericArrayType) {
        resolvedComponentType = resolveGenericArrayType((GenericArrayType) componentType, srcType, declaringClass);
    } else if (componentType instanceof ParameterizedType) {
        resolvedComponentType = resolveParameterizedType((ParameterizedType) componentType, srcType, declaringClass);
    }
    // 【2】创建 GenericArrayTypeImpl 对象
    if (resolvedComponentType instanceof Class) {
        return Array.newInstance((Class<?>) resolvedComponentType, 0).getClass();
    } else {
        return new GenericArrayTypeImpl(resolvedComponentType);
    }
}

// TypeParameterResolver.java 内部静态类

/**
 * GenericArrayType 实现类
 *
 * 泛型数组类型，用来描述 ParameterizedType、TypeVariable 类型的数组；即 List<T>[]、T[] 等；
 */
static class GenericArrayTypeImpl implements GenericArrayType {

    /**
     * 数组元素类型
     */
    private Type genericComponentType;

    GenericArrayTypeImpl(Type genericComponentType) {
        super();
        this.genericComponentType = genericComponentType;
    }

    @Override
    public Type getGenericComponentType() {
        return genericComponentType;
    }

}
```

###### resolveTypeVar

- TODO 不知道干嘛的

#### ArrayUtil

-  `org.apache.ibatis.reflection.ArrayUtil` ，数组工具类 

```java
public class ArrayUtil {

    public static int hashCode(Object obj) {
        if (obj == null) {
            // for consistency with Arrays#hashCode() and Objects#hashCode()
            return 0;
        }
        // 普通类
        final Class<?> clazz = obj.getClass();
        if (!clazz.isArray()) {
            return obj.hashCode();
        }
        // 数组类型
        final Class<?> componentType = clazz.getComponentType();
        if (long.class.equals(componentType)) {
            return Arrays.hashCode((long[]) obj);
        } else if (int.class.equals(componentType)) {
            return Arrays.hashCode((int[]) obj);
        } else if (short.class.equals(componentType)) {
            return Arrays.hashCode((short[]) obj);
        } else if (char.class.equals(componentType)) {
            return Arrays.hashCode((char[]) obj);
        } else if (byte.class.equals(componentType)) {
            return Arrays.hashCode((byte[]) obj);
        } else if (boolean.class.equals(componentType)) {
            return Arrays.hashCode((boolean[]) obj);
        } else if (float.class.equals(componentType)) {
            return Arrays.hashCode((float[]) obj);
        } else if (double.class.equals(componentType)) {
            return Arrays.hashCode((double[]) obj);
        } else {
            return Arrays.hashCode((Object[]) obj);
        }
    }

    public static boolean equals(Object thisObj, Object thatObj) {
        if (thisObj == null) {
            return thatObj == null;
        } else if (thatObj == null) {
            return false;
        }
        
        final Class<?> clazz = thisObj.getClass();
        if (!clazz.equals(thatObj.getClass())) {
            return false;
        }
        // 普通类
        if (!clazz.isArray()) {
            return thisObj.equals(thatObj);
        }
        // 数组
        final Class<?> componentType = clazz.getComponentType();
        if (long.class.equals(componentType)) {
            return Arrays.equals((long[]) thisObj, (long[]) thatObj);
        } else if (int.class.equals(componentType)) {
            return Arrays.equals((int[]) thisObj, (int[]) thatObj);
        } else if (short.class.equals(componentType)) {
            return Arrays.equals((short[]) thisObj, (short[]) thatObj);
        } else if (char.class.equals(componentType)) {
            return Arrays.equals((char[]) thisObj, (char[]) thatObj);
        } else if (byte.class.equals(componentType)) {
            return Arrays.equals((byte[]) thisObj, (byte[]) thatObj);
        } else if (boolean.class.equals(componentType)) {
            return Arrays.equals((boolean[]) thisObj, (boolean[]) thatObj);
        } else if (float.class.equals(componentType)) {
            return Arrays.equals((float[]) thisObj, (float[]) thatObj);
        } else if (double.class.equals(componentType)) {
            return Arrays.equals((double[]) thisObj, (double[]) thatObj);
        } else {
            return Arrays.equals((Object[]) thisObj, (Object[]) thatObj);
        }
    }

    public static String toString(Object obj) {
        if (obj == null) {
            return "null";
        }
        final Class<?> clazz = obj.getClass();
        if (!clazz.isArray()) {
            return obj.toString();
        }
        final Class<?> componentType = obj.getClass().getComponentType();
        if (long.class.equals(componentType)) {
            return Arrays.toString((long[]) obj);
        } else if (int.class.equals(componentType)) {
            return Arrays.toString((int[]) obj);
        } else if (short.class.equals(componentType)) {
            return Arrays.toString((short[]) obj);
        } else if (char.class.equals(componentType)) {
            return Arrays.toString((char[]) obj);
        } else if (byte.class.equals(componentType)) {
            return Arrays.toString((byte[]) obj);
        } else if (boolean.class.equals(componentType)) {
            return Arrays.toString((boolean[]) obj);
        } else if (float.class.equals(componentType)) {
            return Arrays.toString((float[]) obj);
        } else if (double.class.equals(componentType)) {
            return Arrays.toString((double[]) obj);
        } else {
            return Arrays.toString((Object[]) obj);
        }
    }

}

```

####  ExceptionUtil

- `org.apache.ibatis.reflection.ExceptionUtil` ，异常工具类。

```java
// ExceptionUtil.java

public class ExceptionUtil {

    private ExceptionUtil() {
        // Prevent Instantiation
    }

    /**
     * 去掉异常的包装
     *
     * @param wrapped 被包装的异常
     * @return 去除包装后的异常
     */
    public static Throwable unwrapThrowable(Throwable wrapped) {
        Throwable unwrapped = wrapped;
        while (true) {
            if (unwrapped instanceof InvocationTargetException) {
                unwrapped = ((InvocationTargetException) unwrapped).getTargetException();
            } else if (unwrapped instanceof UndeclaredThrowableException) {
                unwrapped = ((UndeclaredThrowableException) unwrapped).getUndeclaredThrowable();
            } else {
                return unwrapped;
            }
        }
    }

}
```

