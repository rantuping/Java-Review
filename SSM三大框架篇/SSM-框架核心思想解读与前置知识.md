### SSM - 框架核心思想解读与前置知识

#### 什么是SSM

- 在单体结构中，SSM指的是：SpringMVC+Spring+Mybatis
- 在微服务架构中，SSM指的是：SpringMVC+SpringBoot+Mybatis
- 此定义可能会有歧义，小伙伴不要开撕哈
- **SpringMVC**
  -  Spring MVC属于SpringFrameWork的后续产品，已经融合在Spring Web Flow里面。Spring MVC 分离了[控制器](https://link.zhihu.com/?target=http%3A//baike.baidu.com/view/122229.htm)、模型[对象](https://link.zhihu.com/?target=http%3A//baike.baidu.com/view/2387.htm)、分派器以及处理程序对象的角色，这种分离让它们更容易进行定制。 
- **Spring**
  - Spring是一个开源框架，Spring是于2003 年兴起的一个轻量级的Java 开发框架，由Rod Johnson 在其著作Expert One-On-One J2EE Development and Design中阐述的部分理念和原型衍生而来。它是为了解决企业应用开发的复杂性而创建的。Spring使用基本的JavaBean来完成以前只可能由EJB完成的事情。然而，Spring的用途不仅限于服务器端的开发。从简单性、可测试性和松耦合的角度而言，任何Java应用都可以从Spring中受益。 简单来说，Spring是一个轻量级的控制反转（IoC）和面向切面（AOP）的容器框架。
- **Mybatis**
  - MyBatis 本是[apache](https://link.zhihu.com/?target=http%3A//baike.baidu.com/view/28283.htm)的一个开源项目[iBatis](https://link.zhihu.com/?target=http%3A//baike.baidu.com/view/628102.htm), 2010年这个项目由apache software foundation 迁移到了google code，并且改名为MyBatis 。MyBatis是一个基于Java的[持久层](https://link.zhihu.com/?target=http%3A//baike.baidu.com/view/198047.htm)框架。iBATIS提供的[持久层](https://link.zhihu.com/?target=http%3A//baike.baidu.com/view/198047.htm)框架包括SQL Maps和Data Access Objects（DAO）MyBatis 消除了几乎所有的JDBC代码和参数的手工设置以及结果集的检索。MyBatis 使用简单的 XML或注解用于配置和原始映射，将接口和 Java 的POJOs（Plain Old Java Objects，普通的 Java对象）映射成数据库中的记录。

#### 如何演变到SSM

- 在EJB时代，大量的配置使得系统的开发慢的很，然后呢？出现了问题就解决问题。
- 刚刚开始，原本是使用JDBC操作数据库，然后JDBC操作数据库太繁琐了，然后就又出现了JDBCTemple，用来简化操作，然后在开发中，又发现JDBCTemplate操作还是麻烦，然后又演变出了Hibernate这样的一款全自动框架，然后又发现Hibernate入门简单，但是想要SQL调优是很不方便的，所以，一款半自动框架ibatis出世，后来ibatis被捐献给Apache，改名为Mybatis，并成为Apache的顶级项目，现在已经开源到Github
- Spring框架是一个轻量级框架，其核心就是为了解决繁琐的配置（**虽然SSM的整合配置也挺多，但是随着SpringBoot时代开启，约定大于配置，几乎见不到繁琐的XML配置了**）和实现快速开发
- 核心思想就是为了更快更有效的开发。

#### Spring

- 看到这个，很多人会和你说什么控制反转（IoC），依赖注入（DI）、AOP、事务什么……对新手来说，这些是什么鬼呢？

##### 控制反转

- 我不写定义，就说一下你的电脑。你的电脑上有usb口吧，有hdmi口吧。电脑上用什么键盘，不取决于电脑本身，而是插在usb上的键盘是什么？这个能理解吧。你看控制电脑用什么输入的设备并不是由电脑控制的，而是你插入的键盘是什么来控制的。这个控制不就反了吗？

##### 依赖注入

- 就是控制反转。电脑的输入设备依赖它被插入的是什么键盘。插入机械键盘，输入设备就是机械键盘，插入数字键盘就只能输入数字。从插键盘这个事来说：无非是从电脑角度看，是依赖注入，输入设备是什么依赖注入的键盘是什么；从键盘角度看是控制反转，插入的键盘来控制电脑是什么输入设备。

- 为什么要用依赖注入？目的就是可以灵活搭配。如果你的电脑上的键盘是焊死在电脑上的，键盘坏了，你的电脑也废了。你肯定不想这样吧。

- Java开发中，我们需要创建大量的对象，有些重要的对象牵一发而动全身，这样的对象我们可以交给Spring帮我们来创建，我们只需要提供配置文件，这个配置文件可以是xml，也可以是java本身。好比Spring是个工厂，按图纸就可以做出产品。

##### AOP

- 什么是切面？你可以这样想：一个公司有多个销售部，都需要行政帮忙搞定日常的订文具、机票、办公室、出差酒店什么的，都需要财务搞定报销、收付款什么的事务吧，要仓库帮忙发货什么的。不可能每个销售部都去招几个行政、财务和仓管什么吧，销售部门只要做好业务就行了，订文具什么的事情交个专门的部门去办就行。

- 在Spring AOP 中，把这些公共的事务，也就是这个例子中的行政啊、财务啊、仓管啊，看成一个个切面，让你的开发的时候，专注于核心业务。

##### 事务

- 说个例子：比如我支付宝转给你100块钱，正常操作应该是我的账号里少了100，你的多了100。如果由于各种原因，我少100后，系统出问题了。没有事务的话，你的账号不会多100，而我的账号少了100。这个就是有问题了。有事务的话：要不我少100你多100都成功，要不就都不成功，这样钱才不会少。而手写一个事务还是有点小难度的，Spring的事务(spring-tx)可以帮我们轻松实现。

#### SpringMVC

- 这个的话，你要先理解一下MVC是什么？

- C - Controller：控制器。接受用户请求，调用 Model 处理，然后选择合适的View给客户。

- M - Model：模型。业务处理模型，接受Controller的调遣，处理业务，处理数据。

- V - View：视图。返回给客户看的结果。注意这里的客户未必是人，可能是浏览器，可能是APP。

- 这样想必你不明白，举例：你去饭店吃饭，坐下来后，服务员小妹来找你点菜。你点了个宫保鸡丁，小妹就拿着你的点菜单都后厨。后厨的师傅打开食物储藏柜，找到鸡丁，开炉子，放油，一顿爆炒，装盘。小妹端起盘子回到你的桌子。当然，如果小妹觉得你帅，说不定还会偷偷加个鸡腿给你；如果你碰巧是当年甩了小妹的前男友，说不定小妹会吐口口水在里面。

- 这里，你就是客户，你点菜就是发出请求（Request），小妹就是Controller，后厨就是 Model，小妹最后给的盘子及里面的内容就是View，小妹给你盘子就是响应（Respond）。Controller处理客户请求，反馈给Model。Model处理，返回数据（宫保鸡丁）给Controller（小妹）。小妹可以直接给你，也可以给你加鸡腿或吐口水后给你。最终，你得到了一盘吃的（View）。

- Spring MVC就是个MVC框架，和Spring天生就集成。

#### Mybatis

- 一个帮你和数据库打交道的框架。帮你把数据库的表翻译成类，字段翻译成类的字段，记录翻译成对象。（当然这些全要靠你自己定义。）

- 这样做的后果就是，你可以像写java一样操作数据库了。什么打开JDBC，防止sql注入，不用考虑，MyBatis帮你搞定。

- Spring中，通过简单的设置就可以集成MyBatis这个框架，所以会有SSM框架的说法。

#### ORM思想

- 从配置文件(通常是XML配置文件中)得到 sessionfactory.

- 由sessionfactory 产生 session

- 在session 中完成对数据的增删改查和事务提交等.

- 在用完之后关闭session 。

- 在java 对象和 数据库之间有做mapping 的配置文件，也通常是xml 文件。

