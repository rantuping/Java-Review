### SpringMVC、Spring、Mybatis三大框架篇

#### SSM-框架核心思想解读与前置知识

[SSM-框架核心思想解读与前置知识](./SSM-框架核心思想解读与前置知识.md)

#### Mybatis

- **Mybatis的整体架构分为三层**
 - 基础支持层
-  核心处理层
- 接口层
- 整体架构如下

![1598938458724](images/1598938458724.png)

##### 基础支持层

[SSM-Mybatis3-源码解读-第1篇-开篇](./SSM-Mybatis3-源码解读-第1篇-开篇.md)

[SSM-Mybatis3-源码解读-第2篇-框架目录结构](./SSM-Mybatis3-源码解读-第2篇-框架目录结构.md)

[SSM-Mybatis3-源码解读-第3篇-解析器模块](./SSM-Mybatis3-源码解读-第3篇-解析器模块.md)

[SSM-Mybatis3-源码解读-第4篇-反射模块](./SSM-Mybatis3-源码解读-第4篇-反射模块.md)

[SSM-Mybatis3-源码解读-第5篇-异常模块](./SSM-Mybatis3-源码解读-第5篇-异常模块.md)

[SSM-Mybatis3-源码解读-第6篇-数据源模块](./SSM-Mybatis3-源码解读-第6篇-数据源模块.md)

[SSM-Mybatis3-源码解读-第7篇-事务模块](./SSM-Mybatis3-源码解读-第7篇-事务模块.md)

[SSM-Mybatis3-源码解读-第8篇-缓存模块](./SSM-Mybatis3-源码解读-第8篇-缓存模块.md)

[SSM-Mybatis3-源码解读-第9篇-类型模块](./SSM-Mybatis3-源码解读-第9篇-类型模块.md)

[SSM-Mybatis3-源码解读-第10篇-IO模块](./SSM-Mybatis3-源码解读-第10篇-IO模块.md)

[SSM-Mybatis3-源码解读-第11篇-日志模块](./SSM-Mybatis3-源码解读-第11篇-日志模块.md)

[SSM-Mybatis3-源码解读-第12篇-注解模块](./SSM-Mybatis3-源码解读-第12篇-注解模块.md)

[SSM-Mybatis3-源码解读-第13篇-Binding模块](./SSM-Mybatis3-源码解读-第13篇-Binding模块.md)

##### 核心处理层

- TODO

##### 接口层

- TODO

