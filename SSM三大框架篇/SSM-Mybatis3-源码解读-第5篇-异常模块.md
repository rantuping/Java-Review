### SSM - Mybatis3 - 源码解读 - 第5篇 - 异常模块

#### 概述

- Mybatis的异常模块，对应 `exceptions`

![1599043132911](images/1599043132911.png)

- 定义了Mybatis专有的 PersistenceException和TooManyResultsException异常
- 其他的包下也有异常，如下

![](images/02.png)

#### IbatisException

-  `org.apache.ibatis.exceptions.IbatisException` ，实现 RuntimeException 类，IBatis 的**异常基类** 

```java
@Deprecated
public class IbatisException extends RuntimeException {

    private static final long serialVersionUID = 3880206998166270511L;

    public IbatisException() {
        super();
    }

    public IbatisException(String message) {
        super(message);
    }

    public IbatisException(String message, Throwable cause) {
        super(message, cause);
    }

    public IbatisException(Throwable cause) {
        super(cause);
    }

}

```

- 此异常已经被废弃（2015年），取代它的是 PersistenceException

#### PersistenceException

-  `org.apache.ibatis.exceptions.PersistenceException` ，继承 IbatisException 类，目前 MyBatis **真正的异常基类** 

```java
@SuppressWarnings("deprecation")
public class PersistenceException extends IbatisException {

    private static final long serialVersionUID = -7537395265357977271L;

    public PersistenceException() {
        super();
    }

    public PersistenceException(String message) {
        super(message);
    }

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistenceException(Throwable cause) {
        super(cause);
    }
}

```

#### ExceptionFactory

-  `org.apache.ibatis.exceptions.ExceptionFactory` ，异常工厂 

```java
public class ExceptionFactory {

    private ExceptionFactory() {
        // Prevent Instantiation
    }

    // 被包装成 PersistenceException
    // message 消息
    // e 异常
    public static RuntimeException wrapException(String message, Exception e) {
        return new PersistenceException(ErrorContext.instance().message(message).cause(e).toString(), e);
    }
}

```

#### TooManyResultsException

-  `org.apache.ibatis.exceptions.TooManyResultsException` ，继承 PersistenceException 类，查询返回过多结果的异常。期望返回一条，实际返回了多条 

```java
public class TooManyResultsException extends PersistenceException {

    private static final long serialVersionUID = 8935197089745865786L;

    public TooManyResultsException() {
        super();
    }

    public TooManyResultsException(String message) {
        super(message);
    }

    public TooManyResultsException(String message, Throwable cause) {
        super(message, cause);
    }

    public TooManyResultsException(Throwable cause) {
        super(cause);
    }
}
```

#### parsing 包

##### ParsingException

```java
public class ParsingException extends PersistenceException {
    private static final long serialVersionUID = -176685891441325943L;

    public ParsingException() {
        super();
    }

    public ParsingException(String message) {
        super(message);
    }

    public ParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParsingException(Throwable cause) {
        super(cause);
    }
}

```

#### 其他包

- 实际上，我们会看到其他包，会和 `parsing` 包一样，都会定义其**独有**的异常类。但是，代码都是相同的。所以，这里就简单整理如下：

- `reflection` 包：ReflectionException
- `logging` 包：LogException
- `builder` 包：BuilderException、IncompleteElementException
- `scripting` 包：ScriptingException
- `binding` 包：BindingException
- `type` 包：TypeException
- `session` 包：SqlSessionException
- `cache` 包：CacheException
- `transaction` 包：TransactionException
- `datasource` 包：DataSourceException
- `executor` 包：ResultMapException、ExecutorException、BatchExecutorException
- `plugin` 包：PluginException