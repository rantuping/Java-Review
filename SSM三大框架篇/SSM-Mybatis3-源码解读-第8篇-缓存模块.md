### SSM - Mybatis3 - 源码解读 - 第8篇 - 缓存模块

#### 概述

- 对应`cache`包

- 结构如下

![1599185753144](images/1599185753144.png)

- 在优化系统性能时，优化数据库性能是非常重要的一个环节，而添加缓存则是优化数据库时最有效的手段之一。正确、合理地使用缓存可以将一部分数据库请求拦截在缓存这一层。
- MyBatis 中提供了**一级缓存和二级缓存**，而这两级缓存都是依赖于基础支持层中的缓 存模块实现的。这里需要读者注意的是，MyBatis 中自带的这两级缓存与 MyBatis 以及整个应用是运行在同一个 JVM 中的，共享同一块堆内存。如果这两级缓存中的数据量较大， 则可能影响系统中其他功能的运行，所以当需要缓存大量数据时，优先考虑使用 Redis、Memcache 等缓存产品。

- 类图

![](images/02asdfas.png)

- Mybatis 缓存机制文档： http://www.mybatis.org/mybatis-3/zh/sqlmap-xml.html#cache 

#### Cache

-  `org.apache.ibatis.cache.Cache` ，缓存**容器**接口。**注意，它是一个容器，有点类似 HashMap ，可以往其中添加各种缓存** 

```java
public interface Cache {

    // 获取 cache 的标识
    String getId();

    // 添加指定键的值
    void putObject(Object key, Object value);

    // 获取指定键的值
    Object getObject(Object key);

    // 移除指定的值
    Object removeObject(Object key);

    // 清空缓存
    void clear();

    // 获得容器中的缓存的容量
    int getSize();

    // 获取读写锁
    default ReadWriteLock getReadWriteLock() {
        return null;
    }

}

```

##### PerpetualCache

-  `org.apache.ibatis.cache.impl.PerpetualCache` ，实现 Cache 接口，**永不过期**的 Cache 实现类，基于 HashMap 实现类 

```java
public class PerpetualCache implements Cache {

    // 标识
    private final String id;

    // 缓存容器
    private final Map<Object, Object> cache = new HashMap<>();

    public PerpetualCache(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public int getSize() {
        return cache.size();
    }

    @Override
    public void putObject(Object key, Object value) {
        cache.put(key, value);
    }

    @Override
    public Object getObject(Object key) {
        return cache.get(key);
    }

    @Override
    public Object removeObject(Object key) {
        return cache.remove(key);
    }

    @Override
    public void clear() {
        cache.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (getId() == null) {
            throw new CacheException("Cache instances require an ID.");
        }
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cache)) {
            return false;
        }

        Cache otherCache = (Cache) o;
        return getId().equals(otherCache.getId());
    }

    @Override
    public int hashCode() {
        if (getId() == null) {
            throw new CacheException("Cache instances require an ID.");
        }
        return getId().hashCode();
    }

}

```

##### LoggingCache

-  `org.apache.ibatis.cache.decorators.LoggingCache` ，实现 Cache 接口，**支持打印日志**的 Cache 实现类 

```java
public class LoggingCache implements Cache {

    // Log 对象
    private final Log log;
    // 装饰的 Cache 对象
    private final Cache delegate;
    // 统计请求缓存的次数
    protected int requests = 0;
    // 统计命中的次数
    protected int hits = 0;

    public LoggingCache(Cache delegate) {
        this.delegate = delegate;
        this.log = LogFactory.getLog(getId());
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public int getSize() {
        return delegate.getSize();
    }

    @Override
    public void putObject(Object key, Object object) {
        // 放置新的
        delegate.putObject(key, object);
    }

    // 获取新的
    @Override
    public Object getObject(Object key) {
        // 请求次数++
        requests++;
        // 获得缓存
        final Object value = delegate.getObject(key);
        // 如果命中，则命中次数++
        if (value != null) {
            hits++;
        }
        if (log.isDebugEnabled()) {
            log.debug("Cache Hit Ratio [" + getId() + "]: " + getHitRatio());
        }
        return value;
    }

    @Override
    public Object removeObject(Object key) {
        return delegate.removeObject(key);
    }

    @Override
    public void clear() {
        delegate.clear();
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return delegate.equals(obj);
    }

    // 命中率
    private double getHitRatio() {
        return (double) hits / (double) requests;
    }

}

```

##### BlockingCache

- `org.apache.ibatis.cache.decoratorsBlockingCache` ，实现 Cache 接口，**阻塞的** Cache 实现类。
- 这里的阻塞比较特殊，当线程去获取缓存值时，如果不存在，则会阻塞后续的其他线程去获取该缓存。
  为什么这么有这样的设计呢？因为当线程 A 在获取不到缓存值时，一般会去设置对应的缓存值，这样就避免其他也需要该缓存的线程 B、C 等，重复添加缓存

```java
public class BlockingCache implements Cache {

    // 阻塞等待时间
    private long timeout;
    // 装饰的 Cache 对象
    private final Cache delegate;
    // 缓存键与 ReentrantLock对象的映射
    private final ConcurrentHashMap<Object, ReentrantLock> locks;

    public BlockingCache(Cache delegate) {
        this.delegate = delegate;
        this.locks = new ConcurrentHashMap<>();
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public int getSize() {
        return delegate.getSize();
    }

    @Override
    public void putObject(Object key, Object value) {
        try {
            // 添加缓存
            delegate.putObject(key, value);
        } finally {
            // 释放锁
            releaseLock(key);
        }
    }

    @Override
    public Object getObject(Object key) {
        // 获得锁
        acquireLock(key);
        // 获得缓存
        Object value = delegate.getObject(key);
        // 释放锁
        if (value != null) {
            releaseLock(key);
        }
        return value;
    }

    @Override
    public Object removeObject(Object key) {
        // despite of its name, this method is called only to release locks
        releaseLock(key);
        return null;
    }

    @Override
    public void clear() {
        delegate.clear();
    }

    // 为key获取锁
    private ReentrantLock getLockForKey(Object key) {
        return locks.computeIfAbsent(key, k -> new ReentrantLock());
    }

    // 获得锁
    private void acquireLock(Object key) {
        // 获得ReentrantLock对象
        Lock lock = getLockForKey(key);
        // 获取锁，直到超时
        if (timeout > 0) {
            try {
                boolean acquired = lock.tryLock(timeout, TimeUnit.MILLISECONDS);
                if (!acquired) {
                    throw new CacheException("Couldn't get a lock in " + timeout + " for the key " +  key + " at the cache " + delegate.getId());
                }
            } catch (InterruptedException e) {
                throw new CacheException("Got interrupted while trying to acquire lock for key " + key, e);
            }
        } else {
            // 释放锁
            lock.lock();
        }
    }

    private void releaseLock(Object key) {
        // 根据key 获取锁
        ReentrantLock lock = locks.get(key);
        // 如果当前线程持有，就释放
        if (lock.isHeldByCurrentThread()) {
            lock.unlock();
        }
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }
}

```

##### SynchronizedCache

-  `org.apache.ibatis.cache.decorators.SynchronizedCache` ，实现 Cache 接口，**同步**的 Cache 实现类 

```java
public class SynchronizedCache implements Cache {

    // 装饰的 cache 对象
    private final Cache delegate;

    public SynchronizedCache(Cache delegate) {
        this.delegate = delegate;
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    // 同步
    @Override
    public synchronized int getSize() {
        return delegate.getSize();
    }

    @Override
    public synchronized void putObject(Object key, Object object) {
        delegate.putObject(key, object);
    }

    @Override
    public synchronized Object getObject(Object key) {
        return delegate.getObject(key);
    }

    @Override
    public synchronized Object removeObject(Object key) {
        return delegate.removeObject(key);
    }

    @Override
    public synchronized void clear() {
        delegate.clear();
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return delegate.equals(obj);
    }

}

```

##### SerializedCache

-  `org.apache.ibatis.cache.decorators.SerializedCache` ，实现 Cache 接口，**支持序列化值**的 Cache 实现类。 

```java
public class SerializedCache implements Cache {

    // 封装的  Cache 对象
    private final Cache delegate;
    public SerializedCache(Cache delegate) {
        this.delegate = delegate;
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public int getSize() {
        return delegate.getSize();
    }

    @Override
    public void putObject(Object key, Object object) {
        if (object == null || object instanceof Serializable) {
            delegate.putObject(key, serialize((Serializable) object));
        } else {
            throw new CacheException("SharedCache failed to make a copy of a non-serializable object: " + object);
        }
    }

    @Override
    public Object getObject(Object key) {
        Object object = delegate.getObject(key);
        return object == null ? null : deserialize((byte[]) object);
    }

    @Override
    public Object removeObject(Object key) {
        return delegate.removeObject(key);
    }

    @Override
    public void clear() {
        delegate.clear();
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return delegate.equals(obj);
    }

    private byte[] serialize(Serializable value) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            oos.writeObject(value);
            oos.flush();
            return bos.toByteArray();
        } catch (Exception e) {
            throw new CacheException("Error serializing object.  Cause: " + e, e);
        }
    }

    private Serializable deserialize(byte[] value) {
        Serializable result;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(value);
             ObjectInputStream ois = new CustomObjectInputStream(bis)) {
            result = (Serializable) ois.readObject();
        } catch (Exception e) {
            throw new CacheException("Error deserializing object.  Cause: " + e, e);
        }
        return result;
    }

    public static class CustomObjectInputStream extends ObjectInputStream {

        public CustomObjectInputStream(InputStream in) throws IOException {
            super(in);
        }

        @Override
        protected Class<?> resolveClass(ObjectStreamClass desc) throws ClassNotFoundException {
            return Resources.classForName(desc.getName());
        }

    }

}

```

##### ScheduledCache

-  `org.apache.ibatis.cache.decorators.ScheduledCache` ，实现 Cache 接口，**定时清空整个容器**的 Cache 实现类 

```java
public class ScheduledCache implements Cache {

    // 被装饰的 cache 对象
    private final Cache delegate;
    // 清空间隔 单位：毫秒
    protected long clearInterval;
    // 最后清空的时间
    protected long lastClear;

    public ScheduledCache(Cache delegate) {
        this.delegate = delegate;
        this.clearInterval = TimeUnit.HOURS.toMillis(1);
        this.lastClear = System.currentTimeMillis();
    }

    public void setClearInterval(long clearInterval) {
        this.clearInterval = clearInterval;
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public int getSize() {
        // 判断释放全部清空
        clearWhenStale();
        return delegate.getSize();
    }

    @Override
    public void putObject(Object key, Object object) {
        // 判断释放全部清空
        clearWhenStale();
        delegate.putObject(key, object);
    }

    @Override
    public Object getObject(Object key) {
        return clearWhenStale() ? null : delegate.getObject(key);
    }

    @Override
    public Object removeObject(Object key) {
        // 判断释放全部清空
        clearWhenStale();
        return delegate.removeObject(key);
    }

    @Override
    public void clear() {
        // 记录清空的时间
        lastClear = System.currentTimeMillis();
        // 全部清空
        delegate.clear();
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return delegate.equals(obj);
    }

    private boolean clearWhenStale() {
        if (System.currentTimeMillis() - lastClear > clearInterval) {
            clear();
            return true;
        }
        return false;
    }

}

```

##### FifoCache

-  `org.apache.ibatis.cache.decorators.FifoCache` ，实现 Cache 接口，**基于先进先出的淘汰机制**的 Cache 实现类 

```java
public class FifoCache implements Cache {

    // 装饰的Cache对象
    private final Cache delegate;
    // 双端队列
    private final Deque<Object> keyList;
    // 队列上限
    private int size;

    public FifoCache(Cache delegate) {
        this.delegate = delegate;
        // 使用了 LinkedList
        this.keyList = new LinkedList<>();
        // 默认值为 1024
        this.size = 1024;
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public int getSize() {
        return delegate.getSize();
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public void putObject(Object key, Object value) {
        // 循环 KeyList
        cycleKeyList(key);
        delegate.putObject(key, value);
    }

    @Override
    public Object getObject(Object key) {
        return delegate.getObject(key);
    }

    @Override
    public Object removeObject(Object key) {
        return delegate.removeObject(key);
    }

    @Override
    public void clear() {
        delegate.clear();
        keyList.clear();
    }

    private void cycleKeyList(Object key) {、
        // 添加到 keyList 
        keyList.addLast(key);
		// 超过上限，将队首移除
        if (keyList.size() > size) {
            Object oldestKey = keyList.removeFirst();
            delegate.removeObject(oldestKey);
        }
    }

}

```

##### LruCache

-  `org.apache.ibatis.cache.decorators.LruCache` ，实现 Cache 接口，**基于最少使用的淘汰机制**的 Cache 实现类。 

```java
public class LruCache implements Cache {

    // 装饰的 Cache 对象
    private final Cache delegate;
    // 基于LinkedListMao实现淘汰机制
    private Map<Object, Object> keyMap;
    // 最老的键 即将被淘汰的
    private Object eldestKey;

    public LruCache(Cache delegate) {
        // 初始化对象
        this.delegate = delegate;
        setSize(1024);
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public int getSize() {
        return delegate.getSize();
    }

    public void setSize(final int size) {
        // 构造函数，当第三个参数accessOrder为true的时候，最近访问的在最前面
        keyMap = new LinkedHashMap<Object, Object>(size, .75F, true) {
            private static final long serialVersionUID = 4267176411845948333L;

            // 重写此方法，满足条件就删除数据
            @Override
            protected boolean removeEldestEntry(Map.Entry<Object, Object> eldest) {
                boolean tooBig = size() > size;
                if (tooBig) {
                    eldestKey = eldest.getKey();
                }
                return tooBig;
            }
        };
    }

    @Override
    public void putObject(Object key, Object value) {
        // 添加到缓存
        delegate.putObject(key, value);
        // 循环遍历
        cycleKeyList(key);
    }

    @Override
    public Object getObject(Object key) {
        // 刷新 keyMap的访问顺序
        keyMap.get(key); // touch
        // 获得缓存的值
        return delegate.getObject(key);
    }

    @Override
    public Object removeObject(Object key) {
        return delegate.removeObject(key);
    }

    @Override
    public void clear() {
        delegate.clear();
        keyMap.clear();
    }

    private void cycleKeyList(Object key) {
        // 添加到key中
        keyMap.put(key, key);
        // 如果超过上限，则从delegate中删除，移除最少使用的那一个
        if (eldestKey != null) {
            delegate.removeObject(eldestKey);
            // 置空
            eldestKey = null;
        }
    }

}

```

- LRU缓存机制原理： https://www.cnblogs.com/lzrabbit/p/3734850.html 

##### WaekCache

-  `org.apache.ibatis.cache.decorators.WeakCache` ，实现 Cache 接口，基于 `java.lang.ref.WeakReference` 的 Cache 实现类。 

```java
public class WeakCache implements Cache {
    
    // 强引用的键的队列
    private final Deque<Object> hardLinksToAvoidGarbageCollection;
    // 被GC回收的集合
    private final ReferenceQueue<Object> queueOfGarbageCollectedEntries;
    // 装饰的 Cache 对象
    private final Cache delegate;
    // hardLinksToAvoidGarbageCollection 的大小
    private int numberOfHardLinks;

    public WeakCache(Cache delegate) {
        this.delegate = delegate;
        this.numberOfHardLinks = 256;
        this.hardLinksToAvoidGarbageCollection = new LinkedList<>();
        this.queueOfGarbageCollectedEntries = new ReferenceQueue<>();
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public int getSize() {
        removeGarbageCollectedItems();
        return delegate.getSize();
    }

    public void setSize(int size) {
        this.numberOfHardLinks = size;
    }

    @Override
    public void putObject(Object key, Object value) {
        // 移除已经被GC回收的 WeakEntry
        removeGarbageCollectedItems();
        // 把新的添加到 delegate 中去
        delegate.putObject(key, new WeakEntry(key, value, queueOfGarbageCollectedEntries));
    }

    @Override
    public Object getObject(Object key) {
        Object result = null;
        @SuppressWarnings("unchecked") // assumed delegate cache is totally managed by this cache
        // 获得值得 WeakReference 对象
        WeakReference<Object> weakReference = (WeakReference<Object>) delegate.getObject(key);
        // 不为null
        if (weakReference != null) {
            // 获得值
            result = weakReference.get();
            if (result == null) {
                // 为空，就从delegate中删除，说明已经被GC回收
                delegate.removeObject(key);
            } else {
                // 添加到队头
                hardLinksToAvoidGarbageCollection.addFirst(result);
                if (hardLinksToAvoidGarbageCollection.size() > numberOfHardLinks) {
                    // 移除最后一个
                    hardLinksToAvoidGarbageCollection.removeLast();
                }
            }
        }
        return result;
    }

    @Override
    public Object removeObject(Object key) {
        // 移除已经被GC回收得 WeakEntry
        removeGarbageCollectedItems();
        // 移除出 delegate
        return delegate.removeObject(key);
    }

    @Override
    public void clear() {
        // 清空hardLinksToAvoidGarbageCollection
        hardLinksToAvoidGarbageCollection.clear();
        // 移除 delegate
        removeGarbageCollectedItems();
        // 请空
        delegate.clear();
    }

    // 移除被GC 回收得键
    private void removeGarbageCollectedItems() {
        WeakEntry sv;
        while ((sv = (WeakEntry) queueOfGarbageCollectedEntries.poll()) != null) {
            delegate.removeObject(sv.key);
        }
    }

    private static class WeakEntry extends WeakReference<Object> {
        // 键
        private final Object key;

        private WeakEntry(Object key, Object value, ReferenceQueue<Object> garbageCollectionQueue) {
            super(value, garbageCollectionQueue);
            this.key = key;
        }
    }

}

```

##### SoftCache

-  `org.apache.ibatis.cache.decorators.SoftCache` ，实现 Cache 接口，基于 `java.lang.ref.SoftReference` 的 Cache 实现类。 

```java
public class SoftCache implements Cache {
    // 逻辑与 WeakCache一致，不再赘言
    private final Deque<Object> hardLinksToAvoidGarbageCollection;
    private final ReferenceQueue<Object> queueOfGarbageCollectedEntries;
    private final Cache delegate;
    private int numberOfHardLinks;

    public SoftCache(Cache delegate) {
        this.delegate = delegate;
        this.numberOfHardLinks = 256;
        this.hardLinksToAvoidGarbageCollection = new LinkedList<>();
        this.queueOfGarbageCollectedEntries = new ReferenceQueue<>();
    }

    @Override
    public String getId() {
        return delegate.getId();
    }

    @Override
    public int getSize() {
        removeGarbageCollectedItems();
        return delegate.getSize();
    }

    public void setSize(int size) {
        this.numberOfHardLinks = size;
    }

    @Override
    public void putObject(Object key, Object value) {
        removeGarbageCollectedItems();
        delegate.putObject(key, new SoftEntry(key, value, queueOfGarbageCollectedEntries));
    }

    @Override
    public Object getObject(Object key) {
        Object result = null;
        @SuppressWarnings("unchecked") // assumed delegate cache is totally managed by this cache
        SoftReference<Object> softReference = (SoftReference<Object>) delegate.getObject(key);
        if (softReference != null) {
            result = softReference.get();
            if (result == null) {
                delegate.removeObject(key);
            } else {
                // See #586 (and #335) modifications need more than a read lock
                synchronized (hardLinksToAvoidGarbageCollection) {
                    hardLinksToAvoidGarbageCollection.addFirst(result);
                    if (hardLinksToAvoidGarbageCollection.size() > numberOfHardLinks) {
                        hardLinksToAvoidGarbageCollection.removeLast();
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Object removeObject(Object key) {
        removeGarbageCollectedItems();
        return delegate.removeObject(key);
    }

    @Override
    public void clear() {
        synchronized (hardLinksToAvoidGarbageCollection) {
            hardLinksToAvoidGarbageCollection.clear();
        }
        removeGarbageCollectedItems();
        delegate.clear();
    }

    private void removeGarbageCollectedItems() {
        SoftEntry sv;
        while ((sv = (SoftEntry) queueOfGarbageCollectedEntries.poll()) != null) {
            delegate.removeObject(sv.key);
        }
    }

    private static class SoftEntry extends SoftReference<Object> {
        private final Object key;

        SoftEntry(Object key, Object value, ReferenceQueue<Object> garbageCollectionQueue) {
            super(value, garbageCollectionQueue);
            this.key = key;
        }
    }

}

```

#### CacheKey

- `org.apache.ibatis.cache.CacheKey` ，实现 Cloneable、Serializable 接口，缓存键。
- 因为 MyBatis 中的缓存键不是一个简单的 String ，**而是通过多个对象组成**。所以 CacheKey 可以理解成将多个对象放在一起，计算其缓存键。

```java
public class CacheKey implements Cloneable, Serializable {

    private static final long serialVersionUID = 1146682552656046210L;

    // 单例 - 空缓存键
    public static final CacheKey NULL_CACHE_KEY = new CacheKey() {

        @Override
        public void update(Object object) {
            throw new CacheException("Not allowed to update a null cache key instance.");
        }

        @Override
        public void updateAll(Object[] objects) {
            throw new CacheException("Not allowed to update a null cache key instance.");
        }
    };

    // 默认 multiplier 的值
    private static final int DEFAULT_MULTIPLIER = 37;
    // 默认hashcode得值
    private static final int DEFAULT_HASHCODE = 17;

    // hashcode求值系数
    private final int multiplier;
    // 缓存键得hashcode的值
    private int hashcode;
    // 校验和
    private long checksum;
    // update(Object)的数量
    private int count;
    // 计算 hashcode 的对象的集合
    private List<Object> updateList;

    // 构造方法
    public CacheKey() {
        this.hashcode = DEFAULT_HASHCODE;
        this.multiplier = DEFAULT_MULTIPLIER;
        this.count = 0;
        this.updateList = new ArrayList<>();
    }

    // 当构造方法的方法参数为 Object[] objects 时，
    // 会调用updateAll(Object[] objects) 方法，更新相关属性
    public CacheKey(Object[] objects) {
        this();
        updateAll(objects);
    }

    public int getUpdateCount() {
        return updateList.size();
    }

    // 更新属性
    public void update(Object object) {
        // 方法参数的hashcode
        int baseHashCode = object == null ? 1 : ArrayUtil.hashCode(object);

        count++;
        // checksum 为 baseHashCode的求和
        checksum += baseHashCode;
        baseHashCode *= count;

        // 计算HashCode的值
        hashcode = multiplier * hashcode + baseHashCode;

        // 添加Object的值到 updateList中
        updateList.add(object);
    }

    public void updateAll(Object[] objects) {
        for (Object o : objects) {
            update(o);
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CacheKey)) {
            return false;
        }

        final CacheKey cacheKey = (CacheKey) object;

        if (hashcode != cacheKey.hashcode) {
            return false;
        }
        if (checksum != cacheKey.checksum) {
            return false;
        }
        if (count != cacheKey.count) {
            return false;
        }

        for (int i = 0; i < updateList.size(); i++) {
            Object thisObject = updateList.get(i);
            Object thatObject = cacheKey.updateList.get(i);
            if (!ArrayUtil.equals(thisObject, thatObject)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return hashcode;
    }

    @Override
    public String toString() {
        StringJoiner returnValue = new StringJoiner(":");
        returnValue.add(String.valueOf(hashcode));
        returnValue.add(String.valueOf(checksum));
        updateList.stream().map(ArrayUtil::toString).forEach(returnValue::add);
        return returnValue.toString();
    }

    @Override
    public CacheKey clone() throws CloneNotSupportedException {
        CacheKey clonedCacheKey = (CacheKey) super.clone();
        clonedCacheKey.updateList = new ArrayList<>(updateList);
        return clonedCacheKey;
    }

}

```

#### 二级缓存

- 二级缓存和执行器有很大的关联，放在执行器部分解析