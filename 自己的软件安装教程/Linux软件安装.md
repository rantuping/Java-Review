### Linux软件安装手册

- 本教程基于阿里云Centos7.7 实践
- **注意：在安装maven和Tomcat之前，必须先安装JDK**
- 本手册安装版本如下

![1603623674929](images/1603623674929.png)

- 我将所有的文件都放在 softwore 目录下

![1603624849801](images/1603624849801.png)

#### Linux性能监控软件

**先更新**

```shell
yum update -y
```

![1603778069385](images/1603778069385.png)

**然后执行命令**

```shell
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```

![1603778341162](images/1603778341162.png)

```shell
bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
```

**TODO 安装失败**

#### Git

**方式1**

在 Linux 上安装 Git 向来仅需⼀⾏命令即可搞定，因为各式各样的包管理器帮了我们⼤忙，所以对于
CentOS 系统来讲，直接执⾏如下命令即可安装：

```shell
yum install git 
```

默认版本如下

![1603617149236](images/1603617149236.png)

当然通过这种⽅式安装的 Git 可能不是较新版的 Git ，以我们的实验环境 CentOS 7.7 来说，这种⽅
式安装的 Git 版本为 1.8.3.1 ，不过⼀般来说是够⽤的。

**方式2**

如果想安装较新版本的 Git ，则需要⾃⾏下载 Git 源码来编译安装。
**1、准备Git安装包**
我这⾥选择安装的是 git-2.9.5 版，将下载好的安装包 git-2.9.5.tar.gz 直接放在了 softwore⽬录下
然后将其本地解压，得到 git-2.9.5 ⽬录：

```shell
tar -zxvf 
```

**2、提前安装可能所需的依赖**

```shell
yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel gcc-c++ perl-ExtUtils-MakeMaker
```

注意：上面这个命令执行完毕之后，会再次安装 默认的git版本，此时需要先卸载

```shell
yum -y remove git 
```

**3、编译安装Git**

进⼊到对应⽬录，执⾏配置、编译、安装命令即可，如下所示

```shell
[root@localhost ~]# cd git-2.9.5/
[root@localhost git-2.9.5]# make configure
[root@localhost git-2.9.5]# ./configure --prefix=/usr/local/git
[root@localhost git-2.9.5]# make profix=/usr/local/git
[root@localhost git-2.9.5]# make install
```

如果出现以下的错误：

![1603617539654](images/1603617539654.png)

执行以下命令即可

```shell
yum -y install autoconf
```

**4、将Git加⼊环境变量**
将 Git 的可执⾏程序加⼊环境变量，便于后续使⽤
编辑配置⽂件：

```shell
vim /etc/profile
```

尾部加⼊ Git 的 bin 路径配置即可

```shell
export GIT_HOME=/usr/local/git
export PATH=$PATH:$GIT_HOME/bin
```

最后执⾏ source /etc/profile 使环境变量⽣效即可

**5、查看安装结果**

执⾏ git --version 查看安装后的版本即可

#### JDK8

**卸载已有的OPENJDK（如果有）**

如果系统⾃带有 OpenJDK ，可以按照如下步骤提前卸载之。
⾸先查找已经安装的 OpenJDK 包：

```shell
rpm -qa | grep java
```

如果有，接下来可以将 java 开头的安装包均卸载即可：

```shell
yum -y remove java-xxx 省略
```

**创建⽬录并解压**

**1、在 /usr/local/ 下创建 java ⽂件夹并进⼊**

```shell
cd /usr/local/
mkdir java
cd java
```

**2、将上⾯准备好的 JDK 安装包解压到 /usr/local/java 中即可**

```shell
tar -zxvf /root/jdk-8u201-linux-x64.tar.gz -C ./
```

解压完之后， /usr/local/java ⽬录中会出现⼀个 jdk1.8.0_201 的⽬录

**配置JDK环境变量**

编辑 /etc/profile ⽂件，在⽂件尾部加⼊如下 JDK 环境配置即可

```shell
JAVA_HOME=/usr/local/java/jdk1.8.0_201
CLASSPATH=$JAVA_HOME/lib/
PATH=$PATH:$JAVA_HOME/bin
export PATH JAVA_HOME CLASSPATH
```

![1603623423023](images/1603623423023.png)

然后执⾏如下命令让环境变量⽣效

```shell
source /etc/profile
```

验证JDK安装结果

输⼊如下命令即可检查安装结果：

```shell
java -version
```

![1603623512063](images/1603623512063.png)

```shell
javac
```

![1603623549916](images/1603623549916.png)

#### NodeJS

**创建⽬录并解压**

**1、在 /usr/local/ 下创建 node ⽂件夹并进⼊**

```shell
cd /usr/local/
mkdir node
cd node
```

**2、将 Node 的安装包解压到 /usr/local/node 中即可**

```shell
tar -xJvf /softwore/node-v14.10.1-linux-x64.tar.xz -C ./
```

![1603623774017](images/1603623774017.png)

解压完之后， /usr/local/node ⽬录中会出现⼀个 node-v14.10.1-linux-x64 的⽬录

![1603623845308](images/1603623845308.png)

**配置NODE系统环境变量**

编辑 ~/.bash_profile ⽂件

```shell
vim ~/.bash_profile
```

在⽂件末尾追加如下信息:

```shell
# Nodejs
export PATH=/usr/local/node/node-v14.10.1-linux-x64/bin:$PATH
```

![1603623949119](images/1603623949119.png)

刷新环境变量，使之⽣效即可

```shell
source ~/.bash_profile
```

**检查安装结果**

```shell
node -v
```

![1603624022519](images/1603624022519.png)

```shell
npm version
```

![1603624054244](images/1603624054244.png)

```shell
npx -v
```

![1603624085031](images/1603624085031.png)

#### Maven项⽬构建和管理⼯具安装

新建文件夹

```shell
cd /usr/local
mkdir maven
cd maven 
```

然后执行解压命令，在maven下得到一个目录 apache-maven-3.6.0

```shell
tar zxvf /softwore/apache-maven-3.6.0-bin.tar.gz
```

**配置MAVEN加速镜像源**

这⾥配置的是阿⾥云的maven镜像源。
编辑修改 /usr/local/maven/apache-maven-3.6.0/conf/settings.xml

```shell
vim /usr/local/maven/apache-maven-3.6.0/conf/settings.xml
```

⽂件，在 <mirrors></mirrors> 标签对⾥添加如下内容即可：

```xml
<mirror>
    <id>alimaven</id>
    <name>aliyun maven</name>
    <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
    <mirrorOf>central</mirrorOf>
</mirror>
```

![1603624384925](images/1603624384925.png)

**配置环境变量**
因为下载的是⼆进制版安装包，所以解压完，配置好环境变量即可使⽤了

编辑修改 /etc/profile ⽂件，在⽂件尾部添加如下内容，配置 maven 的安装路径

```shell
export MAVEN_HOME=/usr/local/maven/apache-maven-3.6.0
export PATH=$MAVEN_HOME/bin:$PATH
```

![1603624501683](images/1603624501683.png)

接下来执⾏ source /etc/profile 来刷新环境变量，让 maven 环境的路径配置⽣效，

检验安装结果

执⾏ mvn –v ，能打印出 maven 版本信息说明安装、配置成功：

![1603624528964](images/1603624528964.png)

#### MySQL数据库部署和安装

卸载系统⾃带的MARIADB（如果有）

如果系统之前⾃带 Mariadb ，可以先卸载之。

⾸先查询已安装的 Mariadb 安装包：

```shell
rpm -qa|grep mariadb
```

![1603624646808](images/1603624646808.png)

将其卸载

```shell
yum -y remove mariadb-libs-5.5.64-1.el7.x86_64
```

![1603624698641](images/1603624698641.png)

**解压MySQL安装包**

将准备好的 MySQL 安装包解压到 /usr/local/ ⽬录，并重命名为 mysql

```shell
# 解压
tar -zxvf /softwore/mysql-5.7.31-linux-glibc2.12-x86_64.tar.gz -C /usr/local/
# 改名
mv mysql-5.7.31-linux-glibc2.12-x86_64 mysql
```

![1603624934546](images/1603624934546.png)

**创建MySQL⽤户和⽤户组**

```shell
groupadd mysql
useradd -g mysql mysql
```

![1603624989104](images/1603624989104.png)

同时新建 /usr/local/mysql/data ⽬录，后续备⽤

修改MYSQL⽬录的归属⽤户 

![1603625041070](images/1603625041070.png)

**准备MySQL的配置⽂件**

在 /etc ⽬录下新建 my.cnf ⽂件

![1603625100054](images/1603625100054.png)

写⼊如下简化配置：

```shell
[mysql]
# 设置mysql客户端默认字符集
default-character-set=utf8
socket=/var/lib/mysql/mysql.sock
[mysqld]
skip-name-resolve
#设置3306端⼝
port = 3306
socket=/var/lib/mysql/mysql.sock
# 设置mysql的安装⽬录
basedir=/usr/local/mysql
# 设置mysql数据库的数据的存放⽬录
datadir=/usr/local/mysql/data
# 允许最⼤连接数
max_connections=200
# 服务端使⽤的字符集默认为8⽐特编码的latin1字符集
character-set-server=utf8
# 创建新表时将使⽤的默认存储引擎
default-storage-engine=INNODB
lower_case_table_names=1
max_allowed_packet=16M
```

同时使⽤如下命令创建 /var/lib/mysql ⽬录，并修改权限：

```shell
mkdir /var/lib/mysql
chmod 777 /var/lib/mysql
```

**正式开始安装MySQL**

执⾏如下命令正式开始安装：

```shell
# 进入目录
cd /usr/local/mysql 
# 安装
./bin/mysqld --initialize --user=mysql --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data
```

这个时候报错如下：

![1603625436328](images/1603625436328.png)

这是因为缺少包 libaio

```shell
 yum install libaio
```

![1603625506056](images/1603625506056.png)

然后再执行安装

![1603625573541](images/1603625573541.png)

**复制启动脚本到资源⽬录**

执⾏如下命令复制：

```shell
[root@iZ39eskhxdfqwyZ mysql]# cp ./support-files/mysql.server /etc/init.d/mysqld
```

![1603625626448](images/1603625626448.png)

并修改 /etc/init.d/mysqld ，修改其 basedir 和 datadir 为实际对应⽬录：

```shell
# 编辑
vim /etc/init.d/mysqld 

# 修改
basedir=/usr/local/mysql
datadir=/usr/local/mysql/data
```

![1603625698676](images/1603625698676.png)

**设置MySQL系统服务并开启⾃启**

⾸先增加 mysqld 服务控制脚本执⾏权限：

```shell
chmod +x /etc/init.d/mysqld
```

同时将 mysqld 服务加⼊到系统服务

```shell
chkconfig --add mysqld
```

最后检查 mysqld 服务是否已经⽣效即可：

```shell
chkconfig --list mysqld
```

![1603625845323](images/1603625845323.png)

这样就表明 mysqld 服务已经⽣效了，在2、3、4、5运⾏级别随系统启动⽽⾃动启动，以后可以直接使
⽤ service 命令控制 mysql 的启停。

**启动MySQLD**

直接执⾏：

```shell
service mysqld start
```

![1603625909356](images/1603625909356.png)

**将 MYSQL 的 BIN ⽬录加⼊ PATH 环境变量**
这样⽅便以后在任意⽬录上都可以使⽤ mysql 提供的命令。

编辑 ~/.bash_profile ⽂件，在⽂件末尾处追加如下信息:

```shell
export PATH=$PATH:/usr/local/mysql/bin
```

![1603625969436](images/1603625969436.png)

最后执⾏如下命令使环境变量⽣效

```shell
source ~/.bash_profile
```

**⾸次登陆MYSQL**

以 root 账户登录 mysql ，使⽤上⽂安装完成提示的密码进⾏登⼊

```shell
mysql -u root -p
```

![1603626063298](images/1603626063298.png)

**接下来修改ROOT账户密码**

在mysql的命令⾏执⾏如下命令即可，密码可以换成你想⽤的密码即可：

```mysql
mysql>alter user user() identified by "root";
mysql>flush privileges;
```

![1603626125594](images/1603626125594.png)

**设置远程主机登录**

```mysql
mysql> use mysql;
mysql> update user set user.Host='%' where user.User='root';
mysql> flush privileges;
```

![1603626179004](images/1603626179004.png)

**最后利⽤NAVICAT等⼯具进⾏测试即可：**

![1603626255458](images/1603626255458.png)

#### Redis缓存安装部署

解压安装包

**1、在 /usr/local/ 下创建 redis ⽂件夹并进⼊**

```shell
cd /usr/local/
mkdir redis
cd redis
```

**2、将 Redis 安装包解压到 /usr/local/redis 中即可**

```shell
[root@iZ39eskhxdfqwyZ redis]# tar zxvf /softwore/redis-5.0.5.tar.gz -C ./
```

解压完之后， /usr/local/redis ⽬录中会出现⼀个 redis-5.0.5 的⽬录

**编译并安装**

```shell
cd redis-5.0.5/
make && make install
```

![1603626483859](images/1603626483859.png)

**将 Redis 安装为系统服务并后台启动**

进⼊ utils ⽬录，并执⾏如下脚本即可：

```shell
[root@iZ39eskhxdfqwyZ redis-5.0.5]# cd utils/
[root@iZ39eskhxdfqwyZ utils]# ./install_server.sh
```

![1603626630480](images/1603626630480.png)

**查看Redis服务启动情况**

直接执⾏如下命令来查看Redis的启动结果：

```shell
systemctl status redis_6379.service
```

启动Redis客户端并测试

启动⾃带的 redis-cli 客户端，测试通过：

![1603626837900](images/1603626837900.png)

但是此时只能在本地访问，⽆法远程连接，因此还需要做部分设置

**设置允许远程连接**

编辑 redis 配置⽂件

```shell
vim /etc/redis/6379.conf
```

将 bind 127.0.0.1 修改为 0.0.0.0

![1603626901450](images/1603626901450.png)

然后重启 Redis 服务即可

```shell
systemctl restart redis_6379.service
```

**设置访问密码**

编辑 redis 配置⽂件

```shell
vim /etc/redis/6379.conf
```

找到如下内容：

```shell
#requirepass foobared
```

去掉注释，将 foobared 修改为⾃⼰想要的密码，保存即可。

```shell
requirepass icanci
```

保存，重启 Redis 服务即可

```shell
systemctl restart redis_6379.service
```

#### 消息队列 RabbitMQ 安装部署

因为 RabbitMQ 需要 erlang 环境的⽀持，所以必须先安装 erlang

我们这⾥要安装的是 erlang-22.3.3-1.el7.x86_64.rpm ，所以⾸先执⾏如下命令来安装其对应的 yum repo 

```shell
curl -s https://packagecloud.io/install/repositories/rabbitmq/erlang/script.rpm.sh | sudo bash
```

![1603627102695](images/1603627102695.png)

接下来执⾏如下命令正式安装 erlang 环境

```shell
yum install erlang-22.3.3-1.el7.x86_64
```

![1603627162716](images/1603627162716.png)

接下来可以直接执⾏如下命令，测试 erlang 是否安装成功

```shell
erl
```

![1603627199014](images/1603627199014.png)

**安装RABBITMQ**

接下来正式开始安装 rabbitmq ，⾸先依然是安装其对应的 yum repo ：

```shell
curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.rpm.sh | sudo bash
```

![1603627257855](images/1603627257855.png)

然后执⾏如下命令正式安装 rabbitmq 

```shell
yum install rabbitmq-server-3.8.3-1.el7.noarch
```

![1603627297270](images/1603627297270.png)

**设置RABBITMQ开机启动**

```shell
chkconfig rabbitmq-server on
```

**启动RABBITMQ服务**

```shell
systemctl start rabbitmq-server.service
```

**开启WEB可视化管理插件：**

```shell
rabbitmq-plugins enable rabbitmq_management
```

![1603627429436](images/1603627429436.png)

**我们可以在后台先添加⼀个⽤户/密码对：**

```shell
rabbitmqctl add_user icanci icanci
rabbitmqctl set_user_tags icanci administrator
```

![1603627519960](images/1603627519960.png)

**远程连接的账户配置**

```shell
rabbitmqctl add_user rootic rootic
rabbitmqctl set_user_tags rootic administrator
rabbitmqctl set_permissions -p / rootic ".*" ".*" ".*"
```

**Java连接远程RabbitMQ配置文件**

```java
package cn.icanci.seckill.config;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: icanci
 * @date: Created in 2020/10/23 8:45
 * @classAction: RabbitMQ 配置文件
 */
@Configuration
public class RabbitMQConfig {
    /**
     * 以下代码用于初始化时的地址信息配置 2
     */
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {

        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(new Jackson2JsonMessageConverter());
        return template;
    }

    /**
     * 以下代码用于初始化时的地址信息配置 1
     */
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory =
                new CachingConnectionFactory("47.103.56.126",
                        5672);
        connectionFactory.setUsername("rootic");
        connectionFactory.setPassword("rootic");
         connectionFactory.setVirtualHost("/");
        connectionFactory.setPublisherConfirms(true);
        return connectionFactory;
    }

}

```

**测试远程连接** 

- 注意：需要开启 5672 端口的安全组

```java
package cn.icanci.seckill;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author: icanci
 * @date: Created in 2020/10/23 9:38
 * @classAction: 测试远程连接RabbitMQ
 */
public class Send {
    private final static String QUEUE_NAME = "hello";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("47.103.56.126");
        factory.setUsername("rootic");
        factory.setPassword("rootic");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        String message = "Hello World!";
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        System.out.println(" [x] Sent '" + message + "'");
        channel.close();
        connection.close();
    }
}
```

#### Web服务器Nginx安装部署

⾸先安装包并解压

**1、在 /usr/local/ 下创建 nginx ⽂件夹并进⼊**

```shell
cd /usr/local/
mkdir nginx
cd nginx
```

**2、将 Nginx 安装包解压到 /usr/local/nginx 中即可**

```shell
[root@iZ39eskhxdfqwyZ nginx]# tar zxvf /softwore/nginx-1.18.0.tar.gz -C ./
```

解压完之后， /usr/local/nginx ⽬录中会出现⼀个 nginx-1.18.0 的⽬录

**预先安装额外的依赖**

```shell
yum -y install pcre-devel
yum -y install openssl openssl-devel
```

**编译安装Nginx**

```shell
cd nginx-1.18.0
./configure
make && make install
```

**安装完成后，Nginx的可执⾏⽂件位置位于**

```shell
/usr/local/nginx/sbin/nginx
```

**启动NGINX**

直接执⾏如下命令即可：

```shell
[root@iZ39eskhxdfqwyZ nginx-1.18.0]# /usr/local/nginx/sbin/nginx
```

如果想停⽌Nginx服务，可执⾏：

```shell
/usr/local/nginx/sbin/nginx -s stop
```

如果修改了配置⽂件后想重新加载Nginx，可执⾏：

```shell
/usr/local/nginx/sbin/nginx -s reload
```

注意其配置⽂件位于：

```shell
/usr/local/nginx/conf/nginx.conf
```

浏览器验证启动情况

![1603628252549](images/1603628252549.png)

#### 应⽤服务器Tomcat安装部署

**解压并安装**

**在 /usr/local/ 下创建 tomcat ⽂件夹并进入**

```shell
cd /usr/local/
mkdir tomcat
cd tomcat
```

**将 Tomcat 安装包解压到 /usr/local/tomcat 中即可**

```shell
[root@iZ39eskhxdfqwyZ tomcat]# tar -zxvf /softwore/apache-tomcat-8.5.57.tar.gz -C ./
```

解压完之后， /usr/local/tomcat ⽬录中会出现⼀个 apache-tomcat-8.5.57 的⽬录

**启动TOMCAT**

直接进 apache-tomcat-8.5.57 ⽬录，执⾏其中 bin ⽬录下的启动脚本即可

```shell
[root@iZ39eskhxdfqwyZ bin]# ./startup.sh
```

**网页访问**

![1603628509844](images/1603628509844.png)

**配置快捷操作和开机启动**

⾸先进⼊ /etc/rc.d/init.d ⽬录，创建⼀个名为 tomcat 的⽂件，并赋予执⾏权限

```shell
[root@iZ39eskhxdfqwyZ bin]# cd /etc/rc.d/init.d
[root@iZ39eskhxdfqwyZ init.d]# touch tomcat
[root@iZ39eskhxdfqwyZ init.d]# chmod +x tomcat
```

接下来编辑 tomcat ⽂件，并在其中加⼊如下内容：

```shell
#!/bin/bash
#chkconfig:- 20 90
#description:tomcat
#processname:tomcat
TOMCAT_HOME=/usr/local/tomcat/apache-tomcat-8.5.57
case $1 in
start) su root $TOMCAT_HOME/bin/startup.sh;;
stop) su root $TOMCAT_HOME/bin/shutdown.sh;;
*) echo "require start|stop" ;;
esac
```

这样后续对于Tomcat的开启和关闭只需要执⾏如下命令即可：

```shell
service tomcat start
service tomcat stop
```

最后加⼊开机启动即可：

```shell
chkconfig --add tomcat
chkconfig tomcat on
```

#### Jenkins安装部署

TODO

#### Gitlab安装部署

TODO

#### Nexus 安装

TODO

#### Sonarqube 安装

TODO

#### Docker环境安装

TODO

#### K8S集群部署

TODO

#### ElasticSearch集群部署

TODO

#### Zookeeper安装部署

TODO

#### 消息队列Kafka安装部署

TODO

#### 注意事项

实验过程中如果出现⼀些诸如客户端不能连通或访问等问题，可尝试考虑关闭防⽕墙：

```shell
systemctl stop firewalld.service
systemctl disable firewalld.service
```

