### Spring Cloud 版本 详解

**五大神兽**

- Eureka 服务注册与发现
- Ribbon 负载均衡
- Feign 负载均衡
- Hystrix 断路器
- Zuul 路由网关
- Spring Cloud Config 分布式配置中心

**微服务的四个核心问题**

- 服务很多，客户端怎么访问
- 这么多服务？服务之间怎么通信
- 这么多服务，如何治理？
- 服务挂了怎么办？

**解决方案**

- Spring Cloud NetFlix
  - 一站式解决方案
  - api网关：zuul组件
  - Feign 基于 HttpClient http 通信方式，同步阻塞
  - 服务注册于发现：Eureka
  - 熔断机制：Hystrix
- Apache Dubbo Zookeeper
  - 半自动，需要整合别人的
  - api网关：没有，找第三方组件或者自己实现
  - Dubbo：RPC框架
  - Zookeeper：服务注册与发现
  - 熔断机制：没有
- Spring Cloud Alibaba
  - 新的一站式解决方案
- 万变不离其中
  - API网关
  - HTTP通信或者RPC通信
  - 服务注册与发现
  - 熔断机制

#### 微服务简介

微服务化的核心就是将传统的一站式应用，根据业务拆分成一个一个的服务，彻底地去耦合,每一个微服务提供单个业务功能的服务，一个服务做一件事，从技术角度看就是一种小而独立的处理过程，类似进程概念，能够自行单独启动或销毁，拥有自己独立的数据库。

**业界大牛马丁.福勒（Martin Fowler） 这样描述微服务**
论文网址：https://martinfowler.com/articles/microservices.html

**微服务**
强调的是服务的大小，它关注的是某一个点，是具体解决某一个问题/提供落地对应服务的一个服务应用,
狭意的看,可以看作Eclipse里面的一个个微服务工程/或者Module

微服务架构是⼀种架构模式，它提倡将单⼀应⽤程序划分成⼀组⼩的服务，服务之间互相协调、互相配合，为⽤户提供最终价值。每个服务运⾏在其独⽴的进程中，服务与服务间采⽤轻量级的通信机制互相协作（通常是基于HTTP协议的RESTful API）。每个服务都围绕着具体业务进⾏构建，并且能够被独⽴的部署到⽣产环境、类⽣产环境等。另外，应当尽量避免统⼀的、集中式的服务管理机制，对具体的⼀个服务⽽⾔，应根据业务上下⽂，选择合适的语⾔、⼯具对其进⾏构建。

**SpringCloud**

SpringCloud，基于SpringBoot提供了一套微服务解决方案，包括服务注册与发现，配置中心，全链路监控，服务网关，负载均衡，熔断器等组件，除了基于NetFlix的开源组件做高度抽象封装之外，还有一些选型中立的开源组件。

SpringCloud利用SpringBoot的开发便利性巧妙地简化了分布式系统基础设施的开发，SpringCloud为开发人员提供了快速构建分布式系统的一些工具，包括配置管理、服务发现、断路器、路由、微代理、事件总线、全局锁、决策竞选、分布式会话等等,它们都可以用SpringBoot的开发风格做到一键启动和部署。

SpringBoot并没有重复制造轮子，它只是将目前各家公司开发的比较成熟、经得起实际考验的服务框架组合起来，通过SpringBoot风格进行再封装屏蔽掉了复杂的配置和实现原理，最终给开发者留出了一套简单易懂、易部署和易维护的分布式系统开发工具包



##### 常见的微服务组件及概念

- **服务注册**：服务提供方将自己调用地址注册到服务注册中心，让服务调用方能够方便地找到自己。
- **服务发现**：服务调用方从服务注册中心找到自己需要调用的服务的地址。
- **负载均衡**：服务提供方一般以多实例的形式提供服务，负载均衡功能能够让服务调用方连接到合适的服务节点。并且，节点选择的工作对服务调用方来说是透明的。
- **服务网关**：服务网关是服务调用的唯一入口，可以在这个组件是实现用户鉴权、动态路由、灰度发布、A/B 测试、负载限流等功能。
- **配置中心**：将本地化的配置信息（properties, xml, yaml 等）注册到配置中心，实现程序包在开发、测试、生产环境的无差别性，方便程序包的迁移。
- **API 管理**：以方便的形式编写及更新 API 文档，并以方便的形式供调用者查看和测试。
- **集成框架**：微服务组件都以职责单一的程序包对外提供服务，集成框架以配置的形式将所有微服务组件（特别是管理端组件）集成到统一的界面框架下，让用户能够在统一的界面中使用系统。
- **分布式事务**：对于重要的业务，需要通过分布式事务技术（TCC、高可用消息服务、最大努力通知）保证数据的一致性。
- **调用链**：记录完成一个业务逻辑时调用到的微服务，并将这种串行或并行的调用关系展示出来。在系统出错时，可以方便地找到出错点。
- **支撑平台**：系统微服务化后，系统变得更加碎片化，系统的部署、运维、监控等都比单体架构更加复杂，那么，就需要将大部分的工作自动化。现在，可以通过 Docker 等工具来中和这些微服务架构带来的弊端。 例如持续集成、蓝绿发布、健康检查、性能健康等等。严重点，以我们两年的实践经验，可以这么说，如果没有合适的支撑平台或工具，就不要使用微服务架构。

微服务架构的优点：

- **降低系统复杂度**：每个服务都比较简单，只关注于一个业务功能。
- **松耦合**：微服务架构方式是松耦合的，每个微服务可由不同团队独立开发，互不影响。
- **跨语言**：只要符合服务 API 契约，开发人员可以自由选择开发技术。这就意味着开发人员可以采用新技术编写或重构服务，由于服务相对较小，所以这并不会对整体应用造成太大影响。
- **独立部署**：微服务架构可以使每个微服务独立部署。开发人员无需协调对服务升级或更改的部署。这些更改可以在测试通过后立即部署。所以微服务架构也使得 CI／CD 成为可能。
- **Docker 容器**：和 Docker 容器结合的更好。
- **DDD 领域驱动设计**：和 DDD 的概念契合，结合开发会更好。

微服务架构的缺点：

- **微服务强调了服务大小，但实际上这并没有一个统一的标准**：业务逻辑应该按照什么规则划分为微服务，这本身就是一个经验工程。有些开发者主张 10-100 行代码就应该建立一个微服务。虽然建立小型服务是微服务架构崇尚的，但要记住，微服务是达到目的的手段，而不是目标。微服务的目标是充分分解应用程序，以促进敏捷开发和持续集成部署。
- **微服务的分布式特点带来的复杂性**：开发人员需要基于 RPC 或者消息实现微服务之间的调用和通信，而这就使得服务之间的发现、服务调用链的跟踪和质量问题变得的相当棘手。
- **分区的数据库体系和分布式事务**：更新多个业务实体的业务交易相当普遍，不同服务可能拥有不同的数据库。CAP 原理的约束，使得我们不得不放弃传统的强一致性，而转而追求最终一致性，这个对开发人员来说是一个挑战。
- **测试挑战**：传统的单体WEB应用只需测试单一的 REST API 即可，而对微服务进行测试，需要启动它依赖的所有其他服务。这种复杂性不可低估。
- **跨多个服务的更改**：比如在传统单体应用中，若有 A、B、C 三个服务需要更改，A 依赖 B，B 依赖 C。我们只需更改相应的模块，然后一次性部署即可。但是在微服务架构中，我们需要仔细规划和协调每个服务的变更部署。我们需要先更新 C，然后更新 B，最后更新 A。
- **部署复杂**：微服务由不同的大量服务构成。每种服务可能拥有自己的配置、应用实例数量以及基础服务地址。这里就需要不同的配置、部署、扩展和监控组件。此外，我们还需要服务发现机制，以便服务可以发现与其通信的其他服务的地址。因此，成功部署微服务应用需要开发人员有更好地部署策略和高度自动化的水平。
- **总的来说（问题和挑战）**：API Gateway、服务间调用、服务发现、服务容错、服务部署、数据调用。

不过，现在很多微服务的框架（比如 Spring Cloud、Dubbo）已经很好的解决了上面的问题。

#### 从面试题开始

- 什么是微服务
- 微服务之间是如何通信的
- SpringCloud和Dubbo有哪些区别？
- 什么是服务熔断？什么是服务降级
- 微服务的优缺点是什么？说一下你在项目开发中遇到的坑
- 你所知道的微服务技术栈有哪些？请列举一二
- Eureka 和 Zookeeper都可以提供服务注册与发现的功能，请说说两个的区别
- ....

#### 微服务概述

##### 微服务技术栈

|微服务条目|	落地技术	|备注|
| :----: | :----: | :----: |
| 服务开发	| Springboot、Spring、SpringMVC	|
| 服务配置与管理| 	Netflix公司的Archaius、阿里的Diamond等	|
| 服务注册与发现	| Eureka、Consul、Zookeeper等	|
| 服务调用	| Rest、RPC、gRPC|
| 服务熔断器	| Hystrix、Envoy等	|
| 负载均衡| 	Ribbon、Nginx等	|
| 服务接口调用(客户端调用服务的简化工具)| 	Feign等	|
| 消息队列	| Kafka、RabbitMQ、ActiveMQ等	|
| 服务配置中心管理| 	SpringCloudConfig、Chef等	|
| 服务路由（API网关）| 	Zuul等	|
| 服务监控	| Zabbix、Nagios、Metrics、Spectator等	|
| 全链路追踪| 	Zipkin，Brave、Dapper等	|
| 服务部署| 	Docker、OpenStack、Kubernetes等	|
| 数据流操作开发包| 	SpringCloud Stream（封装与Redis,Rabbit、Kafka等发送接收消息）|
| 事件消息总线	| Spring Cloud Bus	|
| ......		| |

***SpringCloud=分布式微服务架构下的一站式解决方案，是各个微服务架构落地技术的集合体，俗称微服务全家桶***

##### SpringBoot和SpringCloud的关系


SpringBoot专注于快速方便的开发单个个体微服务。

SpringCloud是关注全局的微服务协调整理治理框架，它将SpringBoot开发的一个个单体微服务整合并管理起来，
为各个微服务之间提供，配置管理、服务发现、断路器、路由、微代理、事件总线、全局锁、决策竞选、分布式会话等等集成服务

SpringBoot可以离开SpringCloud独立使用开发项目，但是SpringCloud离不开SpringBoot，属于依赖的关系.

SpringBoot专注于快速、方便的开发单个微服务个体，SpringCloud关注全局的服务治理框架。

**学习网址：**

- 官网： http://projects.spring.io/spring-cloud/
- 参考书：https://springcloud.cc/spring-cloud-netflix.html
- 本文档开发API说明1：http://cloud.spring.io/spring-cloud-static/Dalston.SR1/   
- 本文档开发API说明2：https://springcloud.cc/spring-cloud-dalston.html
- springcloud中国社区：http://springcloud.cn/
- springcloud中文网：https://springcloud.cc/

#### 架构图

- 传统的架构图

![1595125463787](images/1595125463787.png)

- 微服务架构图





#### Rest微服务构建案例工程模块

##### 基于maven构建

**spring-cloud-ic 整体父工程Project** 

- 新建父工程spring-cloud-ic，切记是Packageing是pom模式

- 主要是定义POM文件，将后续各个子模块公用的jar包等统一提出来，类似一个抽象父类

- 依赖 pom

- ```xml
  
  <!-- 打包方式 -->
  <packaging>pom</packaging>
  
  <properties>
      <java.version>1.8</java.version>
      <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
      <maven.compiler.source>1.8</maven.compiler.source>
      <maven.compiler.target>1.8</maven.compiler.target>
      <junit.version>4.12</junit.version>
      <lombok.version>1.18.12</lombok.version>
      <log4j.version>1.2.17</log4j.version>
  </properties>
  <dependencyManagement>
      <dependencies>
          <!-- Spring Cloud 的依赖 -->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-dependencies</artifactId>
              <version>Hoxton.SR1</version>
              <type>pom</type>
              <scope>import</scope>
          </dependency>
          <!-- Spring Boot 依赖 -->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-dependencies</artifactId>
              <version>2.3.0.RELEASE</version>
              <type>pom</type>
              <scope>import</scope>
          </dependency>
          <!-- Mysql 数据库 驱动 -->
          <dependency>
              <groupId>mysql</groupId>
              <artifactId>mysql-connector-java</artifactId>
              <version>5.1.47</version>
          </dependency>
          <!-- Druid 连接池 -->
          <dependency>
              <groupId>com.alibaba</groupId>
              <artifactId>druid-spring-boot-starter</artifactId>
              <version>1.1.22</version>
          </dependency>
          <!-- Spring Boot 启动器 -->
          <dependency>
              <groupId>org.mybatis.spring.boot</groupId>
              <artifactId>mybatis-spring-boot-starter</artifactId>
              <version>2.1.2</version>
          </dependency>
          <!-- Junit -->
          <!-- 单元测试 -->
          <dependency>
              <groupId>junit</groupId>
              <artifactId>junit</artifactId>
              <version>${junit.version}</version>
              <scope>test</scope>
          </dependency>
          <!-- lombok -->
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <version>${lombok.version}</version>
          </dependency>
          <!-- log4j -->
          <dependency>
              <groupId>log4j</groupId>
              <artifactId>log4j</artifactId>
              <version>${log4j.version}</version>
          </dependency>
      </dependencies>
  </dependencyManagement>
  ```


#### Eureka

- 设计图
- ![1595144231887](images/1595144231887.png)

- 思路
  - 导入依赖
  - 编写配置文件
  - 开启这个功能 @Enablexxx
  - 配置文件