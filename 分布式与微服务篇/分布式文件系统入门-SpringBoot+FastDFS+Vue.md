### 分布式文件系统入门-SpringBoot+FastDFS+Vue

#### 什么是分布式文件系统

- **应用场景**
- 解决文件太多高效存储的问题，用户访问量大保证可靠的下载速度

- **什么是文件系统**
- 文件系统是操作系统用于明确存储设置或者分区上的文件的方法和数据的结构，也就是在存储设备上组织文件上的方法。操作系统中负责管理和存储文件信息的软件机构叫做文件管理系统，简称文件系统
- **什么是分布式文件系统**
-  分布式文件系统（Distributed File System，DFS）是指[文件系统](https://baike.baidu.com/item/文件系统/4827215)管理的物理存储资源不一定直接连接在本地节点上，而是通过计算机网络与节点（可简单的理解为一台[计算机](https://baike.baidu.com/item/计算机/140338)）相连；或是若干不同的逻辑[磁盘](https://baike.baidu.com/item/磁盘/2842227)分区或卷标组合在一起而形成的完整的有层次的文件系统。DFS为分布在网络上任意位置的资源提供一个逻辑上的树形文件系统结构，从而使用户访问分布在网络上的共享文件更加简便。单独的 DFS共享文件夹的作用是相对于通过网络上的其他共享文件夹的访问点 

- **主流的分布式文件系统**
- **NFS（Network File System） 网络文件系统**
  - 是FreeBSD支持的文件系统中的一种，它允许网络中的计算机之间通过TCP/IP网络共享资源，在NFS的应用中， 能使使用者访问网络上别处的文件就像在使用自己的计算机一样 
- **GFS（Google File System）**
  -  GFS是一个可扩展的[分布式文件系统](https://baike.baidu.com/item/分布式文件系统/1250388)，用于大型的、分布式的、对大量数据进行访问的应用。它运行于廉价的普通硬件上，并提供容错功能。它可以给大量的用户提供总体性能较高的服务 
  - 采用主从结构，一个GFS集群有一个master和大量的chunkserver组成
  - master存储了数据文件的元数据，一个文件被分为了若干块存储在chunkserver中
  - 用户从master中获取数据元信息，在chunkserver存储数据

- **HDFS（Hadoop File System）**
  -  Hadoop分布式文件系统(HDFS)是指被设计成适合运行在通用硬件(commodity hardware)上的分布式文件系统（Distributed File System）。它和现有的分布式文件系统有很多共同点。但同时，它和其他的分布式文件系统的区别也是很明显的。HDFS是一个高度容错性的系统，适合部署在廉价的机器上。HDFS能提供高吞吐量的数据访问，非常适合大规模数据集上的应用。HDFS放宽了一部分POSIX约束，来实现流式读取文件系统数据的目的。HDFS在最开始是作为Apache Nutch搜索引擎项目的基础架构而开发的。HDFS是Apache Hadoop Core项目的一部分。 
  -  HDFS采用了主从（Master/Slave）结构模型，一个HDFS集群是由一个NameNode和若干个DataNode组成的。其中NameNode作为主服务器，管理文件系统的命名空间和客户端对文件的访问操作；集群中的DataNode管理存储的数据。 

**分布式文件服务提供商**

- 阿里的OSS
- 七牛云存储
- 百度云存储

#### 什么是FastDFS

-  FastDFS是一个开源的轻量级[分布式文件系统](https://baike.baidu.com/item/分布式文件系统/1250388)，它对文件进行管理，功能包括：文件存储、文件同步、文件访问（文件上传、文件下载）等，解决了大容量存储和[负载均衡](https://baike.baidu.com/item/负载均衡/932451)的问题。特别适合以文件为载体的在线服务，如相册网站、视频网站等等。 

- 上面介绍的NFS、GFS都是通用的分布式文件系统，通用的分布式文件系统的优点是开发体验好，但是系统复杂性高、性能一般，而专用的分布式文件系统虽然开发体验性差，但是系统复杂性低并且并发性能高。
- FastDFS非常适合存储图片等哪些小文件，fastDFS不对文件进行分块，所以它就没有分块合并的开销，fastDSF 采用socket，通信速度很快

**FastDFS工作原理**

- **FastDFS工作原理**
- 安装： https://my.oschina.net/shxjinchao/blog/3156300 
-  https://www.cnblogs.com/chiangchou/p/fastdfs.html 