### Java加餐篇之Java-SPI机制

**最近在准备学习Spring源码，这是一个浩大的工程，所以要考一些知识点，圈起来考的。**

#### SPI是什么

- SPI全称Service Provider Interface，是Java提供的一套用来被第三方实现或者扩展的API，它可以用来启用框架扩展和替换组件。具体机制如下图所示

![](images/1.webp)

- Java SPI 实际上是“**基于接口的编程＋策略模式＋配置文件**”组合实现的动态加载机制。
- 系统设计的各个抽象，往往有很多不同的实现方案，在面向的对象的设计里，一般推荐模块之间基于接口编程，模块之间不对实现类进行硬编码。一旦代码里涉及具体的实现类，就违反了可拔插的原则，如果需要替换一种实现，就需要修改代码。为了实现在模块装配的时候能不在程序里动态指明，这就需要一种服务发现机制。
   Java SPI就是提供这样的一个机制：为某个接口寻找服务实现的机制。有点类似IOC的思想，就是将装配的控制权移到程序之外，在模块化设计中这个机制尤其重要。所以SPI的核心思想就是**解耦**。

#### 使用场景

- **调用者根据实际使用需要，启用、扩展、或者替换框架的实现策略**
- 比较常见的例子：
  - 数据库驱动加载接口实现类的加载。JDBC加载不同类型数据库的驱动。
  - 日志门面接口实现类加载。SLF4J加载不同提供商的日志实现类。
  - Spring。Spring中大量使用了SPI，比如：对servlet3.0规范对ServletContainerInitializer的实现、自动类型转换Type Conversion SPI(Converter SPI、Formatter SPI)等
  - Dubbo。Dubbo中也大量使用SPI的方式实现框架的扩展, 不过它对Java提供的原生SPI做了封装，允许用户扩展实现Filter接口。

#### 使用介绍

- **要使用SPI，必须遵守以下约定**
  - 当服务提供者提供了接口的一种具体实现后，在jar包的META-INF/services目录下创建一个以“接口全限定名”为命名的文件，内容为实现类的全限定名
  - 接口实现类所在的jar包放在主程序的classpath中
  - 主程序通过java.util.ServiceLoder动态装载实现模块，它通过扫描META-INF/services目录下的配置文件找到实现类的全限定名，把类加载到JVM
  - SPI的实现类必须携带一个不带参数的构造方法

#### 代码示例

- **步骤1：定义一个接口、并且编写其实现**。

```java
public interface IShout {
    /**
     * say hello
     */
    void sayHello();
}

public class ShoutOne implements IShout {
    /**
     * say hello
     */
    @Override
    public void sayHello() {
        System.out.println("ShoutOne.sayHello");
    }
}

public class ShoutTwo implements IShout {
    /**
     * say hello
     */
    @Override
    public void sayHello() {
        System.out.println("ShoutTwo.sayHello");
    }
}

```

- **步骤2：在src/main/resources/ 下建立 /META-INF/services 目录， 新增一个以接口全限定类名命名的文件，目录层级如下所示**

```shell
- src
    -main
        -resources
            - META-INF
                - services
                    - cn.icanci.demo.IShout
```

- **文件内容如下**

```shell
cn.icanci.demo.ShoutOne
cn.icanci.demo.ShoutTwo
```

- **步骤3：编写测试main函数进行测试**

```java
public class Main {
    public static void main(String[] args) {
        ServiceLoader<IShout> load = ServiceLoader.load(IShout.class);
        Iterator<IShout> iterator = load.iterator();
        while (iterator.hasNext()) {
            IShout next = iterator.next();
            next.sayHello();
        }
    }
}
// 输出结果如下：
// ShoutOne.sayHello
// ShoutTwo.sayHello
```

#### 源码解析

- 其源代码如下所示

```java

package java.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.AccessController;
import java.security.AccessControlContext;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public final class ServiceLoader<S>
  implements Iterable<S>
{

  // 需要加载的文件的路径地址
  private static final String PREFIX = "META-INF/services/";

  // 需要被加载的服务或者类(可以是接口、抽象类、普通类)
  private final Class<S> service;

  // 类加载器
  private final ClassLoader loader;

  // 创建ServiceLoader时采用的访问控制上下文
  private final AccessControlContext acc;

  // 缓存providers，按实例化的顺序排列，也就是被创建的目标实例对象
  private LinkedHashMap<String,S> providers = new LinkedHashMap<>();

  // 懒查找迭代器
  private LazyIterator lookupIterator;

  // 重新加载，先清空，再加载
  public void reload() {
    providers.clear();
    lookupIterator = new LazyIterator(service, loader);
  }

  // 服务加载
  private ServiceLoader(Class<S> svc, ClassLoader cl) {
    service = Objects.requireNonNull(svc, "Service interface cannot be null");
    loader = (cl == null) ? ClassLoader.getSystemClassLoader() : cl;
    acc = (System.getSecurityManager() != null) ? AccessController.getContext() : null;
    reload();
  }

  // 加载失败
  private static void fail(Class<?> service, String msg, Throwable cause)
    throws ServiceConfigurationError
  {
    throw new ServiceConfigurationError(service.getName() + ": " + msg,
                                        cause);
  }

  // 加载失败
  private static void fail(Class<?> service, String msg)
    throws ServiceConfigurationError
  {
    throw new ServiceConfigurationError(service.getName() + ": " + msg);
  }

  // 加载失败
  private static void fail(Class<?> service, URL u, int line, String msg)
    throws ServiceConfigurationError
  {
    fail(service, u + ":" + line + ": " + msg);
  }

  // 从配置文件中解析一行，并且将改行的名称加载在列表中
  private int parseLine(Class<?> service, URL u, BufferedReader r, int lc,
                        List<String> names)
    throws IOException, ServiceConfigurationError
  {
    String ln = r.readLine();
    if (ln == null) {
      return -1;
    }
    int ci = ln.indexOf('#');
    if (ci >= 0) ln = ln.substring(0, ci);
    ln = ln.trim();
    int n = ln.length();
    if (n != 0) {
      if ((ln.indexOf(' ') >= 0) || (ln.indexOf('\t') >= 0))
        fail(service, u, lc, "Illegal configuration-file syntax");
      int cp = ln.codePointAt(0);
      if (!Character.isJavaIdentifierStart(cp))
        fail(service, u, lc, "Illegal provider-class name: " + ln);
      for (int i = Character.charCount(cp); i < n; i += Character.charCount(cp)) {
        cp = ln.codePointAt(i);
        if (!Character.isJavaIdentifierPart(cp) && (cp != '.'))
          fail(service, u, lc, "Illegal provider-class name: " + ln);
      }
      if (!providers.containsKey(ln) && !names.contains(ln))
        names.add(ln);
    }
    return lc + 1;
  }

  // 通过 URL 加载
  private Iterator<String> parse(Class<?> service, URL u)
    throws ServiceConfigurationError
  {
    InputStream in = null;
    BufferedReader r = null;
    ArrayList<String> names = new ArrayList<>();
    try {
      in = u.openStream();
      r = new BufferedReader(new InputStreamReader(in, "utf-8"));
      int lc = 1;
      while ((lc = parseLine(service, u, r, lc, names)) >= 0);
    } catch (IOException x) {
      fail(service, "Error reading configuration file", x);
    } finally {
      try {
        if (r != null) r.close();
        if (in != null) in.close();
      } catch (IOException y) {
        fail(service, "Error closing configuration file", y);
      }
    }
    return names.iterator();
  }

  // 懒加载迭代器
  private class LazyIterator
    implements Iterator<S>
  {

    Class<S> service;
    ClassLoader loader;
    Enumeration<URL> configs = null;
    Iterator<String> pending = null;
    String nextName = null;

    private LazyIterator(Class<S> service, ClassLoader loader) {
      this.service = service;
      this.loader = loader;
    }

    private boolean hasNextService() {
      if (nextName != null) {
        return true;
      }
      if (configs == null) {
        try {
          String fullName = PREFIX + service.getName();
          if (loader == null)
            configs = ClassLoader.getSystemResources(fullName);
          else
            configs = loader.getResources(fullName);
        } catch (IOException x) {
          fail(service, "Error locating configuration files", x);
        }
      }
      while ((pending == null) || !pending.hasNext()) {
        if (!configs.hasMoreElements()) {
          return false;
        }
        pending = parse(service, configs.nextElement());
      }
      nextName = pending.next();
      return true;
    }

    private S nextService() {
      if (!hasNextService())
        throw new NoSuchElementException();
      String cn = nextName;
      nextName = null;
      Class<?> c = null;
      try {
        c = Class.forName(cn, false, loader);
      } catch (ClassNotFoundException x) {
        fail(service,
             "Provider " + cn + " not found");
      }
      if (!service.isAssignableFrom(c)) {
        fail(service,
             "Provider " + cn  + " not a subtype");
      }
      try {
        S p = service.cast(c.newInstance());
        providers.put(cn, p);
        return p;
      } catch (Throwable x) {
        fail(service,
             "Provider " + cn + " could not be instantiated",
             x);
      }
      throw new Error();          // This cannot happen
    }

    public boolean hasNext() {
      if (acc == null) {
        return hasNextService();
      } else {
        PrivilegedAction<Boolean> action = new PrivilegedAction<Boolean>() {
          public Boolean run() { return hasNextService(); }
        };
        return AccessController.doPrivileged(action, acc);
      }
    }

    public S next() {
      if (acc == null) {
        return nextService();
      } else {
        PrivilegedAction<S> action = new PrivilegedAction<S>() {
          public S run() { return nextService(); }
        };
        return AccessController.doPrivileged(action, acc);
      }
    }

    public void remove() {
      throw new UnsupportedOperationException();
    }

  }

  // 迭代器
  public Iterator<S> iterator() {
    return new Iterator<S>() {

      Iterator<Map.Entry<String,S>> knownProviders
        = providers.entrySet().iterator();

      public boolean hasNext() {
        if (knownProviders.hasNext())
          return true;
        return lookupIterator.hasNext();
      }

      public S next() {
        if (knownProviders.hasNext())
          return knownProviders.next().getValue();
        return lookupIterator.next();
      }

      public void remove() {
        throw new UnsupportedOperationException();
      }

    };
  }

  // 加载
  public static <S> ServiceLoader<S> load(Class<S> service,
                                          ClassLoader loader)
  {
    return new ServiceLoader<>(service, loader);
  }

  // 加载
  public static <S> ServiceLoader<S> load(Class<S> service) {
    ClassLoader cl = Thread.currentThread().getContextClassLoader();
    return ServiceLoader.load(service, cl);
  }

  // 使用扩展类加载器为给定的服务类型创建一个新的服务加载器
  public static <S> ServiceLoader<S> loadInstalled(Class<S> service) {
    ClassLoader cl = ClassLoader.getSystemClassLoader();
    ClassLoader prev = null;
    while (cl != null) {
      prev = cl;
      cl = cl.getParent();
    }
    return ServiceLoader.load(service, prev);
  }

  
  public String toString() {
    return "java.util.ServiceLoader[" + service.getName() + "]";
  }
}

```

#### 实现流程

- 应用程序调用ServiceLoader.load方法

- 然后获取类加载器和类，创建ServiceLoader对象

- 实例化其中的对象，如下

  - loader(ClassLoader类型，类加载器)
  - acc(AccessControlContext类型，访问控制器)
  - providers(LinkedHashMap<String,S>类型，用于缓存加载成功的类)
  - lookupIterator(实现迭代器功能)

- 应用程序通过迭代器接口获取对象实例

  - ServiceLoader先判断成员变量providers对象中(LinkedHashMap<String,S>类型)是否有缓存实例对象，如果有缓存，直接返回。
  - 如果没有缓存，执行类的装载，实现如下
  - 读取META-INF/services/下的配置文件，获得所有能被实例化的类的名称，值得注意的是，ServiceLoader**可以跨越jar包获取META-INF下的配置文件**，具体加载配置的实现代码如下：

  ```java
  private boolean hasNextService() {
    if (nextName != null) {
      return true;
    }
    if (configs == null) {
      try {
        String fullName = PREFIX + service.getName();
        if (loader == null)
          configs = ClassLoader.getSystemResources(fullName);
        else
          configs = loader.getResources(fullName);
      } catch (IOException x) {
        fail(service, "Error locating configuration files", x);
      }
    }
    while ((pending == null) || !pending.hasNext()) {
      if (!configs.hasMoreElements()) {
        return false;
      }
      pending = parse(service, configs.nextElement());
    }
    nextName = pending.next();
    return true;
  }
  ```

  - 通过反射方法Class.forName()加载类对象，并用instance()方法将类实例化。
  - 把实例化后的类缓存到providers对象中，(LinkedHashMap<String,S>类型）然后返回实例对象。

#### 总结

##### 优点

- 使用Java SPI机制的优势是实现解耦，使得第三方服务模块的装配控制的逻辑与调用者的业务代码分离，而不是耦合在一起。应用程序可以根据实际业务情况启用框架扩展或替换框架组件。
- 相比使用提供接口jar包，供第三方服务模块实现接口的方式，SPI的方式使得源框架，不必关心接口的实现类的路径，可以不用通过下面的方式获取接口实现类：
  - 代码硬编码import 导入实现类
  - 指定类全路径反射获取：例如在JDBC4.0之前，JDBC中获取数据库驱动类需要通过**Class.forName("com.mysql.jdbc.Driver")**，类似语句先动态加载数据库相关的驱动，然后再进行获取连接等的操作
  - 第三方服务模块把接口实现类实例注册到指定地方，源框架从该处访问实例
- 通过SPI的方式，第三方服务模块实现接口后，在第三方的项目代码的META-INF/services目录下的配置文件指定实现类的全路径名，源码框架即可找到实现类

##### 缺点

- 虽然ServiceLoader也算是使用的延迟加载，但是基本只能通过遍历全部获取，也就是接口的实现类全部加载并实例化一遍。如果你并不想用某些实现类，它也被加载并实例化了，这就造成了浪费。获取某个实现类的方式不够灵活，只能通过Iterator形式获取，不能根据某个参数来获取对应的实现类。
- 多个并发多线程使用ServiceLoader类的实例是不安全的。

#### 参考文章

**作者：caison**
**链接：https://www.jianshu.com/p/46b42f7f593c**