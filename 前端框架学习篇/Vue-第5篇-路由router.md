### Vue-第5篇-路由router

#### 简介

- vue-router
- 官方提供的用来实现SPA的插件
- Github地址： https://github.com/vuejs/vue-router 
- 中文文档： http://router.vuejs.org/zh-cn/ 
- 下载: npm install vue-router --save

#### 相关API说明

- VueRouter()：用来创建路由器的构建函数

```js
new VueRouter({
	// 多个配置项
})
```

- 路由配置

```js
routes:[
    {	// 一般路由
		path:'/about',
        component:About
    },
    {	// 自动跳转路由
        path:'/',
        rediret:'/about'
    }
]
```

- 注册路由器

```js
import router from './router'
new Vue({
    router
})
```

- 使用路由组件标签

```js
<router-link>: // 用来生成路由连接
	<router-link to="/xxx">Go to xxx </router-link>
<router-view></router-view>
```

#### 基本路由

- TODO

#### 嵌套路由

- TODO

#### 向路由组件传递数据

- TODO

#### 缓存路由器组件对象

- TODO

#### 编程式路由导航

- TODO
---
**学疏才浅，这个地方学不动了，先扔这儿 ... 我一定会回来的**

