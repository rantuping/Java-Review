### Vue-第2篇-组件化编码

#### 使用vue-cli创建模板项目

**说明**

- Vue-cli 是Vue官方提供的脚手架工具
- github： https://github.com/vuejs/vue-cli
- 作用：从  https://github.com/vuejs-template 下载模板项目

**Vue环境安装的彩坑**

- 环境安装：
  - 老版本：npm install -g vue-cli
  - 新版本： npm i -g @vue/cli 

- npm ERR! code EEXIST！
  - https://blog.csdn.net/miin_ying/article/details/105487128 
  - https://blog.csdn.net/qq_38019124/article/details/105289103 
- 参考文章： https://blog.csdn.net/qq_42582489/article/details/107396963 

**创建vue项目**

- vue init webpack xxx
- cd xxx
- npm install
- npm run dev

![1599874631082](images/1599874631082.png)

- 访问地址： http://localhost:8080/ 

![1599874757181](images/1599874757181.png)

#### 基于Vue脚手架编写项目案例

```vue
// HelloWorld.vue
<template>
<div>
    <p class="msg">{{msg}}</p>
    </div>
</template>

<script>
    // 配置对象与Vue一致
    export default {
        // 必须写函数
        data () {
            return {
                msg: 'hello Vue component'
            }
        }
    }
</script>

<style>
    .msg {
        color: red;
        font-size: 30px;
    }
</style>

```

```vue
// App.vue
<template>
<div>
    <img class="logo" src="./assets/logo.png" alt="logo">
    <!--第三步，使用标签组件-->
    <HelloWorld>

    </HelloWorld>
    </div>
</template>

<script>
    // 第一步，引入组件
    import HelloWorld from './components/HelloWorld'

    export default {
        //第二步，映射组件标签
        components: {
            HelloWorld
        }
    }
</script>

<style>
    .logo{
        width:200px;
        height: 200px;
    }
</style>

```

```js
/**
 * 入口js，创建Vue实例
 */
import Vue from 'vue'
import App from './App.vue'

new Vue({
    el: '#app',
    components: {
        App
    },
    template: '<App/>'
})

```

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>vue_demo2</title>
    </head>
    <body>
        <div id="app"></div>
        <!-- built files will be auto injected -->
    </body>
</html>

```

- 运行项目 npm run dev

![1599890954457](images/1599890954457.png)

#### 项目的打包与发布

**打包**

- npm run build

**发布1：使用静态服务器工具包**

- npm install -g serve
- serve dist
- 访问： http://localhost:5000/ 

**发布2：使用Web服务器（Tomcat）**

- 修改配置文件：webpack.prod.conf.js

```js
  output: {
    path: config.build.assetsRoot,
    filename: utils.assetsPath('js/[name].[chunkhash].js'),
    chunkFilename: utils.assetsPath('js/[id].[chunkhash].js'),
    // 添加以下内容
    publicPath: '/vue_demo02/'
  },
```

-  修改之后需要重新打包。然后把dist文件夹修改为 publicPath 的名字，因为项目的文件引用目录已经修改，如下，然后把dist文件修改名字之后的文件，放到Tomcat下webapps文件夹下，启动tomcat，地址:端口号/项目名 即可访问

![1599891629682](images/1599891629682.png)

#### eslint

**说明**

- ESLint 是一个代码规范检查工具
- 它定义了很多特地的规则，一旦你的代码违反了某一规则，eslint 会做出非常有用的提示
- 官网：http://eslint.org/
- 基本上替代以前的 JSLint

**ESLint 提供以下支持**

- ES
- JSX
- style检查
- 自定义错误和提示

**ESLint 提供以下几种校验**

- 语法错误校验
- 不重要或者堆是的标点符号，如分号
- 没法运行到的代码块
- 未被使用的参数提醒
- 确保样式的同意规则，如 sass或者less
- 检查变量的命名

**规则的错误等级有三种**

- 0：关闭规则
- 1：打开规则，并且作为一个警告，黄色字体
- 2：打开规则，并且作为一个错误，红色字体

**相关配置文件**

```js
.eslintrc.js : 全局规则配置文件
'rules': {
'no-new': 1
}

在 js/vue 文件中修改局部规则
/* eslint-disable no-new */
new Vue({
el: 'body',
components: { App }
})

.eslintignore: 指令检查忽略的文件
*.js
*.vue

修改配置文件 webpack.base.conf.js
  module: {
    rules: [
      // 把下面的这个注释掉
      // ...(config.dev.useEslint ? [createLintingRule()] : []),
      {}
        
```

#### 组件的定义和使用

- 详情请看代码

- 组件之间通信的三种方式
  - props
  - vue自定义组件
  - 消息订阅与发布（PubSubJS库）
    - 订阅消息：PubSub.subscribe('msg', function(msg, data){})
    - 发布消息：PubSub.publish('msg', data)
  - slot