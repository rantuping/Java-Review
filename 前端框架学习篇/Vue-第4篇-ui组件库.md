### Vue-第4篇-ui组件库

#### Mint UI

- 主页：https://mint-ui.github.io/#!/zh-cn
- 说明：饿了么开源的基于vue的移动端UI组件库

#### Element UI

- 主页：https://element.eleme.cn/#/zh-CN
- 说明：饿了么开源的基于vue的PC端UI组件库

#### 使用 Mint UI

- 下载：npm install --save mint-ui

- 实现按需打包：就是用到了什么就打包啥，没用到的不需要打包

```json
// .babelrc 文件 在 plugins 中添加

[
    "component",
    [
        {
            "libraryName": "mint-ui",
            "style": true
        }
    ]
]
]

// 如下


"plugins": [
    "transform-vue-jsx",
    "transform-runtime",
    [
        "component",
        [
            {
                "libraryName": "mint-ui",
                "style": true
            }
        ]
    ]
],
"env": {
    "test": {
        "presets": [
            "env",
            "stage-2"
        ]
    }
}
}

```

#### mint-ui 组件分类

- 标签组件
- 非标签组件

#### 使用mint-ui的组件

```js
import Vue from 'vue'
import App from './App'
import {Button,Toast} from 'mint-ui'

// 注册成标签
Vue.component(Button.name, Button)
Vue.component(Toast.name, Toast)

new Vue({
  el: '#app',
  components: {App},
  template: '<App/>'
})

```

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,
                                       minimum-scale=1, user-scalable=no"/>
        <link rel="stylesheet" href="./static/css/bootstrap.css">
        <title>vue_demo2</title>
    </head>
    <body>
        <div id="app"></div>
        <!-- built files will be auto injected -->
        <script
                src="https://as.alipayobjects.com/g/component/fastclick/1.0.6/fastclick.js"></script>
        <script>
            if ('addEventListener' in document) {
                document.addEventListener('DOMContentLoaded', function () {
                    FastClick.attach(document.body)
                }, false)
            }
            if (!window.Promise) {
                document.writeln(' < script src = "https://as.alipayobjects.com/g/component/es6-promise/3.2.2/es6-promise.min.js"> ' + ' < ' + '/' + 'script >')
            }
        </script>
    </body>
</html>

```

```vue
<template>
<div>
    <mt-button type="primary" @click.native="handleClick">default</mt-button>
    </div>
</template>

<script>
    import {Toast} from 'mint-ui'

    export default {
        methods:{
            handleClick(){
                Toast('提示信息')
            }
        }
    }
</script>

<style>

</style>

```

