###  Nodejs基础学习

#### 简介

- 入门手册： http://nodejs.cn/learn  
- API文档： http://nodejs.cn/api/ 
- 以下介绍来自官网

- Node.js 是一个开源与跨平台的 JavaScript 运行时环境。 它是一个可用于几乎任何项目的流行工具！
- Node.js 在浏览器外运行 V8 JavaScript 引擎（Google Chrome 的内核）。 这使 Node.js 表现得非常出色。
- Node.js 应用程序运行于单个进程中，无需为每个请求创建新的线程。 Node.js 在其标准库中提供了一组异步的 I/O 原生功能（用以防止 JavaScript 代码被阻塞），并且 Node.js 中的库通常是使用非阻塞的范式编写的（从而使阻塞行为成为例外而不是规范）。
- 当 Node.js 执行 I/O 操作时（例如从网络读取、访问数据库或文件系统），Node.js 会在响应返回时恢复操作，而不是阻塞线程并浪费 CPU 循环等待。
- 这使 Node.js 可以在一台服务器上处理数千个并发连接，而无需引入管理线程并发的负担（这可能是重大 bug 的来源）。
- Node.js 具有独特的优势，因为为浏览器编写 JavaScript 的数百万前端开发者现在除了客户端代码之外还可以编写服务器端代码，而无需学习完全不同的语言。
- 在 Node.js 中，可以毫无问题地使用新的 ECMAScript 标准，因为不必等待所有用户更新其浏览器，你可以通过更改 Node.js 版本来决定要使用的 ECMAScript 版本，并且还可以通过运行带有标志的 Node.js 来启用特定的实验中的特性。

#### 进程和线程

##### 进程

- 进程负责为程序得允许提供必备的环境
- 进程就相当于工厂中的车间

##### 线程

- 线程是计算机中最小的计算单位，线程负责执行进程中的程序
- 相当于是工厂中的工人

##### 单线程和多线程

- js是单线程的
- node 是单线程的，但是在后台拥有一个I/O线程池
- Java支持多线程

#####  Node的用途

- Web服务API，如Rest
- 实时多人游戏
- 后端的Web服务，例如：跨域、服务端的请求
- 基于Web的应用
- 多客户端的通信，如即时通讯

#### node执行js文件

```javascript
// hello.js
console.log("hello world");
```

- 在cmd命令行执行 `node hello.js` 命令行窗口就会打印此内容

#### ECMScript标准的缺陷

- 没有模块系统
- 标准库较少
- 没有标准接口
- 缺乏管理系统

#### 模块化

- 如果程序设计的规模达到了一定程度，则必须对其进行模块化
- 模块化可以有多种形式，但至少应该提供能够将代码分隔为多个源文件的机智
- CommonJS的模块功能可以帮我们解决这个问题

##### CommonJS规范

- CommonJS规范的提出，主要是为了弥补当前JavaScript没有标准的缺陷
- CommonJS规范为JS指定了一个美好的愿景，希望JS能够在任何地方运行
- CommonJS对模块的定义十分简单
  - 模块引用
  - 模块定义
  - 模块标识

##### Node的模块化

- 在Node中，一个js文件就是一个模块
- 在Node中，每一个js文件中的js代码都是独立运行在一个函数里，而不是全局作用域，所以一个模块中的变量和函数在其他模块中无法访问

```javascript
console.log("hello world");
console.log("neded ");

// 我们可以通过 exports 向外部暴漏
exports.x = 90;
exports.fn = function (){
    console.log("？？function")
};
```

```javascript
require("node/hello");
// require("");可以传递一个文件的路径作为参数，node自动根据改引入路劲来引入外部模块
// 这里的路径如果使用相对路径，必须以 . 或者 .. 打头
```

- 使用require() 引入模块以后，该函数会返回一个对象，这个对象代表的是引入的模块
- 当使用 require() 引入模块之后，使用的即使模块标识
  - 核心模块
    - 由node引擎提供的模块
  - 文件模块
    - 用户自己创建的模块
    - 文件标识就是文件的路径（可以是绝对路径也可以是相对路径）

- 在node中有一个全局对象，global，它的作用和网页中的 window 类似
  - 在全局中创建的变量，都会作为 global的属性保存
  - 在全局中创建的方法，都会作为 global的方法保存
- 在node执行模块中的代码的时候，他会首先在代码的最顶部，添加代码，在底部添加代码

```javascript
// exports 该对象用来将变量或者函数暴漏到外部
// require 函数，用来引入外部的模块
// module 代表的是当前模块本身.exports就是module的属性
// module.exports = exports 结果为true
// 但是注意：exports只能通过.的形式向外暴漏的内部变量，而module.exports既可以通过.，也可以直接赋值
// __filename 
// __dirname
function (exports, require, module, __filename, __dirname) {
    //
    console.log("hello world");
    console.log("neded ");

    console.log(arguments.callee+" ");

    // 我们可以通过 exports 向外部暴漏
    exports.x = 90;
    exports.fn = function (){
        console.log("？？function")
    };

    console.log(__filename);
	// E:\WebStromHome\ladybirdHome\node-js-v1.0\node\hello.js
    // 当前模块的完整路径
    console.log(__dirname);
    // E:\WebStromHome\ladybirdHome\node-js-v1.0\node
    // 当前模块所在的文件夹目录
} 
```

- 实际上模块上的代码，都是包装在一个函数中执行的，并且在函数执行的时候，同时传进去了5个实参，如上

#### 包 package

- CommonJS的包规范允许我们将一组相关的模块组合到一起，形成一组完整的工具
- CommonJS的包规范由 **包结构** 和 **包描述文件** 两个部分组成
- 包结构
  - 用于组织包中的各种文件
  - 实际上就是一个压缩问价，解压之后还为目录。符合规范的目录，还应该包含如下文件
    - **package.json 描述文件** （必须的）
    - bin 可执行的二进制文件
    - lib js代码
    - doc 文档
    - test 单元测试
- 包描述文件
  - 描述包的相关信息，以供外部读取分析
  - 用于表达非代码相关的信息，它是一个位于JSON格式下的文件 - package.json 位于包的根目录下，是包的重要组成部分
  - 不可以有注释
  - `name` 设置了应用程序/软件包的名称。
  - `version` 表明了当前的版本。
  - `description` 是应用程序/软件包的简短描述。
  - `main` 设置了应用程序的入口点。
  - `private` 如果设置为 `true`，则可以防止应用程序/软件包被意外地发布到 `npm`。
  - `scripts` 定义了一组可以运行的 node 脚本。
  - `dependencies` 设置了作为依赖安装的 `npm` 软件包的列表。
  - `devDependencies` 设置了作为开发依赖安装的 `npm` 软件包的列表。
  - `engines` 设置了此软件包/应用程序在哪个版本的 Node.js 上运行。
  - `browserslist` 用于告知要支持哪些浏览器（及其版本）。
- 版本控制
  - 如果 Node.js 软件包中有一件很棒的事情，那就是它们都同意使用语义版本控制作为版本编号。
  - 语义版本控制的概念很简单：所有的版本都有 3 个数字：`x.y.z`。
    - 第一个数字是主版本。
    - 第二个数字是次版本。
    - 第三个数字是补丁版本。
  - 当发布新的版本时，不仅仅是随心所欲地增加数字，还要遵循以下规则：
    - 当进行不兼容的 API 更改时，则升级主版本。
    - 当以向后兼容的方式添加功能时，则升级次版本。
    - 当进行向后兼容的缺陷修复时，则升级补丁版本。
  - `^`: 如果写入的是 `^0.13.0`，则当运行 `npm update` 时，会更新到补丁版本和次版本：即 `0.13.1`、`0.14.0`、依此类推。
  - `~`: 如果写入的是 `〜0.13.0`，则当运行 `npm update` 时，会更新到补丁版本：即 `0.13.1` 可以，但 `0.14.0` 不可以。
  - `>`: 接受高于指定版本的任何版本。
  - `>=`: 接受等于或高于指定版本的任何版本。
  - `<=`: 接受等于或低于指定版本的任何版本。
  - `<`: 接受低于指定版本的任何版本。
  - `=`: 接受确切的版本。
  - `-`: 接受一定范围的版本。例如：`2.1.0 - 2.6.2`。
  - `||`: 组合集合。例如 `< 2.1 || > 2.6`。
  - 无符号: 仅接受指定的特定版本（例如 `1.2.1`）。
  - `latest`: 使用可用的最新版本。

#### NPM

- Node Package Manager
- CommonJS包规范是理论，NPM是其中一种实践
- 对于Node而言，NPM帮助其完成了第三方模块的发布、安装和依赖等，借助了NPM，Node与第三方模块之前形成了很好的一个生态系统

#####  NPM命令

- npm -v 
  - 查看版本
- npm version 
  - 打印所有的版本信息
- npm 
  - 帮助说明
- npm search 包名
  - 搜索模块包
- npm install 包名
  - 在当前目录安装包
- npm install 包名 -g
  - 全局模式安装包

- npm remove 包名
  - 删除包
- npm install 包名 --save
  - 安装，然后添加到依赖中
- npm install 
  - 下载当前项目依赖的包

##### 配置CNPM

- 淘宝NPM镜像： https://developer.aliyun.com/mirror/npm 

- npm install -g cnpm --registry=https://registry.npm.taobao.org
- 通过npm 下载的包都放在node_modules 文件夹中
- 通过 npm 下载的包，直接通过包名引用即可

##### NPM搜索包的流程

- node 在使用模块名称来引入模块的时候，它会首先在当前目录的 node_modules 中寻找是否包含有该模块，如果包含，则直接使用，如果没有，就去上一级目录的node_modules重去寻找，如果有就直接使用，没有就继续找，直到找到磁盘的根目录，还没有就报错 

#### 文件系统

##### Buffer缓冲区

- Buffer的结构和数组很像。操作的方法也很像类似
- 数组不能存储二进制文件，但是buffer可以存储二进制文件
- 从结构上来讲buffer非常像一个数组，它的元素为16进制的两位数
- 实际上一个元素就表示内存中的一个字节
- 实际上buffer中的内存不是通过JavaScript分配的，而是通过底层C++申请的
- 也就是我们直接通过Buffer来创建内存中的空间
- 在buffer中存储的都是二进制数据，但是在显示的时候都是以十六进制显示的
- buffer中每一个元素的范围都是从 00 ~ ff [0~255] - 00000000 ~ 11111111
- 计算机 1个0或者一个1，都称之为1位（bit）
- 8bit = 1byte（字节） 字节是传输数据的最小单位
- 1024 byte = 1kb
- 1024 kb = 1mb
- 1024 mb = 1gb

```javascript
var str = "hello demo";

// 将一个字符串保存到buffer中
var buffer = Buffer.from(str);

// <Buffer 68 65 6c 6c 6f 20 64 65 6d 6f>
// 10
// 10
console.log(buffer);
console.log(str.length); // 获取字符串的长度
console.log(buffer.length); // 占用内存的大小

// 创建一个指定大小的buffer 1024byte = 1kb
// buffer的构造函数都是不推荐使用的
var buffer2 = new Buffer(1024);

// 创建一个10个字节的buffer
var buffer3 = new Buffer.alloc(10);
// 通过索引，来操作buffer中的元素,buffer中存储的是16进制的数据
buffer3[1] = 88;
buffer3[2] = 89;
buffer3[3] = 0xaa;
console.log(buffer3) // <Buffer 00 58 59 aa 00 00 00 00 00 00>
// 只要数字在控制台或者页面输出一定是十进制
console.log(buffer3[3].toString(16)); // aa toString()的参数为进制所数，只要是数字，取出来就一定是数字
for (var i = 0; i <buffer3.length+1; i++) {
    console.log(buffer3[i]);
}
// Buffer.allocUnsafe(size) 创建一个指定大小的buffer，但是buffer中可能有敏感数据
// 也就是说在使用的时候没有把数据清除，可能还是上次用过的数据，
var buffer4 = new Buffer.allocUnsafe(10);
console.log(buffer4);
```

- buffer 的大小一旦确定就不能修改，buffer实际上是对底层内存的直接操作  
- 只要数字在控制台或者页面输出一定是十进制

##### 文件系统

- 在Node中，与文件系统交互式非常重要的，服务器的本质就是将本地的文件发送给远程的客户端，

- Node通过fs模块来和文件系统进行交互，

- 该模块提供了一些标准的文件访问API来打开、读取、写入文件，以及与其交互

- 要使用fs模块，首先需要对其进行加载 `- const fs = require("fs");`

- 同步和异步调用

  - fs模块中所有的操作都有2种形式可供选择 **同步** 和 **异步**

  - 同步文件系统会 **阻塞** 程序得运行，也就是除非操作完毕，否则不会向下执行代码

  - 异步文件系统 **不会阻塞** 程序的执行，而是在操作完成的时候，通过回调函数将结果返回

##### 同步文件的写入

  - 打开文件
  - 向文件中写入数据
  - 保存并关闭文件

```javascript
// 需要使用文件系统，得先引入js模块，fs是核心模块，直接引入不需要瞎咋
const fs = require("fs");
// fs.openSync(path[, flags, mode])
// path 是路径，文件的路径
// flags 打开文件要操作的类型 r read w 可写
// mode 可选的，一般不传
// 该方法会返回一个文件的描述符作为结果，我们可以通过该描述符，对文件进行各种操作

// fs.writeSync(fd, string[, position[, encoding]])
// fd 文件的描述符：需要写入的文件的描述符
// string 需要写入的内容
// position 写入的启始位置
// encoding 写入的编码 默认就是 UTF-8
// fs.closeSync(fd); 关闭文件

// const fs = require("fs");

// 打开文件
var fd = fs.openSync("hello.txt","w");

// 向文件中写入内容
fs.writeSync(fd, "hello????????");

// 关闭文件
fs.closeSync(fd);

```

##### 异步文件的写入

```javascript
// 需要使用文件系统，得先引入js模块，fs是核心模块，直接引入不需要瞎咋
const fs = require("fs");
// fs.open(path[, flags[, mode]], callback)
// 异步调用方法 没有返回值，结果都是通过回调函数返回的
// 当用到回调函数的时候，首先需要知道回调函数有哪些参数
// err：错误对象，如果没有错误则为null
// fd: 文件的描述符

// fs.write(fd, string[, position[, encoding]], callback)
// 用来异步写入一个文件
// fd 描述符

// const fs = require("fs");

// 打开文件
fs.open("hello2.txt", "w",function (err,fd ) {
    // 判断是否出错
    if (!err){
        console.log(arguments)
        // 如果没有出错
        fs.write(fd,"只是异步写入的数据",function(err){
            if (!err){
                console.log("success")
            }
            // 关闭文件
            fs.close(fd,function(err){
                console.log("文件已经关闭")
            });
        });
    }else{
        console.log(err)
    }
});

```

![1599569363232](images/1599569363232.png)

##### 简单文件的写入

```javascript
const fs = require("fs");

// 打开文件
fs.open("hello2.txt", "w",function (err,fd ) {
    // 判断是否出错
    if (!err){
        console.log(arguments)
        // 如果没有出错
        fs.write(fd,"只是异步写入的数据",function(err){
            if (!err){
                console.log("success")
            }
            // 关闭文件
            fs.close(fd,function(err){
                console.log("文件已经关闭")
            });
        });
    }else{
        console.log(err)
    }
});


```

##### 流式文件写入

- 同步、异步、简单文件的写入不适合大文件的写入，性能较差，容易导致内存溢出
- 流式读取和写入适用于一些比较大的文件

```javascript
// fs.createWriteStream(path[, options])
// 用来创建一个可写流
// path 文件路径
// options 配置的参数
// fs.createReadStream(path[, options])
var fs = require("fs");

var ws = fs.createWriteStream("hello.txt");
// 输出内容
ws.write("asdasdasdsadsa")

// 可以通过监听流的open和close事件来监听流的打开和关闭


ws.on("open",function () {
  console.log("stream started")
})


// 只绑定一次 触发一次之后失效
ws.once("close",function () {
    console.log("stream started")
})

// 关闭流
ws.end();
```

##### 同步文件读取

- 略

##### 异步文件读取

- 略

##### 简单文件读取

```javascript
// fs.readFile(path[, options], callback)
// fs.readFileSync(path[, options])
// path 读取的文件路径
// options 读取的选项
// callback 通过回调函数将读取的内容返回
//

const fs = require("fs");
fs.readFile("hello.txt",function(err,data){
    if (!err){
        console.log(data.toString());
    }
})
```

##### 流式文件读取

- 适用于大文件读取

```javascript
var fs = require("fs");
// 创建一个可读流
var rs = fs.createReadStream("hello.txt");
var ws = fs.createWriteStream("helloxx.txt");
// 监听流的开启和关闭
rs.once("open", function(){
    console.log("createReadStream open");
})


rs.once("close", function(){
    console.log("createReadStream close");
    ws.end();
})


ws.once("open", function(){
    console.log("createWriteStream open");
})


ws.once("close", function(){
    console.log("createWriteStream close");
})

// 如果需要服务一个可读流中的数据，必须要为可读流绑定一个data事件，他会自动开始读取数据
rs.on("data", function(data){
    // console.log(data.toString());
    ws.write(data);
})
// 直接读取可写流和可读流中的内容
rs.pipe(ws)
```

#### NodeJS操作MySQL数据库

- npm install mysql
- 数据库连接参数说明

| 参数               | 描述                                                         |
| :----------------- | :----------------------------------------------------------- |
| host               | 主机地址 （默认：localhost）                                 |
| user               | 用户名                                                       |
| password           | 密码                                                         |
| port               | 端口号 （默认：3306）                                        |
| database           | 数据库名                                                     |
| charset            | 连接字符集（默认：'UTF8_GENERAL_CI'，注意字符集的字母都要大写） |
| localAddress       | 此IP用于TCP连接（可选）                                      |
| socketPath         | 连接到unix域路径，当使用 host 和 port 时会被忽略             |
| timezone           | 时区（默认：'local'）                                        |
| connectTimeout     | 连接超时（默认：不限制；单位：毫秒）                         |
| stringifyObjects   | 是否序列化对象                                               |
| typeCast           | 是否将列值转化为本地JavaScript类型值 （默认：true）          |
| queryFormat        | 自定义query语句格式化方法                                    |
| supportBigNumbers  | 数据库支持bigint或decimal类型列时，需要设此option为true （默认：false） |
| bigNumberStrings   | supportBigNumbers和bigNumberStrings启用 强制bigint或decimal列以JavaScript字符串类型返回（默认：false） |
| dateStrings        | 强制timestamp,datetime,data类型以字符串类型返回，而不是JavaScript Date类型（默认：false） |
| debug              | 开启调试（默认：false）                                      |
| multipleStatements | 是否许一个query中有多个MySQL语句 （默认：false）             |
| flags              | 用于修改连接标志                                             |
| ssl                | 使用ssl参数（与crypto.createCredenitals参数格式一至）或一个包含ssl配置文件名称的字符串，目前只捆绑Amazon RDS的配置文件 |

- 示例代码 仅仅只是查询代码，CRUD是一样的操作

```javascript
var mysql = require("mysql");
// 创建连接
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'j08'
});
// 确认连接
connection.connect();
// 编写SQL语句
var sql = "SELECT * FROM s_emp"
// 执行SQL语句
connection.query(sql,function(err, result) {
    if (err){
        throw err;
    }
    console.log("The Result is here ，last_name: " + JSON.stringify(result[0].last_name));
    // The Result is here ，last_name: "Velasquez"
})
// 关闭连接
connection.end();
```

