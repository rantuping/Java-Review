### Vue-第3篇-ajax

#### vue-resource

- Vue插件，非官方推荐，vue1.x 应用广泛

**使用**

- npm install vue-resource --save
-  https://github.com/pagekit/vue-resource/blob/develop/docs/http.md 
- **编码**

```vue
// 引入模块
import VueResource from 'vue-resource'
// 使用插件
Vue.use(VueResource)
// 通过 vue/组件对象发送 ajax 请求
this.$http.get('/someUrl').then((response) => {
// success callback
console.log(response.data) //返回结果数据
}, (response) => {
// error callback
console.log(response.statusText) //错误信息
})
```

#### axios

- 通用的ajax请求库，官方推荐，vue2.x使用广泛

**使用**

- npm install axios --save
-  https://github.com/pagekit/vue-resource/blob/develop/docs/http.md 
- **编码**

```js
import Vue from 'vue'
import App from './App'
import VueResource from 'vue-resource'


// 声明使用插件
// 内部会给vm对象和组件添加一个属性：$http
Vue.use(VueResource)

new Vue({
  el: '#app',
  components: {App},
  template: '<App/>'
})

```

```vue
<template>
  <div>
    <div v-if="!repoUrl">loading...</div>
    <div v-else>most star repo is <a :href="repoUrl" target="_blank">{{repoName}}</a></div>
  </div>
</template>

<script>
  import Axios from 'axios'
  export default {
    data () {
      return {
        repoName: '',
        repoUrl: ''
      }
    },
    mounted () {
      // 发送ajax请求获取资源
      const url = 'https://api.github.com/search/repositories?q=vue&sort=stars'
      // this.$http.get(url).then(
      //   response => {
      //     // 成功了
      //     const result = response.data
      //     // 得到第一个。最受欢迎的
      //     const mostRepo = result.items[0]
      //     this.repoUrl = mostRepo.html_url
      //     this.repoName = mostRepo.name
      //   },
      //   response => {
      //     alert('请求失败')
      //   })

      Axios.get(url).then(
        response => {
          // 成功了
          const result = response.data
          // 得到第一个。最受欢迎的
          const mostRepo = result.items[0]
          this.repoUrl = mostRepo.html_url
          this.repoName = mostRepo.name
        }).catch(error => {
          alert('请求失败')
        }
      )
    }
  }
</script>

<style>

</style>

```

**案例**

```js
import Vue from 'vue'
import App from './App'
import VueResource from 'vue-resource'


// 声明使用插件
// 内部会给vm对象和组件添加一个属性：$http
Vue.use(VueResource)

new Vue({
  el: '#app',
  components: {App},
  template: '<App/>'
})

```

```vue
<template>
  <div class="container">
    <Search/>
    <UsersMain/>
  </div>
</template>

<script>
  import Search from './components2/Search.vue'
  import Main from './components2/Main.vue'

  export default {
    components: {
      Search,
      UsersMain:Main
    }
  }
</script>

<style>

</style>

```

```vue
<template>
  <div>
    <h3 v-if="firstView">请输入用户名搜索</h3>
    <h3 v-if="loading">Loading...</h3>
    <h3 v-if="errorMsg">{{errorMsg}}</h3>
    <div class="row"></div>
    <div class="card" v-for="(user,index) in users" :key="index">
      <a :href="user.url" target="_blank">
        <img :src="user.avatar_url" style='width: 100px'/>
      </a>
      <p class="card-text">{{user.name}}</p>
    </div>
  </div>
</template>

<script>
  import PubSub from 'pubsub-js'
  import axios from 'axios'

  export default {
    data () {
      return {
        firstView: true,
        loading: false,
        users: null, // [{url:'',avatar_url:'',name:''}]
        errorMsg: ''
      }
    },
    mounted () {
      // 订阅消息
      PubSub.subscribe('search', (mag, searchName) => {
        // 需要搜索
        const url = `https://api.github.com/search/users?q=${searchName}`
        // 更新状态
        this.firstView = false
        this.loading = true
        this.errorMsg = ''
        this.users = null
        axios.get(url).then(
          response => {
            // 成功
            const data = response.data
            const users = data.items.map(item => ({
              url: item.html_url,
              avatar_url: item.avatar_url,
              name: item.login
            }))
            this.loading = false
            this.users = users
          }
        ).catch(errorMsg => {
          // 失败
          this.loading = false
          this.errorMsg = errorMsg
        })
      })
    }
  }
</script>

<style>
  .card {
    float: left;
    width: 33.333%;
    padding: .75rem;
    margin-bottom: 2rem;
    border: 1px solid #efefef;
    text-align: center;
  }

  .card > img {
    margin-bottom: .75rem;
    border-radius: 100px;
  }

  .card-text {
    font-size: 85%;
  }

</style>

```

```vue
<template>
  <section class="jumbotron">
    <h3 class="jumbotron-heading">Search Github Users</h3>
    <div>
      <input type="text" placeholder="enter the name you search" v-model="searchName"/>
      <button @click="search">Search</button>
    </div>
  </section>
</template>

<script>
  import PubSub from 'pubsub-js'

  export default {
    data () {
      return {
        searchName: ''
      }
    },
    methods: {
      search () {
        const searchName = this.searchName.trim()
        if (searchName) {
          // 发布搜索的消息
          PubSub.publish('search', searchName)
        }
      }
    }
  }
</script>

<style>

</style>

```

**案例结果**

![1599981614920](images/1599981614920.png)

