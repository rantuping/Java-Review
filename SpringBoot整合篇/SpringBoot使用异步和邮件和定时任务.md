### SpringBoot 使用 异步、邮件、定时任务

#### 异步任务

编写 Service ,给需要执行的异步任务添加 ` @Async`注解即可

```java
@Service
public class AsyncService {
    /**
     * 告诉 Spring 这是一个异步的方法
     */
    @Async
    public void hello() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("数据正在处理...");
    }
}
```

编写Controller 测试异步任务

```java
@RestController
public class HelloController {
    @Autowired
    private AsyncService asyncService;

    @GetMapping("/hello")
    public String hello() {
        asyncService.hello();
        return "hello";
    }
}
```

想要异步任务生效，必须在启动类中添加注解 `@EnableAsync`

```java
// 开启异步
@EnableAsync
@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
```

启动项目，浏览器打开网址 `http://localhost:8080/hello`测试即可

#### 邮件任务

添加依赖 

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-mail</artifactId>
</dependency>
```

编写Service类

```java
@Service
public class JavaMailBy163Service {
    @Autowired
    JavaMailSenderImpl mailSender;

    public void sendMail(String message) {
        // 一个简单的邮件
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        // 标题
        simpleMailMessage.setSubject("通知：ic你好");
        // 内容
        simpleMailMessage.setText(message);
        // 发给谁
        simpleMailMessage.setTo("icanci@foxmail.com");
        // 谁发的
        simpleMailMessage.setFrom("icanci@163.com");
        // 发送
        mailSender.send(simpleMailMessage);
    }

    public void sendMoreMail(String message) throws MessagingException {
        // 一个复杂的邮件
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        // 组装
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        // 标题
        helper.setSubject("通知：ic你好");
        // 发送的内容
        String text = "<p style=\"text-align: center;\">欢迎您注册：<a href=\"http://icanci.cn/\">http://icanci.cn/</a></p>\n" +
            "<p style=\"text-align: center;color: cornflowerblue;\">我们将竭诚为您服务！</p>\n" +
            "<p style=\"text-align: center;color: cornflowerblue;\">" + message + "</p>\n";
        // 设置内容，是否为html数据
        helper.setText(text, true);
        // 附件
        helper.addAttachment("213055.jpg", new File("G:\\360downloads\\213055.jpg"));
        helper.addAttachment("1000065.jpg", new File("G:\\360downloads\\1000065.jpg"));
        // 发给谁
        helper.setTo("icanci@foxmail.com");
        // 谁发的
        helper.setFrom("icanci@163.com");
        mailSender.send(mimeMessage);
    }
}
```

编写Controller代码

```java
@RestController
public class HelloController {

    @Autowired
    private JavaMailBy163Service javaMailBy163Service;

    @GetMapping("/mail")
    public String mail() {
        javaMailBy163Service.sendMail("hello icanci");
        return "邮件1发送成功";
    }

    @GetMapping("/mail2")
    public String mail2() throws MessagingException {
        javaMailBy163Service.sendMoreMail("这个是一个测试邮件");
        return "邮件2发送成功";
    }
}
```

添加配置文件

```yml
spring:
  mail:
    default-encoding: UTF-8
    username: icanci@163.com
    password: qqzz1111dsadasdas
    host: smtp.163.com
```

启动项目进行测试 测试地址`http://localhost:8080/mail`  `http://localhost:8080/mail2`

![1594544368692](C:\Users\icanci\Desktop\SpringBoot整合篇\images\1594544368692.png)

#### 定时任务

```txt
TaskScheduler // 任务调度者
TaskExecutor // 任务执行者
@EnableScheduling //开启定时功能的注解
@Scheduled // 什么时候执行

cron 表达式
```

开启定时功能

```java
// 开启异步
@EnableAsync
//开启 定时功能的注解
@EnableScheduling
@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
```

编写Service代码

```java
@Service
public class ScheduledService {

    /**
     * cron 表达式
     * 秒 分 时 日 月 周几
     * 下面的就是每隔10秒执行一次
     */
    @Scheduled(cron = "0/10 * * * * ?")
    public void hello() {
        System.out.println("hello 你被执行了");
    }
}
```

### 总结

任务在手，天下我有