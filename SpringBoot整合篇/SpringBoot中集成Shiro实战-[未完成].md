### Spring Boot中集成 Shiro 实战

#### 简介

Shiro可以完成认证、授权、加密、会话管理、Web集成、缓存等

更多介绍可以查看 `SpringBoot 中集成 Shiro 初步集成.md`

#### 快速开始

- 导入依赖
- 配置文件
- HelloWorld

#### 集成

Shiro的三大对象

- Subject 用户
- SecurityManager  管理所有用户
- Realm  连接数据

导入 依赖

```xml
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-spring</artifactId>
    <version>1.4.0</version>
</dependency>
```

编写配置类

```java

```

