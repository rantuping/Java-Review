### SpringBoot 整合 MyBatis

####  MyBatis 介绍

大家都知道，MyBatis 框架是一个持久层框架，是 Apache 下的顶级项目。Mybatis 可以让开发者的主要精力放在 sql 上，通过 Mybatis 提供的映射方式，自由灵活的生成满足需要的 sql 语句。使用简单的 XML 或注解来配置和映射原生信息，将接口和 Java 的 POJOs 映射成数据库中的记录，在国内可谓是占据了半壁江山。本节课程主要通过两种方式来对 Spring Boot 集成 MyBatis 做一讲解。重点讲解一下基于注解的方式。因为实际项目中使用注解的方式更多一点，更简洁一点，省去了很多 xml 配置（这不是绝对的，有些项目组中可能也在使用 xml 的方式）。

#### MyBatis 的配置

#### 依赖导入

Spring Boot 集成 MyBatis，需要导入 `mybatis-spring-boot-starter` 和 mysql 的依赖，这里我们使用的版本时 2.1.2，如下：

```xml
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.2</version>
</dependency>

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
```

我们点开 `mybatis-spring-boot-starter` 依赖，可以看到我们之前使用 Spring 时候熟悉的依赖，就像我在课程的一开始介绍的那样，Spring Boot 致力于简化编码，使用 starter 系列将相关依赖集成在一起，开发者不需要关注繁琐的配置，非常方便。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-autoconfigure</artifactId>
</dependency>
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis</artifactId>
</dependency>
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis-spring</artifactId>
</dependency>
```

####  properties.yml配置

我们再来看一下，集成 MyBatis 时需要在 properties.yml 配置文件中做哪些基本配置?

```yml
# 服务端口号
server:
  port: 8080

# 数据库地址
datasource:
  url: localhost:3306/blog_test

spring:
  datasource: # 数据库配置
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://${datasource.url}?useSSL=false&useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true&failOverReadOnly=false&maxReconnects=10&serverTimezone=Asia/Shanghai
    username: root
    password: ok
    hikari:
      maximum-pool-size: 10 # 最大连接池数
      max-lifetime: 1770000

mybatis:
  # 指定别名设置的包为所有entity
  type-aliases-package: cn.icanci.mybatis.entity
  configuration:
    map-underscore-to-camel-case: true # 驼峰命名规范
  mapper-locations: # mapper映射文件位置
    - classpath:mapper/*.xml
```

我们来简单介绍一下上面的这些配置：关于数据库的相关配置，我就不详细的解说了，这点相信大家已经非常熟练了，配置一下用户名、密码、数据库连接等等，这里使用的连接池是 Spring Boot 自带的 hikari，感兴趣的朋友可以去百度或者谷歌搜一搜，了解一下。  

这里说明一下 `map-underscore-to-camel-case: true`， 用来开启驼峰命名规范，这个比较好用，比如数据库中字段名为：`user_name`， 那么在实体类中可以定义属性为 `userName` （甚至可以写成 `username`，也能映射上），会自动匹配到驼峰属性，如果不这样配置的话，针对字段名和属性名不同的情况，会映射不到。

另外，现在在连接数据库的语句中，必须设置时区，否则连接报错，即`serverTimezone=Asia/Shanghai`

#### 基于 xml 的整合

使用原始的 xml 方式，需要新建 UserMapper.xml 文件，在上面的 application.yml 配置文件中，我们已经定义了 xml 文件的路径：`classpath:mapper/*.xml`，所以我们在 resources 目录下新建一个 mapper 文件夹，然后创建一个 UserMapper.xml 文件。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="cn.icanci.mybatis.dao.UserMapper">
    <resultMap id="BaseResultMap" type="cn.icanci.mybatis.entity.User">

        <id column="id" jdbcType="BIGINT" property="id" />
        <result column="user_name" jdbcType="VARCHAR" property="username" />
        <result column="password" jdbcType="VARCHAR" property="password" />
    </resultMap>

    <select id="getUserByName" resultType="cn.icanci.mybatis.entity.User" parameterType="String">
       select * from user where user_name = #{username}
  </select>
</mapper>
```

这和整合 Spring 一样的，namespace 中指定的是对应的 Mapper， `<resultMap>` 中指定对应的实体类，即 User。然后在内部指定表的字段和实体的属性相对应即可。这里我们写一个根据用户名查询用户的 sql。  

实体类中有 id，username 和 password

```java
@Component
@Data
public class User {
    private Long id;
    private String username;
    private String password;
}
```

数据库表SQL

```sql
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键，无意义',
  `user_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
```

UserMapper.java 文件中写一个接口即可：

```java
@Repository
public interface UserMapper {
    public User getUserByName(String String);
}
```

service 的代码

```java
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public User getUserByName(String username) {
        return userMapper.getUserByName(username);
    }
}
```

我们写一个 Controller 来测试一下：

```java
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/getUser/{username}")
    public User getUser(@PathVariable String username) {
        return userService.getUserByName(username);
    }
}
```

这里需要注意一下：Spring Boot 如何知道这个 Mapper 呢？一种方法是在上面的 mapper 层对应的类上面添加 `@Mapper` 注解即可，但是这种方法有个弊端，当我们有很多个 mapper 时，那么每一个类上面都得添加 `@Mapper` 注解。另一种比较简便的方法是在 Spring Boot 启动类上添加`@MaperScan` 注解，来扫描一个包下的所有 mapper。如下：

然后因为需要将 xml文件 和 xxxmapper.java 映射起来，所以此时需要在启动类上配置如下

这样的话，`cn.icanci.mybatis.dao` 包下的所有 mapper 都会被扫描到了。

```java
@SpringBootApplication
@MapperScan("cn.icanci.mybatis.dao")
public class MybatisDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisDemoApplication.class, args);
    }

}
```

浏览器输入` http://localhost:8080/user/getUser/icanci `，数据库是有数据的

返回的结果如下：

```json
{
    "id": 1,
    "username": "icanci",
    "password": "123123"
}
```

#### 基于注解的整合

基于注解的整合就不需要 xml 配置文件了，MyBatis 主要提供了 `@Select`， `@Insert`， `@Update`， `Delete` 四个注解。这四个注解是用的非常多的，也很简单，注解后面跟上对应的 sql 语句即可，我们举个例子：

```java
@Repository
public interface UserMapper {

    // 基于配置文件的整合
    public User getUserByName(String String);

    // 基于注解的整合
    @Select("select * from user where id = #{id}")
    User getUser(Long id);
}
```

这跟 xml 文件中写 sql 语句是一样的，这样就不需要 xml 文件了，但是有个问题，有人可能会问，如果是两个参数呢？如果是两个参数，我们需要使用 `@Param` 注解来指定每一个参数的对应关系，如下：

```java
@Repository
public interface UserMapper {

    // 基于配置文件的整合
    public User getUserByName(String String);

    // 基于注解的整合
    @Select("select * from user where id = #{id}")
    User getUser(Long id);
    
    // 基于注解的整合 多个参数
    @Select("select * from user where id = #{id} and user_name=#{name}")
    User getUserByIdAndName(@Param("id") Long id, @Param("name") String username);
}
```

可以看出，`@Param` 指定的参数应该要和 sql 中 `#{}` 取的参数名相同，不同则取不到。可以在 controller 中自行测试一下。

有个问题需要注意一下，一般我们在设计表字段后，都会根据自动生成工具生成实体类，这样的话，基本上实体类是能和表字段对应上的，最起码也是驼峰对应的，由于在上面配置文件中开启了驼峰的配置，所以字段都是能对的上的。但是，万一有对不上的呢？我们也有解决办法，使用 `@Results` 注解来解决。

```java
@Repository
public interface UserMapper {

    // 基于配置文件的整合
    public User getUserByName(String String);

    // 基于注解的整合
    @Select("select * from user where id = #{id}")
    @Results({
        @Result(property = "username", column = "user_name"),
        @Result(property = "password", column = "password")
    })
    User getUser(Long id);
    
    // 基于注解的整合 多个参数
    @Select("select * from user where id = #{id} and user_name=#{name}")
    User getUserByIdAndName(@Param("id") Long id, @Param("name") String username);
}
```

`@Results` 中的 `@Result` 注解是用来指定每一个属性和字段的对应关系，这样的话就可以解决上面说的这个问题了。  

当然了，我们也可以 xml 和注解相结合使用，目前我们实际的项目中也是采用混用的方式，因为有时候 xml 方便，有时候注解方便，比如就上面这个问题来说，如果我们定义了上面的这个 UserMapper.xml，那么我们完全可以使用 `@ResultMap` 注解来替代 `@Results` 注解，如下：

```java
@Select("select * from user where id = #{id}")
@ResultMap("BaseResultMap")
User getUser(Long id);
```

`@ResultMap` 注解中的值从哪来呢？对应的是 UserMapper.xml 文件中定义的 `<resultMap>` 时对应的 id 值：

```xml
 <resultMap id="BaseResultMap" type="cn.icanci.mybatis.entity.User">
     <id column="id" jdbcType="BIGINT" property="id" />
     <result column="user_name" jdbcType="VARCHAR" property="username" />
     <result column="password" jdbcType="VARCHAR" property="password" />
</resultMap>
```

这种 xml 和注解结合着使用的情况也很常见，而且也减少了大量的代码，因为 xml 文件可以使用自动生成工具去生成，也不需要人为手动敲，所以这种使用方式也很常见。

#### 配置Druid日志监控

需要导入依赖

```xml
<!-- Log4j 依赖 -->
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
<!-- Druid 连接池 -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid</artifactId>
    <version>1.1.22</version>
</dependency>
```

编写配置文件

```java
package cn.icanci.cmp.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: icanci
 * @ProjectName: cmp
 * @PackageName: cn.icanci.cmp.config
 * @Date: Created in 2020/7/9 16:54
 * @ClassAction: Druid 配置
 */
@Configuration
public class DruidConfig {

    @ConfigurationProperties("spring.datasource")
    @Bean
    public DruidDataSource druidDataSource() {
        return new DruidDataSource();
    }

    /**
     * 配置Druid的监控
     *
     * 配置一个管理后台的Servlet
     * @return
     */
    @Bean
    public ServletRegistrationBean statViewServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        Map<String, String> initParams = new HashMap<>();

        initParams.put("loginUsername", "admin");
        initParams.put("loginPassword", "icanci");
        //默认就是允许所有访问
        initParams.put("allow", "");
        initParams.put("deny", "192.168.15.21");

        bean.setInitParameters(initParams);
        return bean;
    }

    /**
     * 配置一个web监控的filter
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean webStatFilter() {
        FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.setFilter(new WebStatFilter());

        Map<String, String> initParams = new HashMap<>();
        initParams.put("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        bean.setInitParameters(initParams);
        bean.setUrlPatterns(Arrays.asList("/*"));
        return bean;
    }
}
```

Druid连接池SQL监控地址：` http://localhost:8181/druid/login.html ` 账户：`admin`  密码：`icanci`

### 总结

本节课主要系统的讲解了 Spring Boot 集成 MyBatis 的过程，分为基于 xml 形式和基于注解的形式来讲解，通过实际配置手把手讲解了 Spring Boot 中 MyBatis 的使用方式，并针对注解方式，讲解了常见的问题已经解决方式，有很强的实战意义。在实际项目中，建议根据实际情况来确定使用哪种方式，一般 xml 和注解都在用。

**另外，基于mybatis的缓存、二级缓存、懒加载、多表查询等没有写出**