### SpringBoot 整合 Swagger

#### Swagger

学习目标：

- 了解Swagger的作用和概念
- 了解前后端分离
- 在SpringBoot中集成Swagger

#### Swagger简介

**前后端分离**

Vue+SpringBoot

后端时代：前端直用管理静态页面；HTML ==> 后端。模板引擎 JSP => 后端是主力

前后端分离时代：

- 后端：控制层，服务层，数据访问层 [后端团队]
- 前端：前端控制层，视图层 [前端团队]
  - 伪造后端数据，json。已经存在了，不需要后端依旧而可以把项目跑起来
- 前端后端如何交互 ？ ====> API
- 前后端相对独立，松耦合
- 前后端可以部署在不同的服务器上

产生一个问题：

- 前后端集成联调，前端开发和后端开发人员无法做到“尽快协调，尽快解决”，最终导致问题集中爆发

解决方案：

- 首先指定 schema[计划的提纲]，实时更新最新的API，降低集成的风险
- 早些年：制定word计划文档
- 前后端分离：
  - 前端测试后端接口：postman
  - 后端提供接口，需要实时更新最新的消息以及改动

#### Swagger

- 号称世界上最流行的API框架
- RestFul API 文档在线生成工具 => API文档与API定义同步更新
- 直接运行，可以在线测试API接口
- 支持多种语言(Java,PHP)

官网： https://swagger.io/ 

在项目中使用Swagger需要springBox

- swagger2
- ui

#### SpringBoot集成Swagger

1、新建一个Spring-Web项目

2、导入依赖

```xml
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger2 -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
</dependency>
```

3、编写一个Hello工程

4、配置Swagger => Config

```java
@Configuration
@EnableSwagger2
public class SwaggerConfig {

}
```

5、启动项目就可以看到文档 测试

文档地址：  http://localhost:8080/swagger-ui.html 

![1593592115390](images/1593592115390.png)

#### 配置Swagger

Swagger的bean实例Docket；

```java
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    //配置Swagger的bean实例
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo());
    }

    //配置Swagger信息  = apiInfo
    @Bean
    public ApiInfo apiInfo() {
        Contact contact = new Contact("icanci", "https://icanci.oschina.io/", "icanci@foxmail.com");
        return new ApiInfo(
                "JUST-Admin-Web-API",
                "Just Do It",
                "v1.0",
                "https://icanci.oschina.io/",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<VendorExtension>());

    }
}
```

#### Swagger 配置扫描接口

Docket.select();

```java
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    //配置Swagger的bean实例
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                // 配置 要扫描接口的方式：RequestHandlerSelectors
                // 配置 需要扫描的包：basePackage
                // 扫描全部 ： any()
                // 扫描类上的注解：withClassAnnotation() 注解的反射对象
                // 扫描方法上的注解：withMethodAnnotation
                .apis(RequestHandlerSelectors.basePackage("cn.icanci.swagger.controller"))
                // 过滤路径：paths()
                .paths(PathSelectors.ant("/icanci/**"))
                .build();
    }

    //配置Swagger信息  = apiInfo
    @Bean
    public ApiInfo apiInfo() {
        Contact contact = new Contact("icanci", "https://icanci.oschina.io/", "icanci@foxmail.com");
        return new ApiInfo(
                "JUST-Admin-Web-API",
                "Just Do It",
                "v1.0",
                "https://icanci.oschina.io/",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<VendorExtension>());

    }
}
```

#### 配置是否启动Swagger

```java
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    //配置Swagger的bean实例
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                // 是否启用Swagger：enable 如果为false 就不能再浏览器中访问
                .enable(false)
                .select()
                // 配置 要扫描接口的方式：RequestHandlerSelectors
                // 配置 需要扫描的包：basePackage
                // 扫描全部 ： any()
                // 扫描类上的注解：withClassAnnotation() 注解的反射对象
                // 扫描方法上的注解：withMethodAnnotation
                .apis(RequestHandlerSelectors.basePackage("cn.icanci.swagger.controller"))
                // 过滤路径：paths()
//                .paths(PathSelectors.ant("/icanci/**"))
                .build();
    }
}
```

我只希望我的Swagger在成产环境使用，在发布的时候不用？

- 判断是不是生产环境 flag = false
- 注入enable 

```java
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    //配置Swagger的bean实例
    @Bean
    public Docket docket(Environment environment) {

        //设置要显示的Swagger环境
        Profiles profiles = Profiles.of("dev");

        //获取项目的环境：
        boolean res = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                // 是否启用Swagger：enable 如果为false 就不能再浏览器中访问
                .enable(res)
                .select()
                // 配置 要扫描接口的方式：RequestHandlerSelectors
                // 配置 需要扫描的包：basePackage
                // 扫描全部 ： any()
                // 扫描类上的注解：withClassAnnotation() 注解的反射对象
                // 扫描方法上的注解：withMethodAnnotation
                .apis(RequestHandlerSelectors.basePackage("cn.icanci.swagger.controller"))
                // 过滤路径：paths()
//                .paths(PathSelectors.ant("/icanci/**"))
                .build();
    }
}
```

#### 配置API文档分组

```java
// .groupName("A"); 配置多个分组，就是配置多个Docket实例就可

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("A");
    }

    @Bean
    public Docket docket2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("B");
    }

    @Bean
    public Docket docket3() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("c");
    }
}
```

#### 实体类配置

只要ModelAndView返回实体类对象即可生成映射

```java
@ApiModel("用户实体类")
@Component
public class User {
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("用户密码")
    private String password;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
}
```

```java
@RestController
public class HelloController {
    @GetMapping(value = "/hello")
    public String hello() {
        return "hello";
    }

    //只要我们的接口中存在实体类就有
    @GetMapping(value = "/user")
    public User user() {
        return new User("hello", "pwe");
    }
    @ApiOperation("hello2控制器")
    @GetMapping(value = "/hello2")
    public String hello2(@ApiParam("用户名") String username) {
        return "hello"+username;
    }
}
```

#### 总结

1、我们可以实现通过Swagger给一些比较难以理解的属性或者接口，增加注释信息

2、接口文档试试更新

3、可以在线测试

Swagger是一个优秀的工具

注意点：在正式项目上线的时候，关闭Swagger！