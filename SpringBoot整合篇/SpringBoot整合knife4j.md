### SpringBoot 整合 knife4j

#### Knife4j

学习目标：

- 了解Knife4j的作用和概念
- 了解前后端分离
- 在SpringBoot中集成Swagger

#### Knife4j简介

**前后端分离**

Vue+SpringBoot

后端时代：前端直用管理静态页面；HTML ==> 后端。模板引擎 JSP => 后端是主力

前后端分离时代：

- 后端：控制层，服务层，数据访问层 [后端团队]
- 前端：前端控制层，视图层 [前端团队]
  - 伪造后端数据，json。已经存在了，不需要后端依旧而可以把项目跑起来
- 前端后端如何交互 ？ ====> API
- 前后端相对独立，松耦合
- 前后端可以部署在不同的服务器上

产生一个问题：

- 前后端集成联调，前端开发和后端开发人员无法做到“尽快协调，尽快解决”，最终导致问题集中爆发

解决方案：

- 首先指定 schema[计划的提纲]，实时更新最新的API，降低集成的风险
- 早些年：制定word计划文档
- 前后端分离：
  - 前端测试后端接口：postman
  - 后端提供接口，需要实时更新最新的消息以及改动

#### Knife4j

- 和Swagger类似的接口生成文档 到那时比Swagger好看
- RestFul API 文档在线生成工具 => API文档与API定义同步更新
- 直接运行，可以在线测试API接口

官网：  https://doc.xiaominfo.com/ 

#### SpringBoot集成和配置 Knife4j

 [https://doc.xiaominfo.com/knife4j/springboot.html#maven%E5%BC%95%E7%94%A8](https://doc.xiaominfo.com/knife4j/springboot.html#maven引用) 

1、新建一个Spring-Web项目

2、导入依赖

```xml
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-spring-boot-starter</artifactId>
    <!--在引用时请在maven中央仓库搜索最新版本号-->
    <version>2.0.2</version>
</dependency>

<!-- 但是有的时候会报错 Caused by: java.lang.ClassNotFoundException: javax.validation.constraints.Min 需要添加以下依赖 -->
<dependency>
    <groupId>javax.validation</groupId>
    <artifactId>validation-api</artifactId>
    <version>2.0.1.Final</version>
</dependency>
```

3、编写一个Hello工程

4、配置Swagger => Config

**Knife4j的bean实例Docket**

```java
@Configuration
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //分组名称
                .groupName("2.X版本")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("cn.icanci.knife4j.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

    @Bean
    public ApiInfo apiInfo() {
        Contact contact = new Contact("icanci", "https://icanci.oschina.io/", "icanci@foxmail.com");
        return new ApiInfo(
                "JUST-Admin-Web-API",
                "Just Do It",
                "v1.0",
                "https://icanci.oschina.io/",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<VendorExtension>());
    }
}
```

5、启动项目就可以看到文档 测试

文档地址：   http://localhost:8080/doc.html 

![1593604397678](images/1593604397678.png)

#### Swagger 配置扫描接口

Docket.select();

```java
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    //配置Swagger的bean实例
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                // 配置 要扫描接口的方式：RequestHandlerSelectors
                // 配置 需要扫描的包：basePackage
                // 扫描全部 ： any()
                // 扫描类上的注解：withClassAnnotation() 注解的反射对象
                // 扫描方法上的注解：withMethodAnnotation
                .apis(RequestHandlerSelectors.basePackage("cn.icanci.swagger.controller"))
                // 过滤路径：paths()
                .paths(PathSelectors.ant("/icanci/**"))
                .build();
    }

    //配置Swagger信息  = apiInfo
    @Bean
    public ApiInfo apiInfo() {
        Contact contact = new Contact("icanci", "https://icanci.oschina.io/", "icanci@foxmail.com");
        return new ApiInfo(
                "JUST-Admin-Web-API",
                "Just Do It",
                "v1.0",
                "https://icanci.oschina.io/",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<VendorExtension>());

    }
}
```

#### 配置是否启动Swagger

```java
@Configuration
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //分组名称
                .groupName("2.X版本")
            	//配置是否使用Knife4j
                .enable(false)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("cn.icanci.knife4j.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }
}
```

我只希望我的Swagger在成产环境使用，在发布的时候不用？

- 判断是不是生产环境 flag = false
- 注入enable 

```java
@Configuration
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
    @Bean(value = "defaultApi2")
    public Docket defaultApi2(Environment environment) {
        //设置需要显示的Knife4j环境
        Profiles profiles = Profiles.of("dev");
        boolean flag = environment.acceptsProfiles(profiles);

        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //分组名称
                .groupName("2.X版本")
                .enable(flag)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("cn.icanci.knife4j.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

    @Bean
    public ApiInfo apiInfo() {
        Contact contact = new Contact("icanci", "https://icanci.oschina.io/", "icanci@foxmail.com");
        return new ApiInfo(
                "JUST-Admin-Web-API",
                "Just Do It",
                "v1.0",
                "https://icanci.oschina.io/",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<VendorExtension>());
    }
}
```

#### 实体类配置

只要ModelAndView返回实体类对象即可生成映射

```java
@ApiModel("用户实体类")
@Component
public class User {
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("用户密码")
    private String password;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
}
```

```java
@RestController
public class HelloController {
    @GetMapping(value = "/hello")
    public String hello() {
        return "helli knife4j";
    }

    //只要我们的接口中存在实体类就有
    @GetMapping(value = "/user")
    public User user() {
        return new User("hello", "pwe");
    }

    @ApiOperation("hello2控制器")
    @GetMapping(value = "/hello2")
    public String hello2(@ApiParam("用户名") String username) {
        return "hello" + username;
    }
}
```

##### 自定义文档配置

1、修改配置文件 添加以下内容

```xml
knife4j.markdowns=classpath:markdown/*
```

 然后在Swagger的配置文件中启用 `@EnableKnife4j` 注解 

```java
@Configuration
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
}
```

然后自定义的文档，就可以显示在页面 可配置多个

#### 总结

1、我们可以实现通过Knife4j给一些比较难以理解的属性或者接口，增加注释信息

2、接口文档试试更新

3、可以在线测试

4、Knife4j拓展了很多其他的功能，很nice，值得一试

Knife4j是一个优秀的工具

注意点：在正式项目上线的时候，关闭Knife4j！